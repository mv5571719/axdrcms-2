<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

//header('Location: XDRTEST.php'); exit; //XDRTEST FINAL STEP <- DELETE THIS LINE!!!

define('Start', microtime(true)); 
require '../KERNEL-XDRCMS/FastInit.php';

if($siteBlocked):
	header('Location:' . PATH . '/error/blocked');
	exit;
endif;

if(isset($_SESSION['email']) || isset($_COOKIE['rememberme'], $_COOKIE['rememberme_token'])):
	header ('Location: '. PATH . '/me');
	exit;
endif;

if(isset($_GET['RefId']) && is_numeric($_GET['RefId']))
	$_SESSION['Register']['RefId'] = $_GET['RefId'];

require HEADER . 'login.php';
require HTML . 'Login_index.html';

echo '<!-- Loaded in '.(microtime(true) - Start).' seconds -->'; 
?>
