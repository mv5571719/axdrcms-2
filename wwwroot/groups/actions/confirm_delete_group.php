<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$require_login = true;
require "../../../KERNEL-XDRCMS/Init.php";

if(isset($_POST["groupId"]) && is_numeric($_POST["groupId"]))
	$groupid = $_POST['groupId'];
else
	exit;
	
checkloggedin(1);
$check = $MySQLi->query("SELECT name FROM groups_details WHERE id = '" . $groupid . "' LIMIT 1");
?>
<p>
�Est�s seguro de que quieres borrar el Grupo <?php echo $check->fetch_assoc()["name"]; ?>?
</p>

<p>
<a href="#" class="new-button" id="group-action-cancel"><b>Cancelar</b><i></i></a>
<a href="#" class="new-button" id="group-action-ok"><b>OK</b><i></i></a>
</p>

<div class="clear"></div>