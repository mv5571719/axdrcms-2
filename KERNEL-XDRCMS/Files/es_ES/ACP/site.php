<?php
$PageName = 'General';

$aSettings = CACHE::GetAIOConfig('ADS');
$rSettings = CACHE::GetAIOConfig('Register');
if($do === 'save' && isset($_POST['register_enabled'], $_POST['txt_button_closed'], $_POST['hotel_status'], $_POST['limitips'], $_POST['initial_credits_int'], $_POST['logo'], $_POST['staffpage_visibility'], $_POST['adf_ly'], $_POST['badgesPath'], $_POST['roomenterad'], $_POST['redirection'])):
	if(!checkAntiCsrf()):
		$msg_error = 'Secret key inválida. Porfavor, logueate con la secret key.';
	else:
		$Register = ($_POST['register_enabled'] === '1') ? true : false;
		$IpLimit = (is_numeric($_POST['limitips'])) ? $_POST['limitips'] : 0;

		$Credits = (is_numeric($_POST['initial_credits_int'])) ? $_POST['initial_credits_int'] : 0;
		$ReloadTime = (is_numeric($_POST['reload_time'])) ? $_POST['reload_time'] : 900;
		
		$AdDuration = (is_numeric($_POST['roomenterad_d']) && $_POST['roomenterad_d'] > 0 && $_POST['roomenterad_d'] < 76) ? $_POST['roomenterad_d'] : 30;

		$SiteSettings['ClientEnabled'] = $_POST['hotel_status'] == '0' ? '0' : (($_POST['hotel_status'] == '1') ? '1' : '2');
		$SiteSettings['staff.page.visibility'] = ($_POST['staffpage_visibility'] === '1') ? 1 : 0;
		$SiteSettings['initial.credits.int'] = $Credits;
		$SiteSettings['reload.time'] = $ReloadTime;
		$SiteSettings['logo'] = $_POST['logo'];
		$SiteSettings['adf.ly'] = $_POST['adf_ly'];
		$SiteSettings['badgesPath'] = $_POST['badgesPath'];
		$SiteSettings['txt.button.closed'] = $_POST['txt_button_closed'];

		$rSettings['register_enabled'] = $Register;
		$rSettings['limitips'] = $IpLimit;
		$rSettings['redirection'] = $_POST['redirection'];
		
		$aSettings['roomenterad'] = $_POST['roomenterad'];
		$aSettings['roomenterad_d'] = $AdDuration;

		CACHE::SetAIOConfig('Site', json_encode($SiteSettings));
		CACHE::SetAIOConfig('Register', json_encode($rSettings));
		CACHE::SetAIOConfig('ADS', json_encode($aSettings));

		SLOG('Change', 'Editado la configuración del sitio', 'manage.php[site]', 0);
		$msg_correct = 'Los cambios han sido guardados con éxito.';
	endif;
endif;

require HTML . 'ACP_header.html';
?>
<form action="<?php echo HPATH; ?>/manage?p=site&do=save" method="post" id="form1">
	<?php echo getAntiCsrf(); ?>
	<b>Registro</b><hr />
	<table width="100%" cellpadding="5" align="left" border="0">
		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>Registro</b>
				<div class="graytext">
					Permitir nuevas cuentas
				</div>
			</td>
			<td class="tablerow2" width="40%"  valign="middle">
				<select name="register_enabled">
					<option value="0" <?php echo (($rSettings['register_enabled']) ? '' : 'selected=true'); ?>>Desactivado</option>						 	
					<option value="1" <?php echo (($rSettings['register_enabled']) ? 'selected=true' : ''); ?>>Activado</option>
				</select>
			</td>
		</tr>
		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>IP Limit</b>
				<div class="graytext">
					Máximo de cuentas por usuario (IP)<br />0 = desactivado
				</div>
			</td>
			<td class="tablerow2" width="40%" valign="middle">
				<input type="text" name="limitips" value="<?php echo $rSettings['limitips']; ?>" size="30" class="textinput">
			</td>
		</tr>		
		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>Créditos iniciales</b></td>
			<td class="tablerow2" width="40%" valign="middle">
				<div class="input-control">
					<input type="text" name="initial.credits.int" value="<?php echo $SiteSettings['initial.credits.int']; ?>" size="30" class="textinput">
				</div>
			</td>
		</tr>
		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>Redirección</b>
				<div class="graytext">
					Página a visitar después del registro
				</div>
			</td>
			<td class="tablerow2" width="40%" valign="middle">
				<input type="text" name="redirection" value="<?php echo $rSettings['redirection']; ?>" size="30" class="textinput">
			</td>
		</tr>
	</table>
	<b>General</b><hr />
	<table width="100%" cellpadding="5" align="left" border="0">
		<tr>
			<td class="tablerow1"  width="20%" valign="middle"><b>Manteminiento</b></td>
			<td class="tablerow2" width="40%"  valign="middle">
				<b><a href="<?php echo HPATH; ?>/manage?p=help"><?php echo (($Restrictions['Maintenance']['Active']) ? 'Activado' : 'Desactivado'); ?></a></b>
			</td>
		</tr>
		<tr>
			<td class="tablerow1" width="20%"  valign="middle"><b>Tiempo de inactividad (en segundos; < 1500 = desactivado)</b>
				<div class="graytext">
					Establece el tiempo limite que el usuario pueda estar inactivo.
					Si el usuario lo supera, será desconectado.
				</div>
			</td>
			</td>
			<td class="tablerow2" width="40%" valign="middle">
				<div class="input-control">
					<input type="text" name="reload.time" id="client_text" value="<?php echo $SiteSettings['reload.time']; ?>" size="30" class="textinput">
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>Logo (=+ 121x51)</b>
				<div class="graytext">
					<img src="<?php echo $SiteSettings['logo']; ?>" id="imageide" height="51" width="121"> 
				</div>
			</td>
			<td class="tablerow2" width="40%" valign="middle">
				<input type="text" name="logo" id="imageurl" value="<?php echo $SiteSettings['logo']; ?>" size="100" class="textinput" onchange="ChangeImage('imageide', 'imageurl', '')">
			</td>
		</tr>
		
		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>Directorio de placas</b>
				<div class="graytext">
					<img src="<?php echo $SiteSettings['badgesPath']; ?>/ADM.gif" id="admBadge" height="31" width="31"> 
				</div>
			</td>
			<td class="tablerow2" width="40%" valign="middle">
				<input type="text" name="badgesPath" id="badgesPath" value="<?php echo $SiteSettings['badgesPath']; ?>" size="100" class="textinput" onchange="ChangeImage('admBadge', 'badgesPath', '/ADM.gif')">
			</td>
		</tr>

		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>Visibilidad de la página Staff</b></td>
			<td class="tablerow2" width="40%" valign="middle">
				<select name="staffpage_visibility" class="styled">
					<option value="0" <?php echo (($SiteSettings['staff.page.visibility']) ? '' : 'selected=true'); ?>>Todo el mundo</option>						 	
					<option value="1" <?php echo (($SiteSettings['staff.page.visibility']) ? 'selected=true' : ''); ?>>Solo staffs.</option>
				</select>
			</td>
		</tr>
	</table>
	<b>Client</b><hr />
	<table width="100%" cellpadding="5" align="left" border="0">
		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>Disponibilidad del cliente.</b>
				<div class="graytext"></div>
			</td>
			<td class="tablerow2" width="40%" valign="middle">
				<select name="hotel_status" class="styled">
					<option value="0" <?php echo $SiteSettings['ClientEnabled'] == '0' ? 'selected=true' : ''; ?>>Cerrado</option>						 	
					<option value="1" <?php echo $SiteSettings['ClientEnabled'] == '1' ? 'selected=true' : ''; ?>>Abierto</option>
					<option value="2" <?php echo $SiteSettings['ClientEnabled'] == '2' ? 'selected=true' : ''; ?>>Solo Staffs</option>
				</select>
			</td>
		</tr>
		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>Closed hotel button</b>
				<div class="graytext">
					Texto a mostrar en el botón cuando el hotel está cerrado
				</div>
			</td>
			<td class="tablerow2" width="40%" valign="middle">
				<input type="text" name="txt.button.closed" value="<?php echo $SiteSettings['txt.button.closed']; ?>" size="35" class="textinput">
			</td>
		</tr>

		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>adf.ly Client (u otros)</b>
				<div class="graytext">
					Dejar en blanco para desactivar
				</div>
			</td>
			<td class="tablerow2" width="40%" valign="middle">
				<input type="text" name="adf.ly" id="adf.ly" value="<?php echo $SiteSettings['adf.ly']; ?>" size="30" class="textinput">
			</td>
		</tr>
		
		<tr>
			<td class="tablerow1"  width="20%" valign="middle"><b>Room enter ad</b>
				<div class="graytext">
					Mostrar publicidad al cargar una sala. El anuncio tiene que tener unas dimensiones de: 728x90<br />Dejar vacio para desactivar
				</div>
			</td>
			<td class="tablerow2" width="40%"  valign="middle">
				<input type="number" name="roomenterad_d" min="1" max="75" value="<?php echo $aSettings['roomenterad_d']; ?>"></input> - <i>Duración del anuncio en segundos</i><br />
				<textarea placeholder="HTML del anuncio" name="roomenterad" cols="102" rows="5" wrap="soft" class="multitext"><?php echo $aSettings['roomenterad']; ?></textarea>
			</td>
		</tr>
	</table>
	<div class="clear"></div>
</form>
<script type="text/javascript">
	ChangeMenuButtons("Guardar", function() { element('#form1').submit() }, "", "");
</script>