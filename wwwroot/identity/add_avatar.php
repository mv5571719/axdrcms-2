<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$Load["Page"] = "add_avatar";
$Load["Community"] = "identify.php";

if(!isset($_POST['bean_avatarName']) && !isset($_POST['checkFigureOnly']))
	$Load["SubHeaders"] = "avatars";

$Load["TLR"] = "Avatars_identify";
$Load["Footer"] = "community";
require_once('../Kernel/Init.php');
$Hotel->Close();
?>