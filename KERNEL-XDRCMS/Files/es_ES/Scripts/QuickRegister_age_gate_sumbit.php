<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2013 Xdr.
|+=========================================================+
|| # Xdr 2013. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

unset($_SESSION['quickregister']['register_errors']);
	
if(isset($_POST["bean_day"], $_POST['bean_month'], $_POST['bean_year'])):
	$cConfig = GetAIOConfig("Client");
	$Gender = "";

	if(!$cConfig["client.new.user.reception"] && isset($_POST['bean_gender'])):
		$Gender = ($_POST['bean_gender'] == "male") ? "M" : "F";
		$_SESSION['quickregister']['select_gender'] = $Gender;
	elseif(!$cConfig["client.new.user.reception"]):
		$_SESSION['quickregister']['register_errors'] = "Por favor, debes proporcionar una fecha válida ";
		header("Location:".PATH."/quickregister/age_gate/e/05x");
		exit;
	endif;

	if((date("md") < $_POST['bean_month'] . $_POST["bean_day"] ? date("Y") - $_POST['bean_year'] - 1 : date("Y") - $_POST['bean_year']) < 15):
		header("Location:".PATH."/quickregister/age_limit");
		exit;
	endif;

	$Info = [$_POST['bean_day'], $_POST['bean_month'], $_POST['bean_year'], $Gender];
	$_SESSION['quickregister']['USER']['Step1'] = [$Info[0], $Info[1], $Info[2], $Info[3]];
	$Error = $Users->Step1($Info[0], $Info[1], $Info[2], $Info[3]);

	if(!$Error):
		$_SESSION['quickregister']['register_errors'] = "Por favor, debes proporcionar una fecha válida ";
		header("Location:".PATH."/quickregister/age_gate/e/05x");
	else:
		unset($_SESSION['quickregister']['step1']);

		$_SESSION['quickregister']['step2'] = "focus";
		header("Location:".PATH."/quickregister/email_password");
	endif;
endif;
?>