<?php
require '../../../KERNEL-XDRCMS/FastInit.php';
$cSettings = CACHE::GetAIOConfig('Client');

if($cSettings['managed.override.token'] != $_GET['token'])
	exit(header('Location: ' . PATH . '/managed-gamedata/override/external_override_variables/' . $cSettings['managed.override.token']));

header('Content-type: text/plain');
echo file_exists(KERNEL . '/Cache/Override.Variables.txt') ? file_get_contents(KERNEL . '/Cache/Override.Variables.txt') : '';
?>