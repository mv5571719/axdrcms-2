<?php
$PageName = 'Estadísticas';

$s = CACHE::GetStats();
$sSettings = CACHE::GetAIOConfig('Server');

require HTML . 'ACP_header.html';
?>
<div class="clearfix"></div>
<div style="width: 40%; float:left">
	<h2>Información del sitio.</h2>
	<table class="striped">
		<tbody>
			<tr>
				<td><b>Web Server:</b></td>
				<td><?php echo $_SERVER['SERVER_SOFTWARE']; ?><br>PHP <?php echo PHP_VERSION . ((PHP_INT_SIZE === 4) ? ' x86' : 'x64'); ?><br>MySQL <?php printf("%s\n", $MySQLi->server_info); ?> (PHP mysqli API)</td>
			</tr>
			<tr>
				<td><b>CMS Version:</b></td>
				<td>aXDR 2.0<br>Azure Core 3.0.3<br>Designed for PHP 5.4 or higher<br>d:20140916:1618</td>
			</tr>
			<tr>
				<td><b>Usuarios:</b></td>
				<td>Registrados: <a href="<?php echo HPATH; ?>/manage?p=users"><?php echo $s[0]; ?></a><br>Baneados: <a href="<?php echo HPATH; ?>/manage?p=users&filter=3c212d2d20747970653a62616e73202d2d3e"><?php echo $s[2]; ?></a><br>Con rango: <a href="<?php echo HPATH; ?>/manage?p=users&filter=3c212d2d2072616e6b202d2d3e"><?php echo $s[1]; ?></a><br>Conéctados: <?php echo USER::$Online; ?></td>
			</tr>
			<tr>
				<td><b>Salas:</b></td>
				<td><?php echo $s[9]; ?> furnis en <?php echo $s[8]; ?> salas</td>
			</tr>
			<tr>
				<td><b>Catálogo:</b></td>
				<td><?php echo $s[10]; ?> furnis a la venta</td>
			</tr>
			<tr>
				<td><b>Logs:</b></td>
				<td><?php echo $s[3]; ?></td>
			</tr>
			<tr>
				<td><b>Vouchers:</b></td>
				<td><?php echo $s[4]; ?></td>
			</tr>
			<tr>
				<td><b>Plugins:</b></td>
				<td><?php echo $s[5]; ?></td>
			</tr>
			<tr>
				<td><b>Styles:</b></td>
				<td><?php echo count($SiteSettings['Styles'])+1; ?></td>
			</tr>
			<tr>
				<td><b>Noticias:</b></td>
				<td>Artículos: <?php echo $s[6]; ?><br>Promos: <?php echo $s[7]; ?></td>
			</tr>
		</tbody>
	</table>
</div>
<div style="padding-left: 10px; width: 40%; float:left">
	<h2>Información del servidor.</h2>
	<table class="striped">
		<tbody>
			<tr>
				<td><b>Servidor</b></td>
				<td><?php echo $sSettings['TCP']['Server'] . ':' . $sSettings['TCP']['Port']; ?></td>
			</tr>
			<tr>
				<td><b>Server MUS</b></td>
				<td><?php echo (extension_loaded('sockets') && $sSettings['MUS']['Enabled']) ? ($sSettings['MUS']['Server'] . ':' . $sSettings['MUS']['Port']) : 'Desactivado'; ?></td>
			</tr>
				<td><b>Server SMTP</b></td>
				<td><?php echo ($Config['EnabledMails']) ? ($Config['SMTP'][2] . ':' . $Config['SMTP'][0]) : 'Desactivado'; ?></td>
			</tr>
		</tbody>
	</table>
	<h2>Información de tu usuario.</h2>
	<table class="striped">
		<tbody>
			<tr>
				<td><b>Usuario</b></td>
				<td><?php echo USER::$Data['Name'] . ' - ' . $staffRanks[USER::$Data['Rank']][0]; ?></td>
			</tr>
			<tr>
				<td><b>Navegador</b></td>
				<td><?php echo $_SERVER['HTTP_USER_AGENT']; ?></td>
			</tr>
			<tr>
				<td><b>Ip</b></td>
				<td><?php echo MY_IP; ?></td>
			</tr>
		</tbody>
	</table>
</div>