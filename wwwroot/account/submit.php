<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$no_rea = true;
require_once '../../KERNEL-XDRCMS/Init.php';

header('Connection:keep-alive');
header('Pragma:no-cache');
header('Cache-Control:no-cache, no-store, must-revalidate');
header('x-ua-compatible:IE=Edge');

if($siteBlocked):
	header('Location:' . PATH . '/error/blocked');
	exit;
endif;

$Error = False;
$UseEmail = False;
$Url = '';
require SCRIPT . 'login_submit.php';
?>