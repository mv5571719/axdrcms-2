<?php
$body_id = "news";
if(isset($_GET['URI'])):
	if($_GET['URI'] === 'archive'):
		$showAllArticles = true;
	elseif(strstr($_GET['URI'], '-') !== FALSE):
		$URI = explode('-', $_GET['URI'])[0];
		if(is_numeric($URI)):
			$articleId = $URI;
		endif;
	endif;
endif;
if(isset($articleId)):
	$query = $MySQLi->query('SELECT ID, Title, Summary, Body, Image, Images, Created FROM xdrcms_news WHERE ID = ' . $articleId);
else:
	$query = $MySQLi->query('SELECT ID, Title, Summary, Body, Image, Images, Created FROM xdrcms_news ORDER BY ID DESC LIMIT 1');
endif;
if($query && $query->num_rows):
	$queryRow = $query->fetch_assoc();
	$articleId = $queryRow['ID'];
	$pagename = 'Noticias - ' . $queryRow['Title'];
else:
	$articleId = 0;
	$queryRow = [
		'Title' => 'Artículo no encontrado',
		'Body' => 'El artículo que buscas no se ha encontrado. Por favor, pulsa en volver en tu navegador para retroceder a la página anterior.',
		'Image' => '',
		'Images' => '',
		'Created' => ''
	];
	$pagename = 'Noticias - Artículo no encontrado';
endif;
?>