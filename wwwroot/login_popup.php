<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$no_rea = true;
require '../KERNEL-XDRCMS/Init.php';
USER::REDIRECT(2);
CACHE::SetOnline(USER::$Online);
/*
if(!isset($_SESSION['login'])):
	$_SESSION['login']['enabled'] = true;
	$_SESSION['login']['tries'] = 0;
endif;
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo $hotelName; ?>: <?php echo $siteName; ?> </title>

<script type="text/javascript">
var andSoItBegins = (new Date()).getTime();
</script>
<link rel="shortcut icon" href="<?php echo webgallery; ?>/v2/favicon.ico" type="image/vnd.microsoft.icon" />
<link rel="alternate" type="application/rss+xml" title="Habbo: RSS" href="http://www.habbo.es/articles/rss.xml" />
<meta name="csrf-token" content="928963deb0"/>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/process.css" type="text/css" />
<script src="<?php echo webgallery; ?>/static/js/libs2.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/visual.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/libs.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/common.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/fullcontent.js" type="text/javascript"></script>

<script src="/js/local/es.js" type="text/javascript"></script>

<script type="text/javascript"> 
document.habboLoggedIn = <?php echo (USER::$LOGGED) ? 'true' : 'false'; ?>;
var habboName = <?php echo (USER::$LOGGED) ? '"' . USER::$Data['Name'] . '"' : 'null'; ?>;
var habboId = <?php echo (USER::$LOGGED) ? USER::$Data['ID'] : 'null'; ?>;
var habboReqPath = "<?php echo PATH; ?>";
var habboStaticFilePath = "<?php echo webgallery; ?>";
var habboImagerUrl = "<?php echo PATH; ?>/habbo-imaging/";
var habboPartner = "";
var habboDefaultClientPopupUrl = "<?php echo PATH; ?>/client";
window.name = "<?php echo (USER::$LOGGED) ? USER::$Row['client_token'] : 'client'; ?>";
if (typeof HabboClient != "undefined") {
    HabboClient.windowName = "<?php echo (USER::$LOGGED) ? USER::$Row['client_token'] : 'client'; ?>";
    HabboClient.maximizeWindow = true;
}
</script>

<meta property="fb:app_id" content="<?php echo $_Facebook['App']['ID']; ?>"> 

<meta property="og:site_name" content="<?php echo $hotelName; ?> Hotel" />
<meta property="og:title" content="<?php echo $hotelName; ?>: Home" />
<meta property="og:url" content="<?php echo PATH; ?>" />
<meta property="og:image" content="<?php echo PATH; ?>/v2/images/facebook/app_habbo_hotel_image.gif" />
<meta property="og:locale" content="es_ES" />

<meta name="description" content="<?php echo $siteName; ?>: haz amig@s, �nete a la diversi�n y date a conocer." />
<meta name="keywords" content="<?php echo strtolower($siteName); ?>, mundo, virtual, red social, gratis, comunidad, personaje, chat, online, adolescente, roleplaying, unirse, social, grupos, forums, seguro, jugar, juegos, amigos, adolescentes, raros, furni raros, coleccionable, crear, coleccionar, conectar, furni, muebles, mascotas, dise�o de salas, compartir, expresi�n, placas, pasar el rato, m�sica, celebridad, visitas de famosos, celebridades, juegos en l�nea, juegos multijugador, multijugador masivo" />


<!--[if IE 8]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/ie8.css" type="text/css" />
<![endif]-->
<!--[if lt IE 8]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/ie.css" type="text/css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/ie6.css" type="text/css" />
<script src="<?php echo webgallery; ?>/static/js/pngfix.js" type="text/javascript"></script>
<script type="text/javascript">
try { document.execCommand('BackgroundImageCache', false, true); } catch(e) {}
</script>

<style type="text/css">
body { behavior: url(/js/csshover.htc); }
</style>
<![endif]-->
<meta name="build" content="<?php echo $_XDRBuild; ?>" />
</head>
<body id="popup" class="process-template">

<div id="overlay"></div>

<div id="change-password-form" class="overlay-dialog" style="display: none;">
    <div id="change-password-form-container" class="clearfix form-container">
        <h2 id="change-password-form-title" class="bottom-border">�contraseña olvidada?</h2>
        <div id="change-password-form-content" style="display: none;">
            <form method="post" action="<?php echo SPATH; ?>/account/password/identityResetForm" id="forgotten-pw-form">
                <input type="hidden" name="page" value="/login_popup?changePwd=true" />
                <span>Por favor, introduce el email de tu <?php echo $hotelName; ?> cuenta:</span>
                <div id="email" class="center bottom-border">
                    <input type="text" id="change-password-email-address" name="emailAddress" value="" class="email-address" maxlength="48"/>
                    <div id="change-password-error-container" class="error" style="display: none;">Por favor, introduce un e-mail</div>
                </div>
            </form>
            <div class="change-password-buttons">
                <a href="#" id="change-password-cancel-link">Cancelar</a>
                <a href="#" id="change-password-submit-button" class="new-button"><b>Enviar email</b><i></i></a>
            </div>
        </div>
        <div id="change-password-email-sent-notice" style="display: none;">
            <div class="bottom-border">
                <span>Te hemos enviado un email a tu direcci�n de correo electr�nico con el link que necesitas clicar para cambiar tu contraseña.<br>
<br>

�NOTA!: Recuerda comprobar tambi�n la carpeta de 'Spam'</span>
                <div id="email-sent-container"></div>
            </div>
            <div class="change-password-buttons">
                <a href="#" id="change-password-change-link">Atr�s</a>
                <a href="#" id="change-password-success-button" class="new-button"><b>Cerrar</b><i></i></a>
            </div>
        </div>
    </div>
    <div id="change-password-form-container-bottom" class="form-container-bottom"></div>
</div>

<script type="text/javascript">
    function initChangePasswordForm() {
        ChangePassword.init();
    }
    if (window.HabboView) {
        HabboView.add(initChangePasswordForm);
    } else if (window.habboPageInitQueue) {
        habboPageInitQueue.push(initChangePasswordForm);
    }
</script>

<div id="container">
	<div class="cbb process-template-box clearfix">
		<div id="content" class="wide">
					<div id="header" class="clearfix">

						<h1><a href="<?php echo PATH; ?>/"></a></h1>
<ul class="stats">
    <li class="stats-online">�<span class="stats-fig"><?php echo USER::$Online; ?></span> <?php echo $hotelName; ?>s conectados ahora!</li>
</ul>
					</div>
			<div id="process-content">
	        	<div id="column1" class="column">
			     		
				<div class="habblet-container ">		
	
						<div class="cbb clearfix green">

    <h2 class="title">Reg�strate gratis</h2>
    <div class="box-content">
        <p>Reg�strate gratis haciendo clic en el bot�n 'Crea tu <?php echo $hotelName; ?>' de debajo. Si ya te has registrado, con�ctate en la parte derecha.</p>
        <div class="register-button clearfix">
            <a href="<?php echo PATH; ?>/register" onclick="HabboClient.closeHabboAndOpenMainWindow(this); return false;">Crea tu Habbo �</a>
            <span></span>
        </div>                
    </div>

</div>
	
						
					
				</div>
				<script type="text/javascript">if (!$(document.body).hasClassName('process-template')) { Rounder.init(); }</script>
			 

</div>
<div id="column2" class="column">
			     		
				<div class="habblet-container ">		
	
						<div class="logincontainer">

<div class="cbb loginbox clearfix">
    <h2 class="title">Con�ctate</h2>       
    <div class="box-content clearfix" id="login-habblet">

        <form action="<?php echo SPATH; ?>/account/submit" method="post" class="login-habblet">
            <input type="hidden" name="page" value="/client" />
            <ul>

                <li>
                    <label for="login-username" class="login-text">Email</label>
                    <input tabindex="1" type="text" class="login-field" name="credentials.username" id="login-username" value="" maxlength="48" />
                </li>

                <li>

                    <label for="login-password" class="login-text">contraseña</label>
                    <input tabindex="2" type="password" class="login-field" name="credentials.password" id="login-password" maxlength="32" />
                    <input type="submit" value="Conectar" class="submit" id="login-submit-button"/>
                    <a href="#" id="login-submit-new-button" class="new-button" style="margin-left: 0;display:none"><b style="padding-left: 10px; padding-right: 7px; width: 55px">Conectar</b><i></i></a>
                </li>



                
                <li>

<div id="fb-root"></div>
<script type="text/javascript">
    window.fbAsyncInit = function() {
        Cookie.erase("fbsr_<?php echo $_Facebook["App"]["ID"]; ?>");
        FB.init({appId: '<?php echo $_Facebook["App"]["ID"]; ?>', status: true, cookie: true, xfbml: true});
        if (window.habboPageInitQueue) {
            // jquery might not be loaded yet
            habboPageInitQueue.push(function() {
                $(document).trigger("fbevents:scriptLoaded");
            });
        } else {
            $(document).fire("fbevents:scriptLoaded");
        }

    };
    window.assistedLogin = function(FBobject, optresponse) {
        
        Cookie.erase("fbsr_<?php echo $_Facebook["App"]["ID"]; ?>");
        FBobject.init({appId: '<?php echo $_Facebook["App"]["ID"]; ?>', status: true, cookie: true, xfbml: true});

        permissions = 'user_birthday,email';
        defaultAction = function(response) {

            if (response.authResponse) {
                fbConnectUrl = "/facebook?connect=true";
                Cookie.erase("fbhb_val_<?php echo $_Facebook["App"]["ID"]; ?>");
                Cookie.set("fbhb_val_<?php echo $_Facebook["App"]["ID"]; ?>", response.authResponse.accessToken);
                Cookie.erase("fbhb_expr_<?php echo $_Facebook["App"]["ID"]; ?>");
                Cookie.set("fbhb_expr_<?php echo $_Facebook["App"]["ID"]; ?>", response.authResponse.expiresIn);
                window.location.replace(fbConnectUrl);
            }
        };

        if (typeof optresponse == 'undefined')
            FBobject.login(defaultAction, {scope:permissions});
        else
            FBobject.login(optresponse, {scope:permissions});

    };

    (function() {
        var e = document.createElement('script');
        e.async = true;
        e.src = document.location.protocol + '//connect.facebook.net/es_ES/all.js';
        document.getElementById('fb-root').appendChild(e);
    }());
</script>
<a class="fb_button fb_button_large" onclick="assistedLogin(FB); return false;">
    <span class="fb_button_border">
        <span class="fb_button_text">Conectar v�a Facebook</span>
    </span>
</a>
                </li>

                <li>

<div id="rpx-signin">
</div>                </li>

                <li id="remember-me" class="no-label">
                    <input tabindex="4" type="checkbox" name="_login_remember_me" id="login-remember-me" value="true"/>
                    <label for="login-remember-me">Mantener conectado</label>
                </li>
                <li id="register-link" class="no-label">
                    <a href="<?php echo PATH; ?>/quickregister/start" class="login-register-link" onclick="HabboClient.closeHabboAndOpenMainWindow(this); return false;"><span>Reg�strate gratis</span></a>
                </li>
                <li class="no-label">
                    <a href="<?php echo PATH; ?>/account/password/forgot" id="forgot-password"><span>�contraseña olvidada?</span></a>
                </li>
            </ul>
<div id="remember-me-notification" class="bottom-bubble" style="display:none;">
	<div class="bottom-bubble-t"><div></div></div>
	<div class="bottom-bubble-c">
                Seleccionando esta opci�n permanecer�s conectado a Habbo hasta que des a la opci�n 'Desconectar'
	</div>

	<div class="bottom-bubble-b"><div></div></div>
</div>
        </form>

    </div>
</div>
</div>
<script type="text/javascript">
L10N.put("authentication.form.name", "Email");
L10N.put("authentication.form.password", "contraseña");
HabboView.add(function() {LoginFormUI.init();});
HabboView.add(function() {window.setTimeout(function() {RememberMeUI.init("newfrontpage");}, 100)});
</script>
	
						
					
				</div>
				<script type="text/javascript">if (!$(document.body).hasClassName('process-template')) { Rounder.init(); }</script>

			 

</div>
<!--[if lt IE 7]>
<script type="text/javascript">
Pngfix.doPngImageFix();
</script>
<![endif]-->
<div id="footer">
	<p class="footer-links"><a href="http://xdrcms.es/" target="_new">XdrCMS</a> | <a href="http://azurecms.es/" target="_new">Azure</a> | <a href="<?php echo PATH; ?>/papers/termsAndConditions" target="_new">T�rminos y Condiciones</a> | <a href="<?php echo PATH; ?>/papers/privacy" target="_new">Pol�tica de Privacidad</a> | <a href="<?php echo PATH; ?>/safety/habbo_way" target="_new">La Manera Habbo</a>  | <a href="<?php echo PATH; ?>/groups/ConsejosdeSeguridad">Seguridad</a></p>
	<p class="copyright">� 2014 Xdr. Powered by <a href="http://xdr13.com/" target="_blank">XdrCMS 13</a>.<br />Habbo Hotel es una marca registrada de Sulake Corporation Oy. Todos los derechos reservados</p>
</div></div>
        </div>
    </div>
</div>
<script type="text/javascript">
if (typeof HabboView != "undefined") {
	HabboView.run();
}
</script>


</body>
</html>
