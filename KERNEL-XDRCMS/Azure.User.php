<?php
class USER{
	public static $LOGGED = false;

	public static $Data = ['ID' => 0, 'Name' => '', 'Email' => '', 'Rank' => 0, 'Motto' => '', 'Look' => '', 'Gender' => 'F', 'Credits' => 0, 'ICON' => 'icon_habbo_small'];
	public static $Row = [];
	public static $IsFB = false;

	private static $Security = null;
	public static $BanData;
	public static $Online = 0;

	private static $_Rights = [
		'ACP' => 2,
		'dashboard' => 2,
		'stats' => 2,
		'help' => 2,
		'logs' => 2,
		'users' => 2,
		'debug' => 2,
		'editdelete' => 3,
		'deleteLogs' => 4,
		'server' => 5,
		'loader' => 5,
		'site' => 6,
		'articles' => 7,
		'promos' => 7,
		'minimail' => 7,
		'styles' => 8,
		'editUsers' => 9,
		'ban' => 10,
		'unban' => 11,
		'vouchers' => 12,
		'giveRank' => 13,
		'ranktest' => 13,
		'emulator' => 13,
		'exchange' => 13,
		'override_varstexts' => 5,
		'plugins' => 14
	];

	public static function Exist($e, $p, $t, $d = 'null', &$P = '') {
		if($t === 'facebookid')
			$q = $GLOBALS['MySQLi']->query("SELECT " . $d . " FROM users, xdrcms_users_data WHERE xdrcms_users_data.AccountID = '" . $e . "' AND xdrcms_users_data.rpx_type = 'facebookid' AND users.id = xdrcms_users_data.id LIMIT 1");
		else
			$q = $GLOBALS['MySQLi']->query("SELECT " . $d . " FROM users, xdrcms_users_data WHERE xdrcms_users_data.mail = '" . $e . "' AND xdrcms_users_data.password = '" . $p . "' AND xdrcms_users_data.rpx_type = '" . $t . "' AND users.id = xdrcms_users_data.id LIMIT 1");

		if($q && $q->num_rows === 1):
			if($d != 'null'):
				$P = $q->fetch_assoc();
			endif;

			return true;
		endif;

		return false;
	}

	public static function UsernameExist($e, $p, $t, $d = 'null', &$P = '') {

		$q = $GLOBALS['MySQLi']->query("SELECT " . $d . " FROM users, xdrcms_users_data WHERE users.username = '" . $e . "' AND xdrcms_users_data.password = '" . $p . "' AND xdrcms_users_data.rpx_type = '" . $t . "' AND users.id = xdrcms_users_data.id LIMIT 1");

		if($q && $q->num_rows === 1):
			if($d != 'null'):
				$P = $q->fetch_assoc();
			endif;

			return true;
		endif;

		return false;
	}

	public static function IsEmail($e) {
		return (preg_match("/^[a-z0-9_\.-]+@([a-z0-9]+([\-]+[a-z0-9]+)*\.)+[a-z]{2,7}$/i", $e)) ? true : false;
	}
	
	public static function GetData($i, $v = 'username', $c = 'id', $o = '', $t = 'users') {
		$q = $GLOBALS['MySQLi']->query('SELECT ' . $v . ' FROM ' . $t . ' WHERE ' . $c . ' = \'' . $i . '\'' . $o . ' LIMIT 1');
		return (!$q || $q->num_rows === 0) ? null : $q->fetch_assoc();
	}
	
	public static function SomeRegistered($e, $t = 'habboid', $d = ['xdrcms_users_data', 'mail']) {
		$t = (!empty($t)) ? ' AND xdrcms_users_data.rpx_type = \'' . $t . '\'' : '';
		$q = $GLOBALS['MySQLi']->query('SELECT null FROM ' . $d[0] . ' WHERE ' . $d[1] . ' = \'' . $e . '\'' . $t . ' LIMIT 1');
		return (($q === FALSE) ? null : ($q->num_rows === 1));
	}
	
	public static function IsOnline($i) {
		if($GLOBALS['Config']['OnlineType'] < 2):
			$q = $GLOBALS['MySQLi']->query('SELECT null FROM users WHERE online = \'1\' WHERE id = ' . $i);
			return ($q && $q->num_rows === 1);
		elseif($GLOBALS['Config']['OnlineType'] === 2):
			$q = $GLOBALS['MySQLi']->query('SELECT null FROM user_online WHERE userid = ' . $i);
			return ($q && $q->num_rows === 1);
		elseif($GLOBALS['Config']['OnlineType'] === 3):
			return false;
		endif;
		
		return false;
	}
	
	public static function IsBanned($u, $i) {
		$q = $GLOBALS['MySQLi']->query('SELECT id, reason, expire FROM bans WHERE ((value = \'' . $u . '\' AND bantype = \'user\') OR (value = \'' . $i . '\' AND bantype = \'ip\')) LIMIT 1');

		if(!$q || $q->num_rows !== 1)
			return;

		self::$BanData = $q->fetch_assoc();
		
		if((self::$BanData['expire'] - time()) <= 0)
			$GLOBALS['MySQLi']->query('DELETE FROM bans WHERE id = ' . self::$BanData['id']);
		else
			return true;

		return false;
	}

	public static function GetSecurity($d) {
		if(self::$Security === null): self::$Security = json_decode(self::$Row['securityTokens'], true); endif;
		return self::$Security[$d];
	}
	
	public static function CheckAge($d, $m, $y) {
		if(is_numeric($d) && is_numeric($m) && is_numeric($y))
			return checkdate($m, $d, $y);
		else
			return false;
	}
	
	public static function GenerateName($e, $t = 0) {
		$e = $E = substr((strstr($e, '@')) ? explode('@', $e)[0] : $e, 0, 18);
		$c = ['.', '-', '_'];

		if(strlen($e) > 6):
			foreach($c as $C):
				if(strstr($e, $C) === FALSE) continue;
				$n = rand(0, 1);
				$e = explode($C, $e);
				$e = strlen($e[$n]) < 3 ? ($e[($n === 1) ? 0 : 1]) : $e[$n];
				break;
			endforeach;
		endif;
		
		$r = self::SomeRegistered($e, '', ['users', 'username']);
		if($r === null)	return null;
		if($r === false)	return $e;
		
		$c = strlen($e);
		$r = ($c > 10) ? rand(rand(1, 2 + $t), rand(2, 3 + $t)) : ($c > 8) ? rand(rand(2, 2 + $t), rand(3, 3 + $t)) : rand(rand(3, 4 + $t), rand(3, 4 + $t));

		$e .= METHOD::RANDOM($r, true, false);
		$r = self::SomeRegistered($e, '', ['users', 'username']);
		if($r === null)	return null;
		if($r === false)	return $e;
		self::GenerateName($E, $t++);
	}
	
	public static function GenerateAuthToken($i = 0) {
		$t = METHOD::RANDOM(8) . '-' . METHOD::RANDOM(4) . '-' . METHOD::RANDOM(4) . '-' . METHOD::RANDOM(4) . '-' . METHOD::RANDOM(12) . '-AX2';
		if($i !== 0):
			$GLOBALS['MySQLi']->query('UPDATE users SET auth_ticket = \'' . $t . '\' WHERE id = ' . $i);
			$GLOBALS['MySQLi']->query('REPLACE INTO user_tickets (userid, sessionticket, ipaddress) VALUES (' . $i . ', \'' . $t . '\', \'' . MY_IP . '\')');
		endif;
		
		return $t;
	}
	
	public static function NewRefer($i, $r){
		global $MySQLi;

		$u = $MySQLi->query('SELECT ReferIds FROM xdrcms_users_refers WHERE UserId = ' . $r . ' LIMIT 1');
		if(!$u || $u->num_rows === 0):
			$MySQLi->query('INSERT INTO xdrcms_users_refers (UserId, Count, ReferIds) VALUES (' . $r . ', 1, \'' . json_encode([$i]) . '\')');
		else:
			$j = json_decode($u->fetch_assoc()['ReferIds'], true);
			$j[] = $i;
			$MySQLi->query('UPDATE xdrcms_users_refers SET Count = Count + 1, ReferIds = \'' . json_encode($j) . '\' WHERE UserId = ' . $r);
		endif;
		$MySQLi->query('UPDATE users SET credits = credits + 2500 WHERE id = ' . $r);
		//newTransaction($r, time(), '2500', 'Nuevo referido, Id: ' . $i));
	}
	
	public static function GetRefersIds($i) {
		$userRefers = $GLOBALS['MySQLi']->query('SELECT ReferIds FROM xdrcms_users_refers WHERE UserId = ' . $i . ' LIMIT 1');
		return ($userRefers && $userRefers->num_rows === 1) ? json_decode($userRefers->fetch_assoc()['ReferIds'], true) : null;
	}

	public static function GetRefersCount($i = 0) {
		$q = $GLOBALS['MySQLi']->query('SELECT Count FROM xdrcms_users_refers WHERE UserId = ' . $i . ' LIMIT 1');
		return ($q && $q->num_rows === 1) ? $q->fetch_assoc()['Count'] : 0;
	}
	
	public static function HaveWidget($i, $v) {
		$q = $GLOBALS['MySQLi']->query('SELECT null FROM xdrcms_site_items WHERE var = \'' . $v . '\' AND (userId = \'' . $i . '\') LIMIT 1'); //  OR groupId = '" . $Id . "'
		return ($q && $q->num_rows === 1);
	}
	
	public static function CAN($r, $R = 0) {
		if(!isset(self::$_Rights[$r]))	return ($R < $GLOBALS['MinRank']) ? false : $GLOBALS['staffRanks'][$R][1];
		$R = ($R === 0) ? self::$Data['Rank'] : $R;
		return ($R < $GLOBALS['MinRank'] || !isset($GLOBALS['staffRanks'][$R])) ? false : $GLOBALS['staffRanks'][$R][self::$_Rights[$r]];
	}

	public static function REDIRECT($t) {
		if(self::$LOGGED === true):
			if($t === 2): header('Location: ' . PATH . '/me'); exit; endif;
		else:
			if($t === 1): header('Location: ' . PATH); exit; endif;
			if($t === 3): header('Location: ' . PATH . '/login_popup'); exit; endif;
		endif;
	}
	
	public static function LOGIN($ID, $e, $p, $t, $r = false, $o = '') {
		$c = '';
		if($r):
			$c = strtolower(METHOD::RANDOM(128));
			setcookie('rememberme', 'true', time() + (3600*24*14), '/');
			setcookie('rememberme_token', $c, time() + (3600*24*14), '/');
		endif;

		$_SESSION['special_login'] = $t;
		$_SESSION['email'] = $e;
		$_SESSION['password'] = $p;
		$_SESSION['webonline'] = time();
		$_SESSION['LastUsed'] = time();
		$_SESSION['token'] = METHOD::RANDOM(10);
		$_SESSION['id'] = $ID;

		$GLOBALS['MySQLi']->query('UPDATE users, xdrcms_users_data SET 
					xdrcms_users_data.client_token = \'' . METHOD::RANDOM(40) . '\', 
					xdrcms_users_data.RememberMeToken = \'' . $c . '\', 
					xdrcms_users_data.token = \'' . $_SESSION['token'] . '\', 
					xdrcms_users_data.web_online = ' . time() . ', 
					users.ip_last = \'' . MY_IP . '\' ' . $o .'
					WHERE users.id = ' . $ID . ' AND xdrcms_users_data.id = ' . $ID);
	}

	public static function CHECK() {
		if(!isset($_SESSION['email'], $_SESSION['id'], $_SESSION['password'], $_SESSION['special_login'])):
			if(isset($_COOKIE['rememberme'], $_COOKIE['rememberme_token'])): self::_CHECKCOOKIE(); endif;
			return;
		endif;

		global $MySQLi;

		$usersql = $MySQLi->query('
			SELECT users.username, users.rank, users.credits, users.account_created, users.block_newfriends, users.ip_last, users.look, users.gender, users.motto, users.online, users.hide_online, xdrcms_users_data.* 
			FROM users, xdrcms_users_data 
			WHERE users.id = ' . $_SESSION['id'] . ' AND (xdrcms_users_data.mail = \'' . $_SESSION['email'] . '\' OR users.username = \'' . $_SESSION['email'] . '\') AND xdrcms_users_data.password = \'' . (($_SESSION['special_login'] !== 'facebookid') ? $_SESSION['password'] : '') . '\' AND xdrcms_users_data.predermited = \'1\' AND xdrcms_users_data.rpx_type = \'' . $_SESSION['special_login'] . '\' AND xdrcms_users_data.id = users.id 
			LIMIT 1');
		$icon = ($_SESSION['special_login'] === 'facebookid') ? 'icon_facebook_connect_small' : 'icon_habbo_small';
		
		if($usersql === FALSE || $usersql->num_rows !== 1):
			unset($_SESSION['email'], $_SESSION['id'], $_SESSION['password'], $_SESSION['special_login']);
			$_SESSION['Index']['Login']['Error']['Name'] = 'Ha ocurrido un error:368';
			header('Location: ' . PATH); exit;
		endif;
		
		$Row = $usersql->fetch_assoc();
		
		self::_CHECKINACTIVITY($Row['token']);
		$_SESSION['LastUsed'] = time();

		if(!isset($GLOBALS['no_rea']) && $Row['token'] !== $_SESSION['token']):
			header('Location: ' . PATH . '/account/logout?reason=concurrentlogin&token=' . $Row['token']);
			exit();
		endif;
		
		if(self::IsBanned($Row['username'], MY_IP)):
			unset($_SESSION['email'], $_SESSION['id'], $_SESSION['password'], $_SESSION['special_login']);
			$_SESSION['Index']['Login']['Error']['Name'] = '¡Has sido baneado! La Razón es: "' . self::$BanData['reason'] . '", y acabará en ' . date('d/m/Y H:i:s', self::$BanData['expire']) . '.';
			header('Location: ' . PATH . '/?username=' . $Row['mail'] . '&rememberme=false&focus=login-password');
			exit;
		endif;

		self::$LOGGED = true;			
		self::$Data = ['ID' => $Row['id'], 'Name' => $Row['username'], 'Email' => $Row['mail'], 'Rank' => $Row['rank'], 'Motto' => $Row['motto'], 'Look' => $Row['look'], 'Gender' => $Row['gender'], 'Credits' => $Row['credits'], 'ICON' => $icon
		];
		self::$Row = $Row;
		self::$IsFB = ($_SESSION['special_login'] === 'facebookid');
	}
	
	static function _CHECKINACTIVITY($t) {
		if(isset($GLOBALS['InSecurity']))	return;
		if($GLOBALS['SiteSettings']['reload.time'] < 1500) return;
		if((time() - $_SESSION['LastUsed']) < $GLOBALS['SiteSettings']['reload.time']) return;
		
		if(isset($_COOKIE['rememberme'], $_COOKIE['rememberme_token'])): self::_CHECKCOOKIE(); endif;

		header('Location: ' . PATH . '/account/logout?token=' . $t . '&reason=expired');
		exit;
	}
	
	static function _CHECKCOOKIE() {
		if(!strlen($_COOKIE['rememberme_token']) === 128 || $_COOKIE['rememberme'] != 'true'):
			setcookie('rememberme_token', '', time() - 3600, '/'); setcookie('rememberme', '', time() - 3600, '/');
			return;
		endif;

		$Token = str_replace(['<', '>', '"', '\'', '\\'], ['&lt;', '&gt;', '&quot;', '&#39;', '&#92;'], $_COOKIE['rememberme_token']);
		$Query = $GLOBALS['MySQLi']->query('SELECT users.id, xdrcms_users_data.mail, xdrcms_users_data.password FROM users, xdrcms_users_data WHERE xdrcms_users_data.RememberMeToken = \'' . $Token . '\' AND users.ip_last = \'' . MY_IP . '\'');
		if($Query == null || $Query->num_rows === 0):
			setcookie('rememberme_token', '', time() - 3600, '/'); setcookie('rememberme', '', time() - 3600, '/');
			return;
		endif;
		
		$Row = $Query->fetch_assoc();
		self::LOGIN($Row['id'], $Row['mail'], $Row['password'], 'habboid', true);

		header('Location: ' . PATH . '/account/submit');
		exit;
	}
}
?>