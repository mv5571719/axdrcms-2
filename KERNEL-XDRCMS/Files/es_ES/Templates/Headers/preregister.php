<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo $hotelName; ?>:  </title>

<script type="text/javascript">
var andSoItBegins = (new Date()).getTime();
</script>
<link rel="shortcut icon" href="http://images.habbo.com/habboweb/63_1dc60c6d6ea6e089c6893ab4e0541ee0/1210/web-gallery/v2/favicon.ico" type="image/vnd.microsoft.icon" />
<link rel="alternate" type="application/rss+xml" title="Habbo Hotel - RSS" href="http://www.habbo.com.tr/articles/rss.xml" />

<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/common.css" type="text/css" />
<script src="<?php echo webgallery; ?>/static/js/libs2.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/visual.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/libs.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/common.js" type="text/javascript"></script>


<script type="text/javascript">
var ad_keywords = "";
var ad_key_value = "";
</script>
<script type="text/javascript">
document.habboLoggedIn = false;
var habboName = null;
var habboId = null;
var habboReqPath = "";
var habboStaticFilePath = "<?php echo webgallery; ?>";
var habboImagerUrl = "<?php echo PATH; ?>/habbo-imaging/";
var habboPartner = "";
var habboDefaultClientPopupUrl = "http://www.habbo.com.tr/client";
window.name = "client";
if (typeof HabboClient != "undefined") {
    HabboClient.windowName = "client";
    HabboClient.maximizeWindow = true;
}


</script>

<meta property="fb:app_id" content="368598699868806" />
<meta property="fb:admins" content="100002459206074" />
<meta property="og:site_name" content="Habbo Hotel" />
<meta property="og:title" content="Habbo Hotel - " />
<meta property="og:url" content="<?php echo PATH; ?>" />
<meta property="og:image" content="http://www.habbo.com.tr/v2/images/facebook/app_habbo_hotel_image.gif" />
<meta property="og:locale" content="tr_TR" />

<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/preregister.css" type="text/css" />

<meta name="description" content="" />
<meta name="keywords" content="" />

<script type="text/javascript" src="https://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>

<!--[if IE 8]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/ie8.css" type="text/css" />
<![endif]-->
<!--[if lt IE 8]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/ie.css" type="text/css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/ie6.css" type="text/css" />
<script src="<?php echo webgallery; ?>/static/js/pngfix.js" type="text/javascript"></script>
<script type="text/javascript">
try { document.execCommand('BackgroundImageCache', false, true); } catch(e) {}
</script>

<style type="text/css">
body { behavior: url(/js/csshover.htc); }
</style>
<![endif]-->
<meta name="build" content="63-BUILD-FOR-PATCH-1210a - 16.08.2012 11:23 - tr" />
</head>

<body id="client">
<div id="overlay"></div>
<img src="<?php echo webgallery; ?>/v2/images/page_loader.gif" style="position:absolute; margin: -1500px;" />

<div id="change-password-form" style="display: none;">
    <div id="change-password-form-container" class="clearfix">
        <div id="change-password-form-title" class="bottom-border"><?php echo $lans['XdrCms']['change-password-form-title']; ?></div>
        <div id="change-password-form-content" style="display: none;">
            <form method="post" action="<?php echo PATH; ?>/account/password/identityResetForm" id="forgotten-pw-form">
                <input type="hidden" name="page" value="<?php echo $_SERVER["REQUEST_URI"]; ?>?changePwd=true" />
                <span><?php echo $lans['XdrCms']['change-password-email-address']; ?></span>

                <div id="email" class="center bottom-border">
                    <input type="text" id="change-password-email-address" name="emailAddress" value="" class="email-address" maxlength="48"/>
                    <div id="change-password-error-container" class="error" style="display: none;"><?php echo $lans['XdrCms']['change-password-error-container']; ?></div>
                </div>
            </form>
            <div class="change-password-buttons">
                <a href="#" id="change-password-cancel-link"><?php echo $lans['XdrCms']['change-password-cancel-link']; ?></a>
                <a href="#" id="change-password-submit-button" class="new-button"><b><?php echo $lans['XdrCms']['change-password-submit-button']; ?></b><i></i></a>

            </div>
        </div>
        <div id="change-password-email-sent-notice" style="display: none;">
            <div class="bottom-border">
                <span>
<?php if($Config['EnabledMails']) { ?>
Te hemos enviado un email a tu direcci�n de correo electr�nico con el link que necesitas clicar para cambiar tu contraseña.<br>

<br>

�NOTA!: Recuerda comprobar tambi�n la carpeta de 'Spam'</span>
<?php } else { ?>
No te hemos enviado el email a tu direcci�n porque el servidor SMTP de <?php echo $hotelName; ?> no est� disponible. Intentelo más tarde.
<?php }?>
</span>
                <div id="email-sent-container"></div>
            </div>
            <div class="change-password-buttons">
                <a href="#" id="change-password-change-link">Atr�s</a>
                <a href="#" id="change-password-success-button" class="new-button"><b>Cerrar</b><i></i></a>
            </div>

        </div>
    </div>
    <div id="change-password-form-container-bottom"></div>
</div>

<script type="text/javascript">
HabboView.add( function() {
     ChangePassword.init();


});
</script>