<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

require "../KERNEL-XDRCMS/Init.php";

$pagename = 'Comunidad';
$pageid = 'community';

require HEADER . 'community.php';
require HTML . 'Community_community.html';
require FOOTER . 'community.php';
?>