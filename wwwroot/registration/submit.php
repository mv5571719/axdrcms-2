<?php
$LOAD = ['Azure.Items'];
require '../../KERNEL-XDRCMS/Init.php';

if($siteBlocked):
	header('Location:' . PATH . '/error/blocked');
	exit;
endif;

header('Content-Type: application/json;');

if(USER::$LOGGED):
	echo '{"registrationCompletionRedirectUrl":"' . str_replace("/", "\\/", PATH) . '\/me"}';
	exit;
endif;

if(isset($_SESSION['Registration']['Started']) === false):
	echo '{"registrationErrors":{}}';
	exit;
endif;

require SCRIPT . 'registration_submit.php';
?>