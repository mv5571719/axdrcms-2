<?php
if($do === 'view' && is_numeric($key)):
	$testrankQuery = $MySQLi->query("SELECT Id, Content, UserId FROM xdrcms_users_testrank WHERE Id = '" . $key . "' AND Status = '0'");
	if($testrankQuery->num_rows == 0):
		unset($testrankQuery);
	endif;
elseif($do == "rechaze" && is_numeric($key)):
	$MySQLi->query("UPDATE xdrcms_users_testrank SET Content = '', Status = '1' WHERE Id = '" . $key . "'");
	$MySQLi->query('INSERT INTO xdrcms_staff_log (action,message,note,userid,targetid,timestamp) VALUES (\'ACP\',\'Declined a rank petition ID: ' . $key . '\',\'ranktest.php[decline]\', ' . USER::$Data['ID'] . ', 0,' . time() .')');
elseif($do == "aprove" && is_numeric($key) && isset($_GET["Rank"])):
	if(!checkAntiCsrf()):
		echo 'An error occurred. Security token failed.';
		exit;
	endif;

	$Rank = $_GET['Rank'];
	if($Rank < $MinRank || $Rank > $MaxRank):
		exit;
	elseif(!USER::CAN('giverank')):
		exit;
	elseif($Rank >= USER::$Data['Rank']):
		exit;
	else:
		$_UserId = $MySQLi->query("SELECT UserId FROM xdrcms_users_testrank WHERE Id = '" . $key . "'  AND Status = '0'");
		if($_UserId->num_rows == 0):
			exit;
		endif;

		$UserId = $_UserId->fetch_assoc()['UserId'];
		$MySQLi->multi_query('UPDATE xdrcms_users_testrank SET Content = \'\', Status = \'2\' WHERE Id = \'' . $key . '\'; UPDATE users SET rank = \'' . $Rank . '\' WHERE id = \'' . $UserId . '\';INSERT INTO xdrcms_staff_log (action,message,note,userid,targetid,timestamp) VALUES (\'ACP\',\'Raised the rank of userID: ' . $UserId . ' to 7\',\'ranktest.php[accept]\', ' . USER::$Data['ID'] . ', 0,' . time() .')');
	endif;
	
	exit;
endif;

$PageName = 'Ranks Petitions';
require HTML . 'ACP_header.html';
?>
<script type="text/javascript">
var rankSelected = 4;

function UpdateRank(rankId){
	rankSelected = rankId;
}

function Aprove(Id){
	get("<?php echo HPATH; ?>/manage?p=ranktest&do=aprove&key=" + Id + "&Rank=" + rankSelected, "POST", "p=ranktest&do=aprove&key=" + Id + "&Rank=" + rankSelected);
	document.getElementById("accordionid").innerHTML = "";
	document.getElementById("petition-" + Id).innerHTML = "";
}
</script>
				<table class="striped">
                    <thead>
                        <tr>
                            <th>Id</th>
							<th class="right">Usuario</th>
                            <th class="right">Referidos</th>
                            <th class="right">Acciones</th>
                        </tr>
                    </thead>
<?php
$testsrankQuery = $MySQLi->query("SELECT Id, UserId FROM xdrcms_users_testrank WHERE Status = '0' ORDER BY Id DESC LIMIT 15");

while ($Row = $testsrankQuery->fetch_assoc()){
			?>
                        <tr id="petition-<?php echo $Row['Id']; ?>">
							<td><?php echo $Row['Id']; ?></td>
							<td class="right"><?php echo USER::GetData($Row['UserId'], 'username')['username']; ?></td>
							<td class="right"><?php echo USER::GetRefersCount($Row["UserId"]); ?></td>
							<td class="right"><a href="<?php echo HPATH; ?>/manage?p=ranktest&do=view&key=<?php echo $Row['Id']; ?>">Ver</a></td>
						</tr>
<?php } ?>
                    </tbody>

                    <tfoot></tfoot>
                </table>
<?php
if(isset($testrankQuery)):
	$Row = $testrankQuery->fetch_assoc();
?>
		<ul class="accordion dark" id="accordionid" data-role="accordion">
            <li class="accordionli active">
                <a onclick="ChangeAccordion(this)" href="javascript:void(0)"><span></span> Peticion #<?php echo $Row['Id'] . ": " . USER::GetData($Row['UserId'], 'username')["username"]; ?></a>
                <div class="accordionccn">
					<fieldset>
                        <legend>Informe</legend>
                        <p class="tertiary-info-secondary-text">
                            <?php echo $Row["Content"]; ?>
                        </p>
                    </fieldset>
					Aprobar dandole el rango: <select>
						<option value="4" onclick="UpdateRank(4)">Guide</option>
						<option value="5" onclick="UpdateRank(5)">Staff</option>
						<option value="6" onclick="UpdateRank(6)">Manager</option>
						<?php if($sysadmin == USER::$Data['ID']): ?><option value="7" onclick="UpdateRank(7)">Administrator</option><?php endif; ?>
					</select> <a href="#" onclick="Aprove(<?php echo $Row['Id']; ?>)" >Aceptar</a>
					<br />
					<a href="<?php echo HPATH; ?>/manage?p=ranktest&do=rechaze&key=<?php echo $Row["Id"]; ?>">Rechazar</a>
				</div>
			</li>
		</ul>
<?php endif; ?>