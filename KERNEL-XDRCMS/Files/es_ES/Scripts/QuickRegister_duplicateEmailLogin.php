<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright � 2012 Xdr.
|+=========================================================+
|| # Xdr 2012. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

require_once('../includes/core.php');

$inLogin = true;

if($do == "submit")
{
	$email = FilterText($_POST['credentials.username']);
	$password = HoloHash(FilterText($_POST['credentials.password']));

	$check = mysql_query("SELECT id,rpxid,rpx_type,mail,suspend FROM users WHERE (mail = '".$email."') AND password = '".$password."' LIMIT 1") or die(mysql_error());
	$exist = mysql_num_rows($check);

	if(empty($email) && empty($password))
	{
		$_SESSION['quickregister']['register_errors'] = "Por favor, introduce tu email y contraseña para conectarte.";
		$error = true;
	} 			
	else if(empty($email))
	{
		$_SESSION['quickregister']['register_errors'] = "Por favor, escribe tu email";
		$error = true;
	} 
	else if(empty($password))
	{
		$_SESSION['quickregister']['register_errors'] = "Por favor, escribe tu contraseña";
		$error = true;
	}
	else if($exist == 0)
	{
		$_SESSION['quickregister']['register_errors'] = "Tu contraseña y email no coinciden. Para leer más sobre problemas de conexi�n, por favor haz clic <a href=\"http://www.habbo.es/help/25\">aqu�.</a>";
		$error = true;
	}

	if($error == true)
	{
		header("Location:".PATH."/quickregister/login_submit");
	}
	else
	{	
		$_SESSION['special_login'] = "habboid";
		$_SESSION['email'] = $email;
		$_SESSION['password'] = $password;

		unset($_SESSION['quickregister']['register_errors']);

		$_SESSION['quickregister']['check_username'] = $email;
		$_SESSION['quickregister']['check_password'] = $password;

		$_SESSION['client_login'] = true;
		$_SESSION['next_login'] = true;
	
		header("location:".PATH."/security_check"); exit;
	}
}

require_once('../templates/quickregister_subheader.php');
?>

<div id="main-container" class="login">

<div class="cbb">
    <div id="alert-container" class="rounded">
        <div id="alert-title" class="notice">
            Esta cuenta ya existe
        </div>
        <div id="alert-text">

            �Intentas crear una nueva <?php echo $hotelName; ?> cuenta? Reg�strate con tu cuenta actual y a�ade más personajes a trav�s de Ajustes / Ajustes <?php echo $hotelName; ?> Cuenta.
        </div>
    </div>
</div>

    <div id="title">
        Registro en <?php echo $hotelName; ?>
    </div>

    <div id="inner-container" class="clearfix">
        <form method="post" action="<?php echo SPATH; ?>/quickregister/login_submit" id="quickregister-login" method="post" onsubmit="Overlay.show(null,'Cargando...');">

            <input type="hidden" name="emailOnlyLogin" value="true" />

                <input type="hidden" name="next" value="/client"/>



            <div id="login-container" class="field-content clearfix">
                <div class="left">
                    <div class="field">
                        <div class="label" class="login-text">Email:</div>

                        <input type="text" id="login-username" name="credentials.username" value="<?php echo $_SESSION['quickregister']['error']['duplicateEmail']; ?>"  />
                    </div>
                    <div class="field">
                        <div class="label" class="registration-text">contraseña:</div>
                        <input type="password" name="credentials.password" id="login-password" maxlength="32" value="" />
                    </div>
                    <div id="login-forgot-password">
                        <a href="<?php echo SPATH; ?>/account/password/forgot" id="forgot-password"><span>�contraseña olvidada?</span></a>

                    </div>
                </div>
                <div class="right text-right">
                </div>
            </div>
            <input type="submit" style="position:absolute; margin: -1500px;"/>            
        </form>
    </div>

    <div id="select">

            <a href="<?php echo SPATH; ?>/quickregister/backToAccountDetails" id="back-link">Atr�s</a>
        <div class="button">
            <a id="proceed-button" href="#" class="area">Finalizar</a>
            <span class="close"></span>
        </div>
   </div>
</div>

<script type="text/javascript">
        if ($("login-username").value.length == 0) {
            $("login-username").focus();
        } else {
            $("login-password").focus();
        }


    if (!!$("back-link")) {
        Event.observe($("back-link"), "click", function() {
            Overlay.show(null,'Cargando...');
        });
    }

    Event.observe($("proceed-button"), "click", function() {
        Overlay.show(null,'Cargando...');
        $("quickregister-login").submit();
    });

</script>


<script src="https://ssl.google-analytics.com/ga.js" type="text/javascript"></script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-448325-19");
pageTracker._trackPageview();
</script>

<script type="text/javascript">
    HabboView.run();
</script>

</body>
</html>