<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$no_maintenance = true;
$no_rea = true;

require "../KERNEL-XDRCMS/Init.php";

header("Content-type: text/xml"); 
?>
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.google.com/schemas/sitemap/0.84"><url><loc><?php echo PATH; ?>/community</loc><changefreq>daily</changefreq><priority>0.7</priority></url><url><loc><?php echo PATH; ?>/community/socialmedia</loc><changefreq>daily</changefreq><priority>0.5</priority></url><url><loc><?php echo PATH; ?>/community/fansites</loc><changefreq>daily</changefreq><priority>0.5</priority></url><url><loc><?php echo PATH; ?>/safety</loc><changefreq>daily</changefreq><priority>0.7</priority></url><url><loc><?php echo PATH; ?>/safety/safety_tips</loc><changefreq>daily</changefreq><priority>0.5</priority></url><url><loc><?php echo PATH; ?>/safety/habbo_way</loc><changefreq>daily</changefreq><priority>0.5</priority></url><url><loc><?php echo PATH; ?>/groups/CentroDeSeguridad</loc><changefreq>daily</changefreq><priority>0.5</priority></url><url><loc><?php echo PATH; ?>/credits</loc><changefreq>daily</changefreq><priority>0.7</priority></url></urlset>