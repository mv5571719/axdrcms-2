<?php
class ITEM {
	public static function CREATE($i, $v, $s, $t)
	{
		$GLOBALS['MySQLi']->query('INSERT INTO xdrcms_site_inventory_items (userId, var, skin, type) VALUES (' . $i . ', \'' . $v . '\', \'' . $s . '\', \'' . $t . '\')');
	}
	
	public static function PLACE($h, $g, $pl, $pt, $pz, $v, $s, $c, $t) {
		$GLOBALS['MySQLi']->query('INSERT INTO xdrcms_site_items (userId, groupId, position_left, position_top, position_z, var, skin, content, type) 
			VALUES (' . $h . ', ' . $g . ', ' . $pl . ', ' . $pt . ', ' . $pz . ', \'' . $v . '\', \'' . $s . '\', \'' . $c . '\', \'' . $t . '\')');
	}
	
	public static function UPDATE($t, $c) { //$t>d|s|t
		$q = 'UPDATE xdrcms_site_items SET ';
		if($t === 'd'):
			$q .= ' userId = ' . $c[1] . ', groupId = ' . $c[2] . ', position_left = ' . $c[3] . ', position_top = ' . $c[4] . ', position_z = ' . $c[5] . '';
		elseif($t === 's'):
			$q .= ' userId = ' . $c[1] . ', groupId = ' . $c[2] . ', skin = \'' . $c[3] .'\'';
		elseif($t === 't'):
			$q .= ' Temporal = \'False\'';
		endif;

		$q .= ' WHERE id = ' . $c[0] . ' LIMIT 1';
		$GLOBALS['MySQLi']->query($q);
	}

	public static function RESTOREWAITING($i) {
		$GLOBALS['MySQLi']->query('UPDATE xdrcms_site_inventory_items SET isWaiting = \'0\' WHERE userId = ' . $i);
	}
	
	public static function REMOVEWAITING($i, $I) {
		$GLOBALS['MySQLi']->query('DELETE FROM xdrcms_site_inventory_items WHERE userId = ' . $i . ' AND id = ' . $I . ' AND isWaiting = \'1\' LIMIT 1');
	}
}
?>