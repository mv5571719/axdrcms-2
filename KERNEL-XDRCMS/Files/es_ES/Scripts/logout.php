<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2013 Xdr.
|+=========================================================+
|| # Xdr 2013. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$pagename = "Reiniciar";

$token = (isset($_GET['token'])) ? $_GET['token'] : "";
$reason = (isset($_GET['reason'])) ? $_GET['reason'] : "";

if(USER::$LOGGED === true && $token == USER::$Row['token']):
	$MySQLi->query('UPDATE users SET online = \'0\' WHERE id = ' . USER::$Data['ID'] . ' LIMIT 1');
endif;

if(!empty($reason)):
	header('Location: ' . PATH . '/account/logout_ok?reason=' . $reason . '&token=' . $token);
else:
	header('Location: ' . PATH . '/account/logout_ok?token=' . $token);
endif;
?>