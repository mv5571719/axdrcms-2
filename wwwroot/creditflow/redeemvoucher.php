<?php
if(!isset($_POST['voucherCode']) || strlen($_POST['voucherCode']) < 4)	exit;
require '../../KERNEL-XDRCMS/Init.php';

header('Content-Type: application/json');

$c = $MySQLi->query('SELECT ' . $Config['Vouchers']['ValueColumn'] . ' FROM ' . $Config['Vouchers']['TableName'] . ' WHERE ' . $Config['Vouchers']['CodeColumn'] . ' = \'' . $_POST['voucherCode'] . '\' LIMIT 1'); 
if($c && $c->num_rows === 1):
	$value = $c->fetch_assoc()[$Config['Vouchers']['ValueColumn']];

	header('X-Habbo-Balance: ' . USER::$Data['Credits'] + $value);
	header('X-Habbo-Loyalty: 0');
	
	$MySQLi->multi_query('DELETE FROM ' . $Config['Vouchers']['TableName'] . ' WHERE ' . $Config['Vouchers']['CodeColumn'] . ' = \'' . $_POST['voucherCode'] . '\' LIMIT 1;UPDATE users SET credits = credits + ' . $value . ' WHERE id = ' . USER::$Data['ID'] . ' LIMIT 1');
	echo '{"actionMessages":["You received ' . $value . ' Credits."]}';
	exit;
endif;

echo '{"actionErrors":["Your voucher code could not be found. Please check that you entered it correctly."]}';
?>