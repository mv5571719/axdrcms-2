<?php
$o = (MY_IP != '127.0.0.1') ? 'opacity:0.25; -moz-opacity:0.25;filter: alpha(Opacity=25);' : '';

$newBuffer = '
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body style="background:url(http://fc08.deviantart.net/fs43/f/2009/121/6/a/More_Dark_Clouds_by_GRANNYSATTICSTOCK.jpg); background-size:100% auto;font-family: \'Segoe UI\', \'Segoe Light\', \'Segoe UI Light\', Segoe, Arial, sans-serif; ' . $o . '">
    <div style="position:absolute;top:30%;width:100%;height:auto;left:0px;">
        <div style="background:rgb(35, 35, 35);width:100%;height:100%;">
            <div style="color:white;margin-left:30%;font-size: 26px;padding-top:1%;">Ha ocurrido un error inesperado</div>
            <div style="color:white;margin-left:30%;font-size: 16px;font-weight:lighter;">Vuelva mas tarde.</div>
            <div style="padding-top:1.5%;"></div>
';

if(MY_IP == "127.0.0.1"):
	$newBuffer .= '            <div style="color:white;margin-left:30%;font-size: 16px;font-weight:lighter;">Error in ' . $error['file']  . ':' . $error['line'] . ' (' . $error['message'] . ')</div>
            <div style="padding-top:1.5%;"></div>';
endif;

$newBuffer .= '        </div>
    </div>
</body>
</html>';
?>