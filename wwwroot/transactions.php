<?php
/*=========================================================+
|| # XdrCMS - Sistema de administración de contenido Habbo.
|+=========================================================+
|| # Copyright ® 2010 Xdr. All rights reserved.
|| # http://www.xdrcms.es/
|+=========================================================+
|| # Lns 2010. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
|| # Todas las imagenes, scripts y temas
|| # Copyright (C) 2010 Sulake Ltd. All rights reserved.
|+=========================================================*/

$require_login = true;
require_once('./includes/core.php');

$pagename = "Transacciones";
$pageid = "transaction";
$body_id = "newcredits";
$do = $_GET['do'];

if($do == "give")
{
$giveuser = FilterText($_POST['giveuser']);
$givecredits = FilterText($_POST['givecredits']);
$givepixels = FilterText($_POST['givepixels']);

$check = mysql_query("SELECT * FROM users WHERE username = '".$giveuser."' LIMIT 1") or die(mysql_error());
$exist = mysql_num_rows($check);
$fail = false;

if($giveuser == $name)
{

$msg = "¡No puedes darte Créditos o Píxeles a ti mismo!";
$fail = true;

}
else if($exist == 0)
{

$msg = "Tu amigo no existe. Por favor escribe alguien que si exista.";
$fail = true;

}
else if(!is_numeric($givecredits) || !is_numeric($givepixels))
{

$msg = "Por favor escribe valores numericos en el monto a dar.";
$fail = true;

}
else if($myrow['credits'] < $givecredits || $myrow['pixels'] < $givepixels)
{

$msg = "No tienes suficientes Créditos o Píxeles de los que daras.";
$fail = true;

}
else if($givecredits < 0 || $givepixels < 0)
{

$msg = "Por favor escribe un monto válido y mayor a 0";
$fail = true;

}

if($fail == false)
{

	mysql_query("UPDATE users SET credits = credits - ".$givecredits.", activity_points = activity_points - ".$givepixels." WHERE id = '".$my_id."' LIMIT 1") or die(mysql_error());
	mysql_query("INSERT INTO xdrcms_transactions (userid,date,amount,descr) VALUES ('".$my_id."','".$date_full."','".$givecredits."','Transacción')") or die(mysql_error());
	mysql_query("UPDATE users SET credits = credits + ".$givecredits.", activity_points = activity_points + ".$givepixels." WHERE id = '".userData("id",$giveuser)."' LIMIT 1") or die(mysql_error());

	$msg = "Se ha completado la transacción con éxito";

	@SendMUSData('UPRC' . $my_id); 
	@SendMUSData('UPRC' . userData("id",$giveuser)); 
	@SendMUSData('UPRP' . $my_id); 
	@SendMUSData('UPRP' . userData("id",$giveuser)); 

}

}
else if($do == "convert_credits")
{
	$con_credits = FilterText($_POST['con_credits']);
	$fail = false;
	
	if(!is_numeric($con_credits))
	{
		$msgc = "Por favor escribe valores numericos en el monto a dar.";
		$fail = true;
	}
	else if($con_credits < 0)
	{
		$msgc = "Por favor escribe un monto válido y mayor a 0.";
		$fail = true;
	}
	else if($con_credits > $myrow['credits'])
	{
		$msgc = "No tienes suficientes Créditos. Indica un monto menor a ".$myrow['credits']." (Tus créditos)";;
		$fail = true;
	}
	
	if($fail == false)
	{
		mysql_query("UPDATE users SET credits = credits - ".$con_credits.", pixels = pixels + ".$con_credits." WHERE id = '".$my_id."' LIMIT 1") or die(mysql_error());
		$msgc = "La conversión se ha realizado correctamente.";
	}
}
else if($do == "convert_pixels")
{
	$con_pixels = FilterText($_POST['con_pixels']);
	$fail = false;
	
	if(!is_numeric($con_pixels))
	{
		$msgc = "Por favor escribe valores numericos en el monto a dar.";
		$fail = true;
	}
	else if($con_pixels < 0)
	{
		$msgc = "Por favor escribe un monto válido y mayor a 0.";
		$fail = true;
	}
	else if($con_pixels > $myrow['pixels'])
	{
		$msgc = "No tienes suficientes Píxeles. Indica un monto menor a ".$myrow['activity_points']." (Tus Píxeles)";
		$fail = true;
	}
	
	if($fail == false)
	{
		mysql_query("UPDATE users SET credits = credits + ".$con_pixels.", activity_points = activity_points - ".$con_pixels." WHERE id = '".$my_id."' LIMIT 1") or die(mysql_error());
		$msgc = "La conversión se ha realizado correctamente.";
	}
}


require_once('templates/community_subheader.php');
?>
<div id="container">
	<div id="content" style="position: relative" class="clearfix">
    <div id="column1" class="column">

			<div class="cbb clearfix white">
					<div class="box-content">
				<h3>Transacciones</h3>
					<img src="<?php echo webgallery; ?>/v2/images/extras/credits2.png" align="right" />
					
					<p><center><b><?php echo $msg; ?></b></center><br /></p>
					
				<p>¿Deseas regalarle a esa persona especial unos Créditos o Píxeles?<br />
				¡Ahora puedes hacerlo! Facil, rapido y economicamente.</p>
				
				<form action="<?php echo webgallery; ?>/transactions?do=give" method="POST">
				<p>
				Nombre de usuario:<br />
				<input type="text" name="giveuser" size="30" maxlength="30" /><br /><br />
				Créditos a dar:<br />
				<input type="text" name="givecredits" size="10" maxlength="10" value="0" /><br /><br />
				Píxeles a dar:<br />
				<input type="text" name="givepixels" size="10" maxlength="10" value="0" /><br /><br />
				<input type="submit" value="¡Dar!" class="submit" />
				</p>
				</form>				
					

					</div>
				</div>
				<script type="text/javascript">if (!$(document.body).hasClassName('process-template')) { Rounder.init(); }</script>
				
			<div class="cbb clearfix white">
					<div class="box-content">
				<h3>Conversión</h3>
					
					<p><center><b><?php echo $msgc; ?></b></center><br /></p>
					
				<p>¿Se te acabaron los Créditos/Píxeles? ¿Pero tienes más Créditos/Píxeles y hasta de sobra?<br />
				¡Convierte tus Créditos a Píxeles o tus Píxeles a Créditos!</p>
				
				<form action="<?php echo webgallery; ?>/transactions?do=convert_credits" method="POST">
				<p>
				Deseo convertir
				<input type="text" name="con_credits" size="4" maxlength="10" value="0" />
				Créditos a Píxeles				
				<input type="submit" value="Convertir" class="submit" />
				</p>
				</form>				
				
				<form action="<?php echo webgallery; ?>/transactions?do=convert_pixels" method="POST">
				<p>
				Deseo convertir
				<input type="text" name="con_pixels" size="4" maxlength="10" value="0" />
				Píxeles a Créditos			
				<input type="submit" value="Convertir" class="submit" />
				</p>
				</form>	
					

					</div>
				</div>
				<script type="text/javascript">if (!$(document.body).hasClassName('process-template')) { Rounder.init(); }</script>
				
			</div>	 

<div id="column2" class="column">
</div>
<?php require_once('templates/community_footer.php'); ?>