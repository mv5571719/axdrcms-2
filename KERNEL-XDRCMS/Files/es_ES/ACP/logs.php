<?php
if($do === 'deleteAll' && USER::CAN('deleteLogs')):
	if(checkAntiCsrf()):
		$MySQLi->query('TRUNCATE xdrcms_staff_log');
		SLOG('Remove', 'Borrado de todos los logs', 'manage.php[logs]', 0);
		echo 'Se han borrado todos los logs';
	else:
		echo 'Invalid Secret Key. Please log in with a valid Secret Key.';
	endif;
	exit;
endif;

if(isset($_POST['UBuserId']) && is_numeric($_POST['UBuserId']) && USER::CAN('deleteLogs')):
	if(checkAntiCsrf()):
		$MySQLi->query('DELETE FROM xdrcms_staff_log WHERE id = ' . $_POST['UBuserId']);
		SLOG('Remove', 'Borrado de un log.', 'manage.php[logs]', 0);
		echo 'Se ha borrado el log.';
	else:
		echo 'Invalid Secret Key. Please log in with a valid Secret Key.';
	endif;
	exit;
elseif(isset($_GET['filter'])):
	$_GET['filter'] = hex2bin($_GET['filter']);
	$Search = str_replace('\\', '&#92;', htmlentities($_GET['filter'], ENT_HTML401 | ENT_QUOTES, METHOD::GetCharset($_GET['filter'])));

	$queryOptions = '';
	$orderOption = 'DESC';
	$limitOption = 15;
	$pageOption = 1;
	
	$tableName = 'xdrcms_staff_log';
	$getColumns = '*';
 
	// SEARCH FILTER 0.4 preBeta
	// CODED BY XDR
	
	if(strpos($Search, '&lt;!-- ') !== false && strpos($Search, ' --&gt;') !== false):
		$_s = explode(';', explode(' --&gt;', explode('&lt;!-- ', $Search)[1])[0] . ';');

		foreach($_s as $o):
			if(empty($o) || strpos($o, ':') === false)
				continue;

			$o = explode(':', $o);

			if($o[0] === 'page' && (is_numeric($o[1]) && $o[1] > 0)):
				$pageOption = $o[1];
			endif;
		endforeach;
		$Search = preg_replace('/\&lt;!--(.*?)\--\&gt;/', '', $Search);
	endif;

	$_Page = (($pageOption * $limitOption) - $limitOption);	
	
	$usersQueryCount = $MySQLi->query('SELECT COUNT(*) FROM xdrcms_staff_log')->fetch_assoc()['COUNT(*)'];
	$usersQuery = $MySQLi->query('SELECT ' . $getColumns . ' FROM xdrcms_staff_log WHERE id > \'0\' ' . $queryOptions . ' ORDER BY id ' . $orderOption . ' LIMIT ' . $_Page . ',' . $limitOption . '');
	$DataHTML = '<input type="hidden" id="usersTotal" value="' . $usersQueryCount . '" /><input type="hidden" id="nowPage" value="' . $pageOption . '" /><input type="hidden" id="resultCount" value="' . $limitOption . '" />';

	if($usersQuery && $usersQuery->num_rows > 0):
		while ($Row = $usersQuery->fetch_assoc()):
			$time = (is_numeric($Row['timestamp'])) ? date('d-M-o G:i:s', $Row['timestamp']) : '--';
			$DataHTML .= '<tr><td>' . $Row['id'] . '</td><td>' . $Row['note'] . '</td><td>' . $Row['message'] . '</td><td>' . (($Row['userid'] == USER::$Data['ID']) ? USER::$Data['Name'] : USER::GetData($Row['userid'])['username']) . ' -> ' . (($Row['targetid'] === '0') ? '<i>Sistema</i>' : (($Row['targetid'] == USER::$Data['ID']) ? USER::$Data['Name'] : USER::GetData($Row['targetid'])['username'])) . '</td><td title="' . $time . '">' . METHOD::ParseUNIXTIME($Row['timestamp']) . '</td><td><a href="javascript:void(0)" onclick="DeleteLog(' . $Row['id'] . ')">Borrar</a></td></tr>';
		endwhile;
	else:
		$DataHTML .= 'No se han encontrado logs.';
	endif;

	if(isset($_POST['onlyTable'])):
		echo $DataHTML;
		exit;
	endif;
endif;

if(!isset($usersQueryCount))
	$usersQueryCount = $MySQLi->query('SELECT COUNT(*) FROM xdrcms_staff_log')->fetch_assoc()['COUNT(*)'];

$PageName = 'Lista de logs';
require HTML . 'ACP_header.html';
?>
	<table class="striped">
		<thead>
			<tr>
				<th>Id</th>
				<th>Página</th>
				<th>Acción</th>
				<th>Editor - Afectado</th>
				<th></th>
				<th >Acciones:</th>
			</tr>
		</thead>

		<tbody id="resultTable">
<?php
if(!isset($DataHTML)): ?>
		<input type="hidden" id="usersTotal" value="<?php echo $usersQueryCount; ?>" />
		<input type="hidden" id="resultCount" value="15" />
		<input type="hidden" id="nowPage" value="1" />
<?php
$usersQuery = $MySQLi->query('SELECT id, note, message, userid, targetid, timestamp FROM xdrcms_staff_log ORDER BY id DESC LIMIT 15');
if($usersQuery && $usersQuery->num_rows > 0):
	while ($Row = $usersQuery->fetch_assoc()):
		$time = (is_numeric($Row['timestamp'])) ? date('d-M-o G:i:s', $Row['timestamp']) : '--';
?>
			<tr>
				<td><?php echo $Row['id']; ?></td>
				<td><?php echo $Row['note']; ?></td>
				<td><?php echo $Row['message']; ?></td>
				<td><?php echo ($Row['userid'] == USER::$Data['ID']) ? USER::$Data['Name'] : USER::GetData($Row['userid'])['username']; ?> -> <?php echo ($Row['targetid'] === '0') ? '<i>Sistema</i>' : (($Row['targetid'] == USER::$Data['ID']) ? USER::$Data['Name'] : USER::GetData($Row['targetid'])['username']); ?></td>
				<td title="<?php echo $time; ?>"><?php echo METHOD::ParseUNIXTIME($Row['timestamp']); ?></td>
				<td><?php if(USER::CAN('deleteLogs')): ?><a href="javascript:void(0)" onclick="DeleteLog(<?php echo $Row['id']; ?>)">Borrar</a><?php endif; ?></td>
			</tr>
<?php endwhile; else: echo ''; endif; else: echo $DataHTML; endif; ?>
		</tbody>

		<tfoot></tfoot>
	</table>
	<div style="text-align: center;">
		<button onclick="ChangePage('first')">&lt;&lt;</button>
		<button onclick="ChangePage('back')">&lt;</button>
		<button onclick="ChangePage('next')">&gt;</button>
		<button onclick="ChangePage('last')">&gt;&gt;</button>
	</div>
	
	<input type="hidden" name="SCH" id="i0120" value="<?php echo (isset($_GET['filter'])) ? $_GET['filter'] : ''; ?>">
<?php if(USER::CAN('deleteLogs')): ?>
	<button onclick="NewDialog('Aviso', '¿Estás seguro de borrar todos los logs? ¡No puedes deshacer los cambios!', '', '<button class=\'red\'onclick=\'DeleteALL()\'>Borrar TODO</button><button onclick=\'CloseDialog()\'>Cerrar</button>');">Borrar TODO</button>
<?php endif; ?>
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<script type="text/javascript">
		var uRank = <?php echo USER::$Data['Rank']; ?>;
<?php if(USER::CAN('deleteLogs')): ?>
		function DeleteALL(){
			get("<?php echo HPATH; ?>/manage?p=logs&do=deleteAll", "POST", "");
			window.location.reload();
		}
		function DeleteLog(userId){
			alert(get("<?php echo HPATH; ?>/manage?p=logs", "POST", "UBuserId=" + userId));
		}
<?php endif; ?>

		function SCHclick(){
			var sValue = element('#i0120').value;
			window.history.pushState("", "", 'manage?p=logs&filter=' + b2h(sValue));

			element("#resultTable").innerHTML = get("<?php echo HPATH; ?>/manage?p=logs&filter=" + b2h(sValue), "POST", "onlyTable=true");
		}
	</script>