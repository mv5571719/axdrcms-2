<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/


require "../../../KERNEL-XDRCMS/Init.php";

$url = $_POST['url'];
$groupId = $_POST['groupId'];
$valid_url = ereg("^[A-Za-z0-9_-]{3,}$", $url);

$check = $MySQLi->query("SELECT * FROM users_groups WHERE id = '" . $groupId . "' LIMIT 1");
$exist = $check->num_rows;

if($exist > 0)
{
	$urlExist = query_rows("SELECT null FROM users_groups WHERE nameUrl = '" . $url . "' LIMIT 1");
	
	if($urlExist > 0)
		echo "ERROR " . $url . " est� reservado para otro Grupo";
	else if(!$valid_url)
		echo "ERROR El alias de tu Grupo contiene caracteres inv�lidos. S�lo son aceptados los del tipo A-z, a-z, 0-9, - y _";
	else
		echo "El alias de tu Grupo va a ser " . SITE . "/groups/" . $url . ". No podr�s cambiarlo despu�s.";
}

exit;
?>


