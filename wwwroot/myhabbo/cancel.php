<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright � 2012 Xdr.
|+=========================================================+
|| # Xdr 2012. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$require_login = true;
require '../../KERNEL-XDRCMS/Init.php';
USER::REDIRECT(1);

if(isset($_GET['Id']) && is_numeric($_GET['Id'])):
	if(USER::$Data['ID'] == $_GET['Id']):
		unset($_SESSION['Ajax']['Update']);
		unset($_SESSION['Ajax']['Delete']);
		unset($_SESSION['Ajax']['UpdateTemporal']);

		unset($_SESSION['home_edit']);

		$MySQLi->query('UPDATE xdrcms_site_inventory_items SET isWaiting = \'0\' WHERE userId = ' . USER::$Data['ID'] . ' AND isWaiting = \'1\'');
		$MySQLi->query('DELETE FROM xdrcms_site_items WHERE Temporal = \'True\' AND userId = ' . USER::$Data['ID']);
		header('Location: ' . PATH . '/home/' . USER::$Data['Name']); exit;
	endif;
endif;

header('Location: ' . PATH);
exit;
?>