<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$pagename = 'Staffs';
if(isset($_GET['Hex']) && is_numeric($_GET['Hex'])):
	if($_GET['Hex'] < $MinRank && $_GET['Hex'] > $MaxRank)
		exit;
?>
						<div id="avatar-selector-habblet">
							<ul>
<?php
$Odd = 'even';
$query = $MySQLi->query('SELECT users.id, users.look, users.username, users.motto, xdrcms_users_data.web_online FROM users, xdrcms_users_data WHERE users.id = xdrcms_users_data.id AND rank = ' . $_GET['Hex']);
if(!$query || $query->num_rows == 0):
	echo '<center>No hay usuarios con este rango</center>';
	exit;
endif;
while($UserRow = $query->fetch_assoc()):
	$Odd = ($Odd === 'even') ? 'odd' : 'even';
?>
								<li class="<?php echo $Odd; ?>">
									<img class="avatar-image" src="<?php echo LOOK . $UserRow['look']; ?>&size=s&direction=4&head_direction=4&gesture=sml" width="33" height="56"/>
									<div class="avatar-info">
										<div class="avatar-info-container">
											<div class="avatar-name"><?php echo $UserRow['username']; ?></div>
											<div class="avatar-info">
												<?php echo $UserRow['motto']; ?>
											</div>
											<div class="avatar-conn">
												�ltima conexi�n: <?php echo METHOD::ParseUNIXTIME($UserRow['web_online']); ?>
											</div>
										</div>
										<div class="avatar-select">
											<a href="<?php echo PATH; ?>/home/xdr"><b>Ver</b><i></i></a>
										</div>
									</div>
								</li>
<?php endwhile; ?>
							</ul>
<?php
	exit;
endif;
?>