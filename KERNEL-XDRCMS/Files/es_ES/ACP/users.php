<?php
$tableName = 'users';
$act = isset($_GET['act']) ? $_GET['act'] : '';
if(isset($_POST['UBuserId']) && is_numeric($_POST['UBuserId'])):
	if(checkAntiCsrf()):
		$MySQLi->query('DELETE FROM bans WHERE id = ' . $_POST['UBuserId']);
		SLOG('Unban', 'Desbaneado', 'manage.php[users]', $_POST['UBuserId']);
		echo 'Usuario desbaneado.';
	else:
		echo 'Invalid Secret Key. Please log in with a valid Secret Key';
	endif;
	exit;
endif;
if(isset($_GET['filter'])):
	$_GET['filter'] = hex2bin($_GET['filter']);
	$s = str_replace('\\', '&#92;', htmlentities($_GET['filter'], ENT_HTML401 | ENT_QUOTES, METHOD::GetCharset($_GET['filter'])));

	$queryOptions = '';
	$orderOption = 'ASC';
	$limitOption = 15;
	$pageOption = 1;

	$getColumns = 'users.id, users.username, users.ip_last, xdrcms_users_data.mail, xdrcms_users_data.web_online';
 
	// SEARCH FILTER 0.4 Beta
	// CODED BY XDR
	// Example: <!-- rank:1;order:DESC;limit:20 -->
	if(strpos($s, '&lt;!-- ') !== false && strpos($s, ' --&gt;') !== false):
		$_s = explode(';', explode(' --&gt;', explode('&lt;!-- ', $s)[1])[0] . ';');

		foreach($_s as $o):
			if($o === 'rank'):
				$queryOptions .= 'AND rank > \'' . $MinRank . '\' ';
				continue;
			endif;

			if(empty($o) || strpos($o, ':') === false)
				continue;

			$o = explode(':', $o);

			if($o[0] === 'rank' && is_numeric($o[1])):
				$queryOptions .= 'AND rank = \'' . $o[1] . '\' ';
			elseif($o[0] === 'order' && $o[1] == 'DESC'):
				$orderOption = 'DESC';
			elseif($o[0] === 'limit' && (is_numeric($o[1]) && $o[1] > 0 && $o[1] < 36)):
				$limitOption = $o[1];
			elseif($o[0] === 'page' && (is_numeric($o[1]) && $o[1] > 0)):
				$pageOption = $o[1];
			elseif($o[0] === 'type' && ($o[1] === 'bans' OR $o[1] === 'logs' OR $o[1] === 'refers')):
				$tableName = $o[1];
			endif;
		endforeach;
		$s = preg_replace('/\&lt;!--(.*?)\--\&gt;/', '', $s);
	endif;
	$_Page = (($pageOption * $limitOption) - $limitOption);	
	
	if($tableName === 'users'):
		$usersQueryCount = $MySQLi->query('SELECT COUNT(*) FROM users')->fetch_assoc()['COUNT(*)'];

		if($s === ''):
			$usersQuery = $MySQLi->query('SELECT ' . $getColumns . ' FROM users, xdrcms_users_data WHERE users.id > \'0\' ' . $queryOptions . ' AND users.id = xdrcms_users_data.id ORDER BY users.id ' . $orderOption . ' LIMIT ' . $_Page . ',' . $limitOption . '');
		elseif(is_numeric($s)):
			$usersQuery = $MySQLi->query("SELECT " . $getColumns . " FROM users, xdrcms_users_data WHERE users.id = '" . $s . "' AND users.id = xdrcms_users_data.id AND xdrcms_users_data.id = users.id");
		else:
			$usersQuery = $MySQLi->query("SELECT " . $getColumns . " FROM users, xdrcms_users_data WHERE (users.username LIKE'%" . $s . "%' OR users.ip_last = '" . $s . "') " . $queryOptions . " AND users.id = xdrcms_users_data.id ORDER BY Id " . $orderOption . " LIMIT " . $_Page . "," . $limitOption . "");
		endif;
	elseif($tableName === 'bans'):
		$usersQueryCount = $MySQLi->query('SELECT COUNT(*) FROM bans')->fetch_assoc()['COUNT(*)'];

		if($s === ''):
			$usersQuery = $MySQLi->query('SELECT id, bantype, value, expire, added_by, reason FROM bans WHERE id > \'0\' ' . $queryOptions . ' ORDER BY id ' . $orderOption . ' LIMIT ' . $_Page . ',' . $limitOption . '');
		elseif(is_numeric($s)):
			$usersQuery = $MySQLi->query("SELECT id, bantype, value, expire, added_by, reason FROM bans WHERE id = '" . $s . "'");
		else:
			$usersQuery = $MySQLi->query("SELECT id, bantype, value, expire, added_by, reason FROM bans WHERE value LIKE'%" . $s . "%' " . $queryOptions . " ORDER BY Id " . $orderOption . " LIMIT " . $_Page . "," . $limitOption . "");
		endif;
	elseif($tableName === 'logs'):
		$usersQueryCount = $MySQLi->query('SELECT COUNT(*) FROM xdrcms_staff_log')->fetch_assoc()['COUNT(*)'];
		if($s === ''):
			$usersQuery = $MySQLi->query('SELECT id, action, message, note, userid, targetid, timestamp FROM xdrcms_staff_log WHERE id > \'0\' ' . $queryOptions . ' ORDER BY id DESC LIMIT ' . $_Page . ',' . $limitOption);
		elseif(is_numeric($s)):
			$usersQuery = $MySQLi->query('SELECT id, action, message, note, userid, targetid, timestamp FROM xdrcms_staff_log WHERE userid = ' . $s . ' ORDER BY id DESC LIMIT ' . $_Page . ',' . $limitOption);
		else:
			$usersQuery = $MySQLi->query("SELECT id, action, message, note, userid, targetid, timestamp FROM xdrcms_staff_log WHERE note LIKE'%" . $s . "%' " . $queryOptions . " ORDER BY Id DESC LIMIT " . $_Page . "," . $limitOption . "");
		endif;
	elseif($tableName === 'refers'):
		if(!is_numeric($s)):
			$usersQueryCount = 0;
			$usersQuery = false;

			goto refError;
		endif;
		$q = $MySQLi->query('SELECT Count, ReferIds FROM xdrcms_users_refers WHERE UserId = ' . $s);
		if(!$q || $q->num_rows === 0):
			$usersQueryCount = 0;
			$usersQuery = false;

			goto refError;
		endif;

		$q = $q->fetch_assoc();
		$usersQueryCount = $q['Count'];

		$usersQuery = $MySQLi->query('SELECT ' . $getColumns . ' FROM users, xdrcms_users_data WHERE users.id IN (' . str_replace(['[', ']', '"'], ['', '', ''], $q['ReferIds']) . ') AND users.id = xdrcms_users_data.id AND xdrcms_users_data.id = users.id');
	endif;
refError:
	$DataHTML = '<input type="hidden" id="tableName" value="' . $tableName . '" /><input type="hidden" id="usersTotal" value="' . $usersQueryCount . '" /><input type="hidden" id="nowPage" value="' . $pageOption . '" /><input type="hidden" id="resultCount" value="' . $limitOption . '" />';

	if($usersQuery && $usersQuery->num_rows > 0):
		if($tableName === 'users' || $tableName === 'refers'):
			while ($Row = $usersQuery->fetch_assoc()):
				$time = (is_numeric($Row['web_online'])) ? date('d-M-o G:i:s', $Row['web_online']) : '--';
				$DataHTML .= '<tr><td>' . $Row['id'] . '</td><td>' . $Row['username'] . '</td><td>' . $Row['mail'] . '</td><td><a title="Buscar los usuarios con dicha Ip" href="' . HPATH . '/manage?p=users&filter=' . bin2hex($Row['ip_last']) . '" target="_blank">' . $Row['ip_last'] . '</a></td><td title="' . $time . '">' . METHOD::ParseUNIXTIME($Row['web_online']) . '</td><td><a href="' . HPATH . '/manage?p=users&filter=' . bin2hex($_GET['filter']) . '&do=edit&key=' . $Row['id'] . '">Editar</a></td></tr>';
			endwhile;
		elseif($tableName === 'bans'):
			while ($Row = $usersQuery->fetch_assoc()):
				$time = (is_numeric($Row["expire"])) ? date('d-M-o G:i:s', $Row["expire"]) : "--";
				$DataHTML .= '<tr><td>' . $Row["id"] . '</td><td>' . $Row['reason'] . '</td><td>' . $Row['value'] . '</td><td title="' . $time . '">' . METHOD::ParseUNIXTIME($Row['expire']) . '</td><td><a href="#" onclick="alert(\'' . htmlspecialchars($Row['reason']) . '\')">Ver motivo</a> <a href="#" onclick="unBan(' . $Row['id'] . ')">Desbanear</a></td></tr>';
			endwhile;
		elseif($tableName === 'logs'):
			while ($Row = $usersQuery->fetch_assoc()):
				$time = (is_numeric($Row['timestamp'])) ? date('d-M-o G:i:s', $Row['timestamp']) : '--';
				$DataHTML .= '<tr><td>' . $Row['id'] . '</td><td>' . $Row['note'] . '</td><td>' . $Row['message'] . '</td><td>' . $Row['userid'] . ' -> ' . (($Row['targetid'] === '0') ? 'Sistema' : $Row['targetid']) . '</td><td title="' . $time . '">' . METHOD::ParseUNIXTIME($Row['timestamp']) . '</td><td></td></tr>';
			endwhile;
		endif;
	else:
		$DataHTML .= 'No se han encontrado usuarios con el id/nombre: \'' . $s . '\'';
	endif;

	if(isset($_POST['onlyTable'])):
		echo $DataHTML;
		exit;
	endif;
endif;

if($do === 'edit' && is_numeric($key)):
	$query = $MySQLi->query('SELECT username, credits, rank, ip_last, look FROM users WHERE id = ' . $key);
	if(!$query):
		echo 'An error has ocurred.';
		exit;
	endif;

	$userRow = $query->fetch_assoc();
	$editingUser = $key;
	if(isset($_POST['credits']) && is_numeric($_POST['credits']) && $_POST['credits'] > -1 && $_POST['credits'] < 500000000 && USER::CAN('editUsers')):
		if(checkAntiCsrf()):
			$MySQLi->query('UPDATE users SET credits = ' . $_POST['credits'] . ' WHERE id = ' . $key);
			SLOG('Give', 'Cambiado los cr�ditos a un valor de: ' . $_POST['credits'] . ' cr�ditos', 'manage.php[users]', $key);
			echo 'Se han actualizado los cr�ditos.';
		else:
			echo "Invalid Secret Key. Please log in with a valid Secret Key.";
		endif;
		exit;
	elseif(isset($_POST['rank']) && is_numeric($_POST['rank']) && $_POST['rank'] > 0 && $_POST['rank'] <= $MaxRank && USER::CAN('giveRank') && USER::$Data['ID'] != $key):
		if(!checkAntiCsrf()):
			echo 'Invalid Secret Key. Please log in with a valid Secret Key';
		elseif($_POST['rank'] >= USER::$Data['Rank']):
			echo 'Rango insuficiente.';
		elseif($userRow['rank'] >= USER::$Data['Rank']):
			echo 'Rango insuficiente.';
		else:
			if($MySQLi->query('UPDATE users SET rank = ' . $_POST['rank'] . ' WHERE id = ' . $key)):
				SLOG('Give', 'Cambiado el rango de ' . $userRow['rank'] . ' a ' . $_POST['rank'], 'manage.php[users]', $key);
				echo 'Rango cambiado con �xito.';
			else:
				echo 'An error has ocurred.';
			endif;
		endif;
		exit;
	elseif(isset($_POST['badge']) && USER::CAN('editUsers')):
		if(!checkAntiCsrf()):
			echo 'Invalid Secret Key. Please log in with a valid Secret Key';
		endif;
		$q = $MySQLi->query('SELECT null FROM user_badges WHERE badge_id = \'' . $_POST['badge'] . '\' AND user_id = ' . $key);
		if($q && $q->num_rows > 0):
			$MySQLi->query('DELETE FROM user_badges WHERE badge_id = \'' . $_POST['badge'] . '\' AND user_id = ' . $key);
			SLOG('Remove', 'Quitado la placa ' . $_POST['badge'], 'manage.php[users]', $key);
		else:
			$MySQLi->query('INSERT INTO user_badges (user_id, badge_id)VALUES (' . $key . ', \'' . $_POST['badge'] . '\')');
			SLOG('Give', 'Dado la placa ' . $_POST['badge'], 'manage.php[users]', $key);
		endif;

		exit;
	elseif(isset($_POST['ban'], $_POST['type'], $_POST['lenght'], $_POST['count']) && USER::CAN('ban')):
		if($userRow['rank'] >= USER::$Data['Rank']):
			echo 'No puedes banear a un superior o igual.';
			exit;
		elseif(USER::IsBanned($userRow['username'], $userRow['ip_last'])):
			echo 'Este usuario ya est� baneado.';
			exit;
		endif;

		$type = ($_POST['type'] == '0') ? 'user' : 'ip';
		$len = 60 * 60 * (($_POST['ban'] > 0) ? 24 : 1) * (($_POST['ban'] > 1) ? 31 : 1) * (($_POST['ban'] > 2) ? 12 : 1);
		$len = time() + $len;
		
		$MySQLi->query('INSERT INTO bans (bantype, value, reason, expire, added_by, added_date) VALUES (\'' . $type . '\', \'' . (($type === 'user') ? $userRow['username'] : $userRow['ip_last']) . '\', \'' . $_POST['ban'] . '\', ' . $len . ', \'' . USER::$Data['Name'] . '\', \'' . date('j-n-Y') . '\')');
		SLOG('Ban', 'Baneado', 'manage.php[users]', $key);
		echo 'Usuario baneado.';
		exit;
	endif;
endif;

if(!isset($usersQueryCount))
	$usersQueryCount = $MySQLi->query('SELECT COUNT(*) FROM users')->fetch_assoc()['COUNT(*)'];

$PageName = 'Lista de usuarios';

require HTML . 'ACP_header.html';
?>
<table>
	<thead>
		<tr>
			<th id="th1" class="text-left">ID</th>
			<th id="th2" class="text-left">Nombre</th>
			<th id="th3" class="text-left">Email</th>
			<th id="th6" class="text-left">IP</th>
			<th id="th4" class="text-left">�ltima vez conectado</th>
			<th id="th5" class="text-left">Acciones</th>
		</tr>
	</thead>

	<tbody id="resultTable">
<?php
if(!isset($DataHTML)): ?>
		<input type="hidden" id="usersTotal" value="<?php echo $usersQueryCount; ?>" />
		<input type="hidden" id="resultCount" value="15" />
		<input type="hidden" id="nowPage" value="1" />
		<input type="hidden" id="tableName" value="<?php echo $tableName; ?>" />
<?php
	$usersQuery = $MySQLi->query('SELECT users.id, users.username, users.ip_last, xdrcms_users_data.mail, xdrcms_users_data.web_online FROM users, xdrcms_users_data WHERE users.id = xdrcms_users_data.id ORDER BY users.id ASC LIMIT 15');
	while ($Row = $usersQuery->fetch_assoc()):
?>
			<tr>
				<td><?php echo $Row['id']; ?></td>
				<td><?php echo $Row['username']; ?></td>
				<td><?php echo $Row['mail']; ?></td>
				<td><a title="Buscar los usuarios con dicha Ip" href="<?php echo HPATH; ?>/manage?p=users&filter=<?php echo bin2hex($Row['ip_last']); ?>" target="_blank"><?php echo $Row['ip_last']; ?></td>
				<td title="<?php echo (is_numeric($Row['web_online'])) ? date('d-M-o G:i:s', $Row['web_online']) : '--'; ?>"><?php echo METHOD::ParseUNIXTIME($Row['web_online']); ?></td>
				<td><a href="<?php echo HPATH; ?>/manage?p=users&do=edit&key=<?php echo $Row['id']; ?>">Opciones</a></td>
			</tr>
<?php endwhile; else: echo $DataHTML; endif; ?>
		</tbody>

		<tfoot></tfoot>
	</table>
	
	<div style="text-align: center;">
		<button onclick="ChangePage('first')">&lt;&lt;</button>
		<button onclick="ChangePage('back')">&lt;</button>
		<button onclick="ChangePage('next')">&gt;</button>
		<button onclick="ChangePage('last')">&gt;&gt;</button>
	</div>

	<div id="title">Buscar</div>
	<div class="textbox" style="margin-bottom: 8px;float:left;">
		<div style="position: relative; width: 100%;">
			<input type="text" style="width:500px" name="SCH" id="i0120" value="<?php echo (isset($_GET['filter'])) ? $_GET['filter'] : ''; ?>" maxlength="128" placeholder="ID o Nombre o IP">
			</input>
			<img style="height: 2.5em; position: absolute; height: 100%; z-index: 2;background-color: black;" src="GHK/img/Other-Search-Metro-icon.png" onclick="SCHclick()">
		</div>
	</div>
	<br />
	<br />
	<br />
<?php if(isset($editingUser)): ?>
	<script type="text/javascript">
<?php if(USER::CAN('ban')): ?>
		function banUser(){
			alert(get("<?php echo HPATH; ?>/manage?p=users&do=edit&key=<?php echo $key; ?>", "POST", "ban=" + element("#bReason").value + "&type=" + element("#bType").value + "&lenght=" + element("#bLenght").value + "&count=" + element("#bCount").value));
		}
<?php endif; if(USER::CAN('editUsers')): ?>
		function changeCredits(){
			alert(get("<?php echo HPATH; ?>/manage?p=users&do=edit&key=<?php echo $key; ?>", "POST", "credits=" + element("#uc").value));
		}
		function giveBadge(){
			get("<?php echo HPATH; ?>/manage?p=users&do=edit&key=<?php echo $key; ?>", "POST", "badge=" + element("#ub").value);
			var el = element('#b-' + element("#ub").value);
			if(el == null){
				element("#badgesList").innerHTML = element("#badgesList").innerHTML + '<img id="b-' + element("#ub").value + '" style="cursor:pointer" onclick="element(\'#ub\').value = \'' + element("#ub").value + '\';" src="<?php echo $SiteSettings['badgesPath']; ?>/' + element("#ub").value + '.gif" title="' + element("#ub").value + '"/>';
			} else {
				el.remove();
			}
		}
<?php endif; if(USER::CAN('giveRank') && USER::$Data['ID'] != $key): ?>
		function changeRank(){
			alert(get("<?php echo HPATH; ?>/manage?p=users&do=edit&key=<?php echo $key; ?>", "POST", "rank=" + element("#ur").value));
		}
<?php endif; ?>
	</script>
	<ul class="accordion dark" id="accordionid" data-role="accordion">
		<li class="accordionli active">
			<a onclick="ChangeAccordion(this)" href="javascript:void(0)"><span></span>Editar userId#<?php echo $key; ?></a>
			<div class="accordionccn">
				<ul class="sideBar" style="margin: -5px;">
					<li>
						<span style="font-size:1.5em;margin: -5px -7px;">Cuenta</span>
						<ul>
							<li><a href="<?php echo HPATH; ?>/manage?p=users&do=edit&key=<?php echo $key; ?>">Informaci�n</a></li>
							<li><a href="<?php echo HPATH; ?>/manage?p=users&filter=<?php echo bin2hex('<!-- type:refers -->' . $key); ?>&do=edit&key=<?php echo $key; ?>">Referidos</a></li>
							<?php if(USER::CAN('ban') && USER::$Data['Rank'] > $userRow['rank']): ?><li><a href="<?php echo HPATH; ?>/manage?p=users&do=edit&key=<?php echo $key; ?>&act=ban">Banear</a></li><?php endif; ?>
						</ul>
						<br />
					</li>
<?php if(USER::CAN('editUsers')): ?>
					<li>
						<span style="font-size:1.5em;margin: 0px -7px;">Bienes</span>
						<ul class="sub">
							<li><a href="<?php echo HPATH; ?>/manage?p=users&do=edit&key=<?php echo $key; ?>&act=coins">Monedas</a></li>
							<li><a href="<?php echo HPATH; ?>/manage?p=users&do=edit&key=<?php echo $key; ?>&act=badges">Placas</a></li>
						</ul>
						<br /><br /><br /><br />
					</li>
<?php endif; ?>
				</ul>
				<div style="padding-left: 150px;">
<?php if($act === 'ban'): ?>
<?php if(USER::IsBanned($userRow['username'], $userRow['ip_last'])): ?>
�Este usuario est� baneado! La Raz�n es: "<?php echo USER::$BanData['reason']; ?>", y acabar� en <?php echo date('d/m/Y H:i:s', USER::$BanData['expire']); ?>. <br />
<button onclick="unBan(<?php echo USER::$BanData['id']; ?>)">Desbanear</button>
<?php else: ?>
					<input type="text" id="bReason" placeholder="Raz�n" value=""></input><br /><select name='user.rank' id="bType" class='styled'><option value="0">Nombre</option><option value="1">IP</option></select><br /><input type="number" min="0" max="12" id="bLenght" value="1"></input><select name='user.rank' id="bCount" class='styled'><option value="0">Hora/s</option><option value="1">D�a/s</option><option value="2">Mes/s</option><option value="3">A�o/s</option></select><br /> <button onclick="banUser()">Banear</button>
<?php endif; ?><br /><br /><br /><br /><br /><br /><br />
<?php elseif($act === 'coins'): ?>
				<h3>Cr�ditos</h3>
					<input type="number" name="quantity" min="1" max="500000000" id="uc" value="<?php echo $userRow['credits']; ?>"></input> <button onclick="changeCredits()">Cambiar</button><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<?php elseif($act === 'badges'): echo 'Placas(haz click para seleccionar placa) <br /><div id="badgesList">'; $q = $MySQLi->query('SELECT badge_id FROM user_badges WHERE user_id = ' . $key); 
if($q && $q->num_rows > 0): while($bR = $q->fetch_assoc()): ?>
<img id="b-<?php echo $bR['badge_id']; ?>" style="cursor:pointer" onclick="element('#ub').value = '<?php echo $bR['badge_id']; ?>';" src="<?php echo $SiteSettings['badgesPath'] . $bR['badge_id']; ?>.gif" title="<?php echo $bR['badge_id']; ?>"/>
<?php endwhile; endif; ?></div>
					<br /><input type="text" name="bCode" id="ub" value=""></input> <button onclick="giveBadge()">Dar/Quitar placa</button><br /><br /><br /><br /><br />
<?php else: ?>
				<?php if($tableName === 'refers' && $s === $key): ?><b>La lista de referidos de este usuario se est� mostrando arriba en la tabla.</b><br /><?php endif; ?>
				<img src="<?php echo LOOK . $userRow['look']; ?>&direction=4&head_direction=3&action=wlk&gesture=sml" />
				<h3>Informaci�n:</h3>
					Nombre: <?php echo $userRow['username']; ?><br />
					Rango: <?php echo $userRow['rank']; ?> - <?php echo (isset($staffRanks[$userRow['rank']])) ? $staffRanks[$userRow['rank']][0] : 'Sin Privilegios'; ?><br />
					IP: <a onclick="element('#i0120').value = '<?php echo $userRow['ip_last']; ?>';SCHclick();" href="javascript:void(0)"><?php echo $userRow['ip_last']; ?></a><br />
<?php $refers = USER::GetRefersIds($key); ?>
					Referidos: <?php echo ($refers > 0) ? count($refers) : 0; ?>
<?php if(USER::CAN('giveRank') && USER::$Data['Rank'] > $userRow['rank']): ?>
				<h3>Rango</h3>
					<select name='user.rank' id="ur" class='styled'>
<?php $n = 1; while($n != USER::$Data['Rank']): if($n === $MaxRank)	continue; ?>
						<option id="ur.<?php echo $n; ?>" value='<?php echo $n; ?>'><?php echo ($n === 1)? $hotelName : (isset($staffRanks[$n]) ? $staffRanks[$n][0] : $n); ?></option>
<?php $n++; endwhile; ?>
					</select>
					<script type="text/javascript">
						element("#ur").selectedIndex = <?php echo $userRow['rank'] - 1; ?>;
					</script>
					<button onclick="changeRank()">Cambiar</button>
<?php endif; endif; ?>
			</div>
			</div>
		</li>
	</ul>
<?php endif; ?>
	<br />
	<br />
	<br />
	<br />
	<br />
	<script type="text/javascript">
		var uRank = <?php echo USER::$Data['Rank']; ?>;

		function unBan(userId){
			if(uRank > <?php echo $MinRank; ?>){
				alert(get("<?php echo HPATH; ?>/manage?p=users", "POST", "UBuserId=" + userId));
			} else {
				alert("Rango insuficiente");
			}
		}

		function _ChangeText(){
			if(element("#tableName").value == "logs"){
				element('#titleT').innerHTML = 'Lista de logs';
				element('#th1').innerHTML = 'Id';
				element('#th2').innerHTML = 'Página';
				element('#th3').innerHTML = 'Acci�n';
				element('#th4').innerHTML = 'Creado';
				element('#th5').innerHTML = '';
				element('#th6').innerHTML = 'EditorID - AfectadoID';
			}else if(element("#tableName").value == "bans"){
				element('#titleT').innerHTML = 'Lista de baneos';
				element('#th1').innerHTML = 'Id';
				element('#th2').innerHTML = 'Raz�n';
				element('#th3').innerHTML = 'Usuario';
				element('#th4').innerHTML = 'Expiraci�n';
				element('#th5').innerHTML = 'Acciones';
				element('#th6').innerHTML = '';
			}else {
				element('#titleT').innerHTML = (element("#tableName").value == "refers") ? 'Lista de referidos' : 'Lista de usuarios';
				element('#th1').innerHTML = 'Id';
				element('#th2').innerHTML = 'Nombre';
				element('#th3').innerHTML = 'Email';
				element('#th4').innerHTML = '�ltima vez conectado';
				element('#th5').innerHTML = 'Acciones';
				element('#th6').innerHTML = 'IP';
			}
		}
		function SCHclick(){
			var sValue = element('#i0120').value;
			window.history.pushState("", "", 'manage?p=users&filter=' + b2h(sValue));

			element("#resultTable").innerHTML = get("<?php echo HPATH; ?>/manage?p=users&filter=" + b2h(sValue), "POST", "onlyTable=true");
			_ChangeText();
		}
		_ChangeText();
	</script>