<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$no_rea = true;
$no_refresh = true;
$InSecurity = true;
$old_login = true;

require '../../KERNEL-XDRCMS/Init.php';

if($siteBlocked):
	header('Location:' . PATH . '/error/blocked');
	exit;
endif;

require SCRIPT . 'logout.php';
?>
