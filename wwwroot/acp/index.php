<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
|| # ACP by Xdr
|| # Copyright (C) 2014. Todos los derechos reservados.
|+=========================================================+
*/

$no_rea = true;
$_uMaintenance = true;
$hkzone = true;

if(isset($_POST['login'], $_POST['password'], $_POST['g-recaptcha-response'])):
	require '../../KERNEL-XDRCMS/Init.php';
	if(isset($_SERVER['HTTP_REFERER']) && parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) != www):
		exit();
	endif;
else:
	require '../../KERNEL-XDRCMS/FastInit.php';
endif;

if(isset($_SESSION['Manage']['Login'])):
	header('Location: ' . HPATH . '/manage');
	exit;
endif;

require ACP . 'login.php';
?>