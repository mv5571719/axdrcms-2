<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

if($siteBlocked):
	header('Location:' . PATH . '/error/blocked');
	exit;
endif;

$pageid = isset($pageid) ? $pageid : '';
$body_id = isset($body_id) ? $body_id : '';

CACHE::SetOnline(USER::$Online);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $charsetm; ?>">
	<title><?php echo $hotelName; ?>: <?php if(isset($pagename)) echo $pagename; ?> </title>

<script type="text/javascript">
var andSoItBegins = (new Date()).getTime();
</script>
<link rel="shortcut icon" href="<?php echo webgallery; ?>/v2/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="alternate" type="application/rss+xml" title="<?php echo $hotelName; ?>: RSS" href="<?php echo PATH; ?>/articles/rss.xml">
<meta name="csrf-token" content="b6a333d72e"/>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/common.css" type="text/css" />
<script src="<?php echo webgallery; ?>/static/js/libs2.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/visual.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/libs.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/common.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/fullcontent.js" type="text/javascript"></script>

<link rel="stylesheet" href="http://habbo.es/styles/local/es.css" type="text/css" />

<script src="http://habbo.es/js/local/es.js" type="text/javascript"></script>

<script type="text/javascript"> 
document.habboLoggedIn = <?php echo (USER::$LOGGED) ? 'true' : 'false'; ?>;
var habboName = <?php echo (USER::$LOGGED) ? '"' . USER::$Data['Name'] . '"' : 'null'; ?>;
var habboId = <?php echo (USER::$LOGGED) ? USER::$Data['ID'] : 'null'; ?>;
var habboReqPath = "<?php echo PATH; ?>";
var habboStaticFilePath = "<?php echo webgallery; ?>";
var habboImagerUrl = "<?php echo PATH; ?>/habbo-imaging/";
var habboPartner = "";
var habboDefaultClientPopupUrl = "<?php echo PATH; ?>/client";
window.name = "habboMain";
if (typeof HabboClient != "undefined") {
    HabboClient.windowName = "<?php echo (USER::$LOGGED) ? USER::$Row['client_token'] : 'client'; ?>";
    HabboClient.maximizeWindow = true;
}
</script>

<meta property="fb:app_id" content="<?php echo $_Facebook["App"]["ID"]; ?>"> 

<meta property="og:site_name" content="<?php echo $hotelName; ?> Hotel" />
<meta property="og:title" content="<?php echo $hotelName; ?>: Home" />
<meta property="og:url" content="<?php echo PATH; ?>" />
<meta property="og:image" content="<?php echo PATH; ?>/v2/images/facebook/app_habbo_hotel_image.gif" />
<meta property="og:locale" content="es_ES" />

<?php if($pageid == 'Safety'): ?>
	<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/safety.css" type="text/css" />

<?php elseif($pageid == 'myprofile' || $pageid == 'profile' && !isset($error)): ?>
	<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/home.css" type="text/css" />
    <link href="<?php echo PATH; ?>/myhabbo/styles/assets/backgrounds.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo PATH; ?>/myhabbo/styles/assets/stickers.css" type="text/css" rel="stylesheet" />
	<link href="<?php echo PATH; ?>/myhabbo/styles/assets/other.css" type="text/css" rel="stylesheet" />

	<script src="<?php echo webgallery; ?>/static/js/homeview.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/lightwindow.css" type="text/css" />
	<script src="<?php echo webgallery; ?>/static/js/homeauth.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/group.css" type="text/css" />
	<style type="text/css">
		#playground, #playground-outer {
			width: 922px;
			height: 1360px;
		}
	</style>

	<script type="text/javascript" src="https://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>

<?php if($edit_mode === false): ?>
	<meta name="description" content="This is the <?php echo $hotelName; ?> Home of <?php echo $user_row['username']; ?>." />
	<meta name="keywords" content="mypage.metadata.keywords" />
	<script type="text/javascript">
		document.observe("dom:loaded", function() { initView(<?php echo $user_row['id']; ?>, null); });
	</script>
<?php else: ?>
	<script src="<?php echo webgallery; ?>/static/js/homeedit.js" type="text/javascript"></script>
<?php if(isset($user_row)): ?>
<script language="JavaScript" type="text/javascript">
document.observe("dom:loaded", function() { initView(<?php echo $user_row['id']; ?>, <?php echo $user_row['id']; ?>); });
function isElementLimitReached() {
	if (getElementCount() >= 200) {
		showHabboHomeMessageBox("Error", "You have already reached the maximum number of elements on the page. Remove a sticker, note or widget to be able to place this item.", "Close");
		return true;
	}
	return false;
}

function cancelEditing(expired) {
	location.replace("/myhabbo/cancel/<?php echo $user_row['id']; ?>" + (expired ? "?expired=true" : ""));
}

function getSaveEditingActionName(){
	return '/myhabbo/save';
}

function showEditErrorDialog() {
	var closeEditErrorDialog = function(e) { if (e) { Event.stop(e); } Element.remove($("myhabbo-error")); Overlay.hide(); };
	var dialog = Dialog.createDialog("myhabbo-error", "", false, false, false, closeEditErrorDialog);
	Dialog.setDialogBody(dialog, '<p>Error occurred! Please try again in couple of minutes.</p><p><a href="#" class="new-button" id="myhabbo-error-close"><b>Close</b><i></i></a></p><div class="clear"></div>');
	Event.observe($("myhabbo-error-close"), "click", closeEditErrorDialog);
	Dialog.moveDialogToCenter(dialog);
	Dialog.makeDialogDraggable(dialog);
}


function showSaveOverlay() {
	var invalidPos = getElementsInInvalidPositions();
	if (invalidPos.length > 0) {
	    $A(invalidPos).each(function(el) { Element.scrollTo(el);  Effect.Pulsate(el); });
	    showHabboHomeMessageBox("¡Ehhh! ¡No puedes hacer eso!", "Lo sentimos, pero no puedes colocar notas, etiquetas o elementos aquí. Cierra la ventana para continuar editando tu página.", "Cerrar");
		return false;
	} else {
		Overlay.show(null,'Guardando');
		return true;
	}
}
</script>
<?php
endif; endif; endif;
if(!isset($group_name)) $group_name = '';
 
if($pageid == 'group' || $group_name == true): ?>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/home.css" type="text/css" />
    <link href="http://www.habbo.es/myhabbo/styles/assets/backgrounds.css?v=2c937838ae489f4e6daaad4bde7a8611" type="text/css" rel="stylesheet" />
    <link href="http://www.habbo.es/myhabbo/styles/assets/stickers.css?v=5a9f5cbe379ca65e6417720f48df0510" type="text/css" rel="stylesheet" />

	<link href="http://www.habbo.com/myhabbo/styles/assets/other.css?v=527a41a4773585b99478c5f31361f45c" type="text/css" rel="stylesheet" />
    <link href="http://www.habbo.com/myhabbo/styles/assets/backgrounds.css?v=4e7125ef9ce2c33718703ced21107073" type="text/css" rel="stylesheet" />
    <link href="http://www.habbo.com/myhabbo/styles/assets/stickers.css?v=8c7552497998ff8f17b9169c7f06a5d6" type="text/css" rel="stylesheet" />

<script src="<?php echo webgallery; ?>/static/js/homeview.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/lightwindow.css" type="text/css" />
<script src="<?php echo webgallery; ?>/static/js/homeauth.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/group.css" type="text/css" />

<style type="text/css">

    #playground, #playground-outer {
	    width: 922px;
	    height: 1360px;
    }

</style>	<script type="text/javascript" src="https://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>


<script src="<?php echo webgallery; ?>/static/js/homeedit.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript">
<?php if(isset($groupid)){ $xid = $groupid; } else { if(isset($user_row)) $xid = $user_row['id']; else $xid = ""; } ?>
document.observe("dom:loaded", function() { initView(<?php echo $xid . "," . $xid; ?>); });
function isElementLimitReached() {
	if (getElementCount() >= 200) {
		showHabboHomeMessageBox("Error", "You have already reached the maximum number of elements on the page. Remove a sticker, note or widget to be able to place this item.", "Close");
		return true;
	}
	return false;
}

function cancelEditing(expired) {
	<?php if(!isset($groupid)){ ?>
	location.replace("<?php echo PATH; ?>/home/<?php echo $searchname; ?>/canceledit" + (expired ? "?expired=true" : ""));
	<?php } else { ?>
	location.replace("<?php echo PATH; ?>/groups/<?php echo $groupid; ?>/id/canceledit" + (expired ? "?expired=true" : ""));
	<?php } ?>
}

function getSaveEditingActionName(){
	return '/myhabbo/savehome';
}

function showEditErrorDialog() {
	var closeEditErrorDialog = function(e) { if (e) { Event.stop(e); } Element.remove($("myhabbo-error")); Overlay.hide(); }
	var dialog = Dialog.createDialog("myhabbo-error", "", false, false, false, closeEditErrorDialog);
	Dialog.setDialogBody(dialog, '<p>¡Error! Por favor, vuelve a intentarlo en unos minutos.</p><p><a href="#" class="new-button" id="myhabbo-error-close"><b>Cerrar</b><i></i></a></p><div class="clear"></div>');
	Event.observe($("myhabbo-error-close"), "click", closeEditErrorDialog);
	Dialog.moveDialogToCenter(dialog);
	Dialog.makeDialogDraggable(dialog);
}

<?php if($edit_mode == true){ ?>
	document.observe("dom:loaded", function() { 
		Dialog.showInfoDialog("session-start-info-dialog", 
		"Tu sesión como editor sólo puede durar 29 minutos", 
		"OK", function(e) {Event.stop(e); Element.hide($("session-start-info-dialog"));Overlay.hide();Utils.setAllEmbededObjectsVisibility('hidden');});
		var timeToTwoMinuteWarning= 1678760;
		if(timeToTwoMinuteWarning > 0){
			setTimeout(function(){ 
				Dialog.showInfoDialog("session-ends-warning-dialog",
					"Tu sesión como editor concluirá en 2 minutos", 
					"OK", function(e) {Event.stop(e); Element.hide($("session-ends-warning-dialog"));Overlay.hide();Utils.setAllEmbededObjectsVisibility('hidden');});
			}, timeToTwoMinuteWarning);
		}
	});
<?php } ?>

function showSaveOverlay() {
	var invalidPos = getElementsInInvalidPositions();
	if (invalidPos.length > 0) {
	    $A(invalidPos).each(function(el) { Element.scrollTo(el);  Effect.Pulsate(el); });
	    showHabboHomeMessageBox("¡Ehhh! ¡No puedes hacer eso!", "Lo sentimos, pero no puedes colocar notas, etiquetas o elementos aquí. Cierra la ventana para continuar editando tu página.", "Cerrar");
		return false;
	} else {
		Overlay.show(null,'Guardando');
		return true;
	}
}
</script>
<?php elseif($pageid === 'community'): ?>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/lightweightmepage.css" type="text/css" />
<script src="<?php echo webgallery; ?>/static/js/lightweightmepage.js" type="text/javascript"></script>

<script src="<?php echo webgallery; ?>/static/js/moredata.js" type="text/javascript"></script>
<?php elseif($pageid === 'community2'): ?>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/community.css" type="text/css" />

<?php elseif($body_id === 'profile'): ?>
<script src="<?php echo webgallery; ?>/static/js/settings.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/settings.css" type="text/css" />

<?php elseif($pageid === 'home'): ?>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/personal.css" type="text/css" />
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/lightweightmepage.css" type="text/css" />


<script src="<?php echo webgallery; ?>/static/js/lightweightmepage.js" type="text/javascript"></script>
<?php endif; ?>

<meta name="description" content="<?php echo $siteName; ?>: haz amig@s, únete a la diversión y date a conocer." />
<meta name="keywords" content="<?php echo strtolower($siteName); ?>, mundo, virtual, red social, gratis, comunidad, personaje, chat, online, adolescente, roleplaying, unirse, social, grupos, forums, seguro, jugar, juegos, amigos, adolescentes, raros, furni raros, coleccionable, crear, coleccionar, conectar, furni, muebles, mascotas, diseño de salas, compartir, expresión, placas, pasar el rato, música, celebridad, visitas de famosos, celebridades, juegos en línea, juegos multijugador, multijugador masivo" />


<!--[if IE 8]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/ie8.css" type="text/css" />
<![endif]-->
<!--[if lt IE 8]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/ie.css" type="text/css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/ie6.css" type="text/css" />
<script src="<?php echo webgallery; ?>/static/js/pngfix.js" type="text/javascript"></script>
<script type="text/javascript">
try { document.execCommand('BackgroundImageCache', false, true); } catch(e) {}
</script>

<style type="text/css">
body { behavior: url(/js/csshover.htc); }
</style>
<![endif]-->
<meta name="build" content="<?php echo $_XDRBuild; ?>">
</head>
<body id="<?php echo $body_id; ?>" class="<?php if(USER::$LOGGED) echo 'anonymous'; ?> ">
<div id="overlay"></div>

<?php if(!USER::$LOGGED): ?>
<div id="eu_cookie_policy" class="orange-notifier"><a class="close" id="eu_cookie_policy_close" href="#">cerrar</a><span><?php echo $hotelName; ?> utiliza cookies propias y de terceros para ofrecer un mejor servicio y mostrar publicidad acorde con tus preferencias . Si utilizas nuestra web consideramos que aceptas su uso. <a href="<?php echo PATH; ?>/papers/cookies" target = "_blank">Leer más</a>.</span></div>
<?php endif; ?>

<div id="header-container">
	<div id="header" class="clearfix">
		<h1><a style="background-image: url('<?php echo $SiteSettings['logo']; ?>');width:300px;height:55px;" href="<?php echo PATH; ?>/"></a></h1>
<div id="subnavi"<?php echo (USER::$LOGGED) ? " class=wide" : ""; ?>>

<?php if(USER::$LOGGED): ?>
     <div id="subnavi-search">
         <div id="subnavi-search-upper">
         <ul id="subnavi-search-links">
                 <li><a href="https://help.habbo.com/forums" target="habbohelp" >Ayuda</a></li>
<?php if(USER::CAN('ACP')): ?>
                 <li><a href="<?php echo HPATH; ?>" target="_xdrsettings" >ACP</a></li>
<?php endif; ?>
             <li>
                 <form action="<?php echo SPATH; ?>/account/logout?token=<?php echo USER::$Row['token']; ?>" method="post">
                     <button type="submit" id="signout" class="link"><span>Salir</span></button>
                 </form>

             </li>
         </ul>
         </div>
     </div>
     <div id="to-hotel">
<?php if($SiteSettings['ClientEnabled'] == 1 || ($SiteSettings['ClientEnabled'] == '2' && USER::CAN('ACP'))):
	if(USER::$Row['online'] == 0): ?>
            <a href="<?php echo PATH; ?>/client" class="new-button green-button" target="<?php echo USER::$Row['client_token']; ?>" onClick="HabboClient.openOrFocus(this); return false;"><b>Entra a <?php echo $siteName; ?></b><i></i></a>
	<?php else: ?>			
            <a href="<?php echo PATH; ?>/client" id="enter-hotel-open-medium-link" target="<?php echo USER::$Row['client_token']; ?>" onclick="HabboClient.openOrFocus(this); return false;">Volver al Hotel</a>
	<?php endif; ?>
<?php else: ?>
			<div id="hotel-closed-medium"><?php echo $SiteSettings['txt.button.closed']; ?></div>
<?php endif; ?>
     </div> 
 </div>

 <script type="text/javascript">
 L10N.put("purchase.group.title", "Create a Group");
 document.observe("dom:loaded", function() {
     $("signout").observe("click", function() {
         HabboClient.close();
     });
 });
 </script>
	
<?php else: ?>

    <div id="subnavi-user">
        <div class="clearfix">&nbsp;</div>
        <p>
<?php if($SiteSettings['ClientEnabled'] == 1 || ($SiteSettings['ClientEnabled'] == '2' && USER::CAN('ACP'))): ?>
            <a href="<?php echo PATH; ?>/client" id="enter-hotel-open-medium-link" target="client" onclick="HabboClient.openOrFocus(this); return false;">Entra a <?php echo $siteName; echo $SiteSettings['ClientEnabled'] == '2' ? ' (Solo staffs)' : ''; ?> ?></a>
<?php else: ?>
			<div id="hotel-closed-medium"><?php echo $SiteSettings['txt.button.closed']; ?></div>
<?php endif; ?>
        </p>
    </div>
	<div id="subnavi-login">
		<form action="<?php echo PATH; ?>/account/submit" method="post" id="login-form">
			<input type="hidden" name="page" value="<?php echo $_SERVER["REQUEST_URI"]; ?>" />
			<ul>
				<li>
					<label for="login-username" class="login-text"><b>Email</b></label>
					<input tabindex="1" type="text" class="login-field" name="credentials.username" id="login-username" />
					<a href="#" id="login-submit-new-button" class="new-button" style="float: left; display:none"><b>Conectar</b><i></i></a>
					<input type="submit" id="login-submit-button" value="Login" class="submit"/>
				</li>
				<li>
					<label for="login-password" class="login-text"><b>Contraseña</b></label>
					<input tabindex="2" type="password" class="login-field" name="credentials.password" id="login-password" />
					<input tabindex="3" type="checkbox" name="_login_remember_me" value="true" id="login-remember-me" />
					<label for="login-remember-me" class="left">Mantener conectado</label>
				</li>
			</ul>
		</form>
		<div id="subnavi-login-help" class="clearfix">
			<ul>
				<li class="register"><a href="<?php echo PATH; ?>/account/password/forgot" id="forgot-password"><span>¿Contraseña olvidada?</span></a></li>
				<li><a href="<?php echo PATH; ?>/register"><span>Regístrate gratis</span></a></li>
			</ul>
		</div>
		<div id="remember-me-notification" class="bottom-bubble" style="display:none;">
			<div class="bottom-bubble-t"><div></div></div>
			<div class="bottom-bubble-c">
				Seleccionando esta opción permanecerás conectado a <?php echo $hotelName; ?> hasta que des a la opción &quot;.Desconectar&quot;.
			</div>
			<div class="bottom-bubble-b"><div></div></div>
		</div>
	</div>
        </div>
      <script type="text/javascript">
         LoginFormUI.init();
         RememberMeUI.init("right");
      </script>
<?php endif; ?>
<ul id="navi">
<?php if(!USER::$LOGGED): ?>
	<li id="tab-register-now"><a href="/">¡Regístrate ahora!</a><span></span></li>
<?php endif; ?>

<?php //Navi System 2.1
$NaviPages = ['userName' => '/me',
	'Comunidad' => '/community',
	'Seguridad' => '/safety',
	'Créditos' => '/credits',
	'Noticias' => '/articles'
];

$NaviUrls = ['userName' => ['/me'],
	'Comunidad' => ['/community', '/community/socialmedia', '/community/fansites', '/community/staffs'],
	'Seguridad' => ['/safety/safety_tips', '/safety/habbo_way'],
	'Créditos' => ['/credits'],
	'Noticias' => ['/articles']
];

if(USER::$LOGGED)
	$NaviUrls['userName'] = ['/me', '/home/' . USER::$Data['Name'] . '', '/profile', '/profile/profileupdate', '/ranktest'];


foreach ($NaviPages as $PageName => $PageUrl):
	if($PageUrl === '/me' && !USER::$LOGGED)
		continue;

	$s = (in_array(URI, $NaviUrls[$PageName]) || ($PageName === 'Noticias' && strstr(URI, '/articles')));
	if($s)	$PageNow = $PageName;	
?>
	<li class="<?php echo (($PageName === 'userName') ? 'metab ' : '') . (($s) ? 'selected' : ''); ?>">
		<?php if(!$s): ?><a href="<?php echo PATH . $PageUrl; ?>"><?php if($PageName === 'userName'): ?><?php echo USER::$Data['Name']; ?> (&nbsp;<i style="background-image: url(<?php echo webgallery; ?>/v2/images/rpx/<?php echo USER::$Data['ICON']; ?>.png)">&nbsp;</i>)<?php else: echo $PageName; endif; ?></a><?php else: ?>
		<strong><?php if($PageName === 'userName'): ?><?php echo USER::$Data['Name']; ?> (&nbsp;<i style="background-image: url(<?php echo webgallery; ?>/v2/images/rpx/<?php echo USER::$Data['ICON']; ?>.png)">&nbsp;</i>)<?php else: echo $PageName; endif; ?></strong><?php endif; ?>
		<span></span>
	</li>
<?php
endforeach; // End Foreach
?>
</ul>
        <div id="habbos-online"><div class="rounded"><span><?php echo USER::$Online . ' ' . $hotelName; ?>s conectados</span></div></div>
	</div>
</div>


<div id="content-container">
<?php
if(USER::$LOGGED || !isset($_InErrorPage)):
	$Navi2Pages = [
		'userName' => [
		],
		'Comunidad' => [
			'Comunidad' => '/community',
			'Medios Sociales' => '/community/socialmedia',
			'Fansite' => '/community/fansites'
		],
		'Seguridad' => [
			'Consejos de Seguridad' => '/safety/safety_tips',
			'La Manera ' . $hotelName => '/safety/habbo_way'
		],
		'Créditos' => [
		],
		'Noticias' => [
			'Noticias' => '/articles'
		]
	];
	
	if($SiteSettings['staff.page.visibility'] === 0 || USER::$Data['Rank'] > $MinRank)
		$Navi2Pages['Comunidad']['Staffs'] = '/community/staffs';

	if(!isset($PageNow))
		$PageNow = 'userName';

	if(USER::$LOGGED):
		$Navi2Pages['userName'] = ['Home' => '/me', 'Mi ' . $hotelName . ' Home' => '/home/' . USER::$Data['Name'], 'Ajustes' => '/profile', 'AJUSTES ' . strtoupper($hotelName) . ' CUENTA' => '/identity/settings', '<b>Referidos</b>' => '/refers'];
		
		if(USER::$Data['Rank'] < $MinRank)
			$Navi2Pages['userName']['<b>Peticiones de rango</b>'] = '/ranktest';
	endif;
?>
<div id="navi2-container" class="pngbg">
    <div id="navi2" class="pngbg clearfix">
	<ul>
<?php
	foreach ($Navi2Pages[$PageNow] as $SubPageName => $SubPageUrl):
		$Class = '';
		if(strcasecmp(URI, $SubPageUrl) === 0)
			$Class = 'selected';
		if(strcasecmp(end($Navi2Pages[$PageNow]), $SubPageUrl) === 0)
			$Class .= ' last';
?>
		<li<?php echo !empty($Class) ? ' class="' . $Class . '"' : ''; ?>>
			<?php if(strcasecmp(URI, $SubPageUrl) !== 0): ?><a href="<?php echo PATH; ?><?php echo $SubPageUrl; ?>"><?php echo $SubPageName; ?></a><?php else: ?><?php echo $SubPageName; ?><?php endif; ?>
		</li>
<?php
	endforeach;
?>
	</ul>
    </div>
</div>
<?php endif; ?>



<?php if($Restrictions['Maintenance']['Active'] && !isset($_SESSION['already_know'])) { ?>
 <script type="text/javascript">
    Dialog.showConfirmDialog("Ahora mismo el Hotel se encuentra en mantenimiento es probable que algunas funciones hayan sido desactivadas.<br /><br />Por favor te pedimos que no hagas demasiadas cosas ya que podrias dañar el sistema.<br /><br />Para continuar haz clic en 'OK'.", {
        headerText: "¡Atención!",
        buttonText: "",
        cancelButtonText: "OK"
    });
  </script>

<?php $_SESSION['already_know'] = true; } ?>