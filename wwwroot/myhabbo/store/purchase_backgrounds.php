<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2013 Xdr.
|+=========================================================+
|| # Xdr 2013. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

require '../../../KERNEL-XDRCMS/Init.php';
USER::REDIRECT(1);

if(!isset($_POST['task'], $_POST['selectedId']) || $_POST['task'] !== 'purchase' || !is_numeric($_POST['selectedId']))
	exit;

$task = $_POST['task'];
$selectedId = $_POST['selectedId'];

$getItem = $MySQLi->query("SELECT id, price, skin, type FROM xdrcms_store_items WHERE id = '" . $selectedId . "' LIMIT 1"); 

if(!$getItem || $getItem->num_rows === 0):
	echo 'REFRESH';
	exit;
endif;

$row = $getItem->fetch_assoc();
	
if(USER::$Data['Credits'] < $row['price']):
	echo 'REFRESH';
	exit;
endif;

$MySQLi->query("INSERT INTO xdrcms_site_inventory_items (userId, var, skin, type) VALUES ('" . USER::$Data['ID'] . "', '', '".$row['skin']."', '".$row['type']."')");
$MySQLi->query('UPDATE users SET credits = \'' . (USER::$Data['Credits'] - $row['price']) . '\' WHERE id = ' . USER::$Data['ID']);

header('X-JSON: ' . $row['id']);
echo 'OK';
?>
