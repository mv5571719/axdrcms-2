<?php
$PageName = 'Artículos';
$articlesQueryCount = $MySQLi->query('SELECT COUNT(*) FROM xdrcms_news')->fetch_assoc()['COUNT(*)'];

if(isset($_GET['filter'])):
	$_GET['filter'] = hex2bin($_GET['filter']);
	$Search = str_replace('\\', '&#92;', htmlentities($_GET['filter'], ENT_HTML401 | ENT_QUOTES, METHOD::GetCharset($_GET['filter'])));

	$queryOptions = '';
	$orderOption = 'DESC';
	$limitOption = 15;
	$pageOption = 1;
 
	// SEARCH FILTER 0.4 Beta
	// CODED BY XDR
	
	if(strpos($Search, '&lt;!-- ') !== false && strpos($Search, ' --&gt;') !== false):
		$_s = explode(';', explode(' --&gt;', explode('&lt;!-- ', $Search)[1])[0] . ';');

		foreach($_s as $o):
			if(empty($o) || strpos($o, ':') === false)
				continue;

			$o = explode(':', $o);

			if($o[0] === 'page' && (is_numeric($o[1]) && $o[1] > 0)):
				$pageOption = $o[1];
			endif;
		endforeach;
		$Search = preg_replace('/\&lt;!--(.*?)\--\&gt;/', '', $Search);
	endif;

	$_Page = (($pageOption * $limitOption) - $limitOption);	

	$articlesQuery = $MySQLi->query('SELECT ID, Title, OwnerId, Created FROM xdrcms_news LIMIT ' . $_Page . ',' . $limitOption . '');
	$DataHTML = '<input type="hidden" id="usersTotal" value="' . $articlesQueryCount . '" /><input type="hidden" id="nowPage" value="' . $pageOption . '" /><input type="hidden" id="resultCount" value="' . $limitOption . '" />';

	if($articlesQuery && $articlesQuery->num_rows > 0):
		while ($Row = $articlesQuery->fetch_assoc()):
			$DataHTML .= '
<tr>
<td>' . $Row['ID'] . '</td>
<td class="right">' . $Row['Title'] . '</td>
<td class="right">' . $Row['OwnerId'] . '</td>
<td class="right" title="' . (is_numeric($Row['Created'])) ? date('d-M-o G:i:s', $Row['Created']) : '--' . '">' . METHOD::ParseUNIXTIME($Row['Created']) . '</td>
<td class="right">' . (($Row['OwnerId'] == USER::$Data['ID'] || USER::CAN('editdelete')) ? '<a href="' .  HPATH . '/manage?p=articles&do=edit&key=' . $Row['ID'] . '">Editar</a> || <a href="' . HPATH . '/manage?p=articles&do=remove&key=' . $Row['ID'] . '">Borrar</a>' : '') . '</td>
</tr>
';
		endwhile;
	else:
		$DataHTML .= 'No se han encontrado artículos.';
	endif;

	if(isset($_POST['onlyTable'])):
		echo $DataHTML;
		exit;
	endif;
endif;

if(($do == 'new' || ($do == 'edit' && is_numeric($key))) && isset($_POST['article_title'], $_POST['article_category'], $_POST['article_summary'], $_POST['article_image'], $_POST['article_body'], $_POST['article_images'])):
	$ArticleData = [
		'title' => $_POST['article_title'],
		'category' => $_POST['article_category'],
		'summary' => $_POST['article_summary'],
		'img' => $_POST['article_image'],
		'body' => $_POST['article_body'],
		'images' => $_POST['article_images']
	];
	if(!checkAntiCsrf()):
		$msg_error = 'Invalid Secret Key. Please, log in with your secret key.';
	elseif(strlen($_POST['article_title']) < 5 || !is_numeric($_POST['article_category'])):
		$msg_error = 'El título es muy corto.';
	else:
		if($do == 'new'):
			if($MySQLi->query('INSERT INTO xdrcms_news (Title, Summary, Body, Image, Images, Created, Category, OwnerId) VALUES (\'' . $_POST['article_title'] . '\', \'' . $_POST['article_summary'] . '\', \'' . $_POST['article_body'] . '\', \'' . $_POST['article_image'] . '\', \'' . $_POST['article_images'] . '\', ' . time() . ', ' . $_POST['article_category'] . ', ' . USER::$Data['ID'] . ')')):
				SLOG('Create', 'Creación del artículo "' . $_POST['article_title'] . '". Id asignada: ' . $MySQLi->insert_id, 'manage.php[articles]', 0);
			endif;
		elseif($do == 'edit'):
			$MySQLi->query('UPDATE xdrcms_news SET Title = \'' . $_POST['article_title'] . '\', Summary = \'' . $_POST['article_summary'] . '\', Body = \'' . $_POST['article_body'] . '\', Category = ' . $_POST['article_category'] . ', Image = \'' . $_POST['article_image'] . '\', Images = \'' . $_POST['article_images'] . '\' WHERE ID = ' . $key . (!USER::CAN('editdelete') ? ' AND OwnerId = ' . USER::$Data['ID'] : ''));
			SLOG('Change', 'Editado del artículo "' . $_POST['article_title'] . '". Id del artículo: ' . $key, 'manage.php[articles]', 0);
		endif;

		unset($ArticleData);
	endif;
	
	if(isset($msg_error) && $do == 'edit'):
		$promoQuery = $MySQLi->query('SELECT * FROM xdrcms_promos WHERE ID = ' . $key);
		$promoRow = $promoQuery->fetch_assoc();

		unset($ArticleData);
	endif;
elseif($do == 'remove' && is_numeric($key)):
	$MySQLi->query('DELETE FROM xdrcms_news WHERE ID = ' . $key . (!USER::CAN('editdelete') ? ' AND OwnerId = ' . USER::$Data['ID'] : ''));
	SLOG('Remove', 'Borrado de un artículo. Id del artículo: ' . $key, 'manage.php[articles]', 0);
elseif($do == 'edit' && is_numeric($key) && !isset($_POST['promo_title'], $_POST['promo_color'], $_POST['promo_desc'], $_POST['promo_img'], $_POST['promo_exp'])):
	$newQuery = $MySQLi->query('SELECT * FROM xdrcms_news WHERE Id = ' . $key);
	$newRow = $newQuery->fetch_assoc();
	if($newRow['OwnerId'] != USER::$Data['ID'] && !USER::CAN('editdelete')):
		unset($newRow);
	endif;
endif;

require HTML . 'ACP_header.html';
?>
<table class="striped">
	<thead>
		<tr>
			<th>Id</th>
			<th class="right">Título</th>
			<th class="right">Creador:</th>
			<th class="right">Creado:</th>
			<th class="right">Acciones</th>
		</tr>
	</thead>

	<tbody id="resultTable">
<?php
if(!isset($DataHTML)): ?>
		<input type="hidden" id="usersTotal" value="<?php echo $articlesQueryCount; ?>" />
		<input type="hidden" id="resultCount" value="15" />
		<input type="hidden" id="nowPage" value="1" />
<?php
$articlesQuery = $MySQLi->query('SELECT ID, Title, OwnerId, Created FROM xdrcms_news ORDER BY ID DESC LIMIT 15');

if($articlesQuery->num_rows > 0):
	while ($Row = $articlesQuery->fetch_assoc()): ?>
		<tr>
			<td><?php echo $Row['ID']; ?></td>
			<td class="right"><?php echo $Row['Title']; ?></td>
			<td class="right"><?php echo $Row['OwnerId']; ?></td>
			<td class="right" title="<?php echo (is_numeric($Row['Created'])) ? date('d-M-o G:i:s', $Row['Created']) : '--'; ?>"><?php echo METHOD::ParseUNIXTIME($Row['Created']); ?></td>
			<td class="right"><?php if($Row['OwnerId'] == USER::$Data['ID'] || USER::CAN('editdelete')): ?><a href="<?php echo HPATH; ?>/manage?p=articles&do=edit&key=<?php echo $Row['ID']; ?>">Editar</a> || <a href="<?php echo HPATH; ?>/manage?p=articles&do=remove&key=<?php echo $Row['ID']; ?>">Borrar</a><?php endif; ?></td>
		</tr>
<?php endwhile; else: echo 'No hay artículos creados.'; endif; else: echo $DataHTML; endif; ?>
	</tbody>
</table>
<div style="text-align: center;">
	<button onclick="ChangePage('first')">&lt;&lt;</button>
	<button onclick="ChangePage('back')">&lt;</button>
	<button onclick="ChangePage('next')">&gt;</button>
	<button onclick="ChangePage('last')">&gt;&gt;</button>
</div>
<input type="hidden" name="SCH" id="i0120" value="<?php echo (isset($_GET['filter'])) ? $_GET['filter'] : ''; ?>">
<br />

<ul class="accordion dark" id="accordionid" data-role="accordion">
    <li class="accordionli <?php echo (isset($ArticleData)) ? 'active' : ''; ?>">
        <a onclick="ChangeAccordion(this)" href="javascript:void(0)"><span></span> Crear artículo</a>
        <div class="accordionccn">
			<form action='<?php echo HPATH; ?>/manage?p=articles&do=new' method='post' name='theAdminForm' id='theAdminForm' class="block-content form">
			<?php echo getAntiCsrf(); ?>
				<table width='100%' cellpadding='5' align='left' border='0'>
					<tr>
						<td class='tablerow1' width='10%'  valign='middle'><b>Título</b>
							<div class='graytext'>
							</div>
						</td>
						<td class='tablerow2' width='30%'  valign='middle'>
							<input type='text' name='article.title' id='title' value="<?php echo (isset($ArticleData)) ? $ArticleData["title"] : ""; ?>" size='30' class='textinput'>
						</td>
					</tr>
					<tr>
						<td class='tablerow1'  width='20%' valign='middle'><b>Categoría</b>
							<div class='graytext'>
								
							</div>
						</td>
						<td class='tablerow2' width='40%'  valign='middle'>
							<select id="article.ncategory" id="colorv" name='article.category'>
								<option value='0'>Otros</option>
								<option value='1'>Actualizaciones</option>
								<option value='2'>Competiciones & encuestas</option>
								<option value='3'>Eventos</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class='tablerow1' width='20%'  valign='middle'><b>Descripción</b>
							<div class='graytext'>
							</div>
						</td>
						<td class='tablerow2' width='40%'  valign='middle'>
							<textarea name='article.summary' cols='150' rows='3' wrap='soft' id='sub_desc' class='multitext'><?php echo (isset($ArticleData)) ? $ArticleData['summary'] : ''; ?></textarea>
						</td>
					</tr>
					<tr>
						<td class='tablerow1' width='20%'  valign='middle'><b>Imágen</b>
							<div class='graytext'>
								<img src="<?php echo (isset($ArticleData)) ? $ArticleData['img'] : ''; ?>" id="imageid" height="128" width="128"> 
							</div>
						</td>
						<td class='tablerow2' width='40%'  valign='middle'>
							<input type='text' name='article.image' id='imageurl' value="<?php echo (isset($ArticleData)) ? $ArticleData['img'] : ''; ?>" size='100' class='textinput' onchange="ChangeImage('imageid', 'imageurl', '')">
						</td>
					</tr>
					<tr>
						<td class='tablerow1' width='20%'  valign='middle'><b>Desarrollo</b>
							<div class='graytext'>
							</div>
						</td>
						<td class='tablerow2' width='40%'  valign='middle'>
							<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
							<script type="text/javascript">
								tinymce.init({
								selector: "textarea#bodyId",
								plugins: [
								"link fullscreen hr paste code"
								],
								toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
								autosave_ask_before_unload: false,
								max_height: 200,
								min_height: 160,
								height : 180
								});
							</script>
							<textarea id='bodyId' name="article.body" style="width:900px; height:300px"><?php echo (isset($ArticleData)) ? $ArticleData['body'] : ''; ?></textarea>
						</td>
					</tr>
					<tr>
						<td class='tablerow1' width='20%'  valign='middle'><b>Grupo de imágenes</b>
							<div class='graytext'>
								Separadas por un espacio, ej: http://URL.png http://URL2.gif http://URL3.jpg ...
							</div>
						</td>
						<td class='tablerow2' width='40%'  valign='middle'>
							<textarea name='article.images' cols='150' rows='3' wrap='soft' id='sub_desc' class='multitext'><?php echo (isset($ArticleData)) ? $ArticleData['images'] : ''; ?></textarea>
						</td>
					</tr>
					<tr>
						<td align='right' class='tablesubheader' colspan='2' >
							<input class='realbutton' type="submit" value="Crear" accesskey='s'/>
						</td>
					</tr>
				</table>
<?php if(isset($ArticleData)): ?>
				<script type="text/javascript">
					element("#article.ncategory").selectedIndex = <?php echo $ArticleData['category']; ?>;
				</script>
<?php endif; ?>
			</form>
        </div>
    </li>

<?php if(isset($newRow)): ?>
                    <li class="accordionli active">
                        <a onclick="ChangeAccordion(this)" href="javascript:void(0)"><span></span> Editar artículo <?php echo $newRow['Title']; ?></a>
                        <div class="accordionccn">
							<form action='<?php echo HPATH; ?>/manage?p=articles&do=edit&key=<?php echo $newRow['ID']; ?>' method='post' name='theAdminForm' id='theAdminForm' class="block-content form">
							<?php echo getAntiCsrf(); ?>
								<table width='100%' cellpadding='5' align='left' border='0'>
									<tr>
										<td class='tablerow1' width='10%'  valign='middle'><b>Título</b>
											<div class='graytext'>
											</div>
										</td>
										<td class='tablerow2' width='30%'  valign='middle'>
											<input type='text' name='article.title' id='title' value="<?php echo $newRow['Title']; ?>" size='30' class='textinput'>
										</td>
									</tr>
									<tr>
										<td class='tablerow1'  width='20%' valign='middle'><b>Color</b>
											<div class='graytext'>
												Category
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<select id="article.category" name='article.category'>
								<option value='0'>Otros</option>
								<option value='1'>Actualizaciones</option>
								<option value='2'>Competiciones & encuestas</option>
								<option value='3'>Eventos</option>
											</select>
										</td>
									</tr>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>Descripción</b>
											<div class='graytext'>
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<textarea name='article.summary' cols='150' rows='3' wrap='soft' id='sub_desc' class='multitext'><?php echo $newRow['Summary']; ?></textarea>
										</td>
									</tr>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>Imágen</b>
											<div class='graytext'>
												<img src="<?php echo $newRow['Image']; ?>" id="imageide" height="128" width="128"> 
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<input type='text' name='article.image' id='imageurle' value="<?php echo $newRow['Image']; ?>" size='100' class='textinput' onchange="ChangeImage('imageide', 'imageurle', '')">
										</td>
									</tr>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>Desarrollo</b>
											<div class='graytext'>
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
	selector: "textarea#bodyId2",
	plugins: [
		"link fullscreen hr paste code"
	],
	toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
	autosave_ask_before_unload: false,
	max_height: 200,
	min_height: 160,
	height : 180
});
</script>
											<textarea id='bodyId2' name="article.body" style="width:900px; height:300px"><?php echo $newRow['Body']; ?></textarea>
										</td>
									</tr>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>Grupo de imágenes</b>
											<div class='graytext'>
												Separados por un espacio, ej: http://URL.png http://URL2.gif http://URL3.jpg ...
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<textarea name='article.images' cols='150' rows='3' wrap='soft' id='sub_desc' class='multitext'><?php echo $newRow['Images']; ?></textarea>
										</td>
									</tr>
									<tr>
										<td align='right' class='tablesubheader' colspan='2' >
											<input class='realbutton' type="submit" value="Guardar" accesskey='s'/>
										</td>
									</tr>
								</table>
							</form>
                        </div>
                    </li>
					<script type="text/javascript">
						element("#article.category").selectedIndex = <?php echo $newRow['Category']; ?>;
					</script>
<?php endif; ?>
                </ul>
	</div>

	<script type="text/javascript">
		function SCHclick(){
			var sValue = element('#i0120').value;
			window.history.pushState("", "", 'manage?p=articles&filter=' + b2h(sValue));

			element("#resultTable").innerHTML = get("<?php echo HPATH; ?>/manage?p=articles&filter=" + b2h(sValue), "POST", "onlyTable=true");
		}
	</script>