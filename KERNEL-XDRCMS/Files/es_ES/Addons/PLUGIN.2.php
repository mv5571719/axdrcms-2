<?php
function ProcessCredits($a){
	$l = strlen($a);
	if($l < 3)
		return $a;
	elseif($l < 7)
		return substr($a, 0, $l - 3) . ' mil';
	elseif($l < 10)
		return substr($a, 0, $l - 6) . ' millones';
	else
		return '+999 millones';
}
?>
<div class="habblet-container">	
	<div class="cbb clearfix <?php echo self::$Colors[$jsonRow['Color']]; ?> ">
		<h2 class="title"><?php echo $jsonRow['Title']; ?></h2> 
		<div class="box-tabs-container clearfix">
			<ul class="box-tabs">
				<li id="tab-0-1-2"><a href="#">Créditos</a><span class="tab-spacer"></span></li>
				<li id="tab-0-1-1" class="selected"><a href="#">Referidos</a><span class="tab-spacer"></span></li>
			</ul>
		</div>
		<div id="tab-0-1-2-content"  style="display: none">
			<div id="hotgroups-habblet-list-container" class="habblet-list-container groups-list">
				<ul class="habblet-list two-cols clearfix">
<?php
$query = $GLOBALS['MySQLi']->query('SELECT id, username, look, credits FROM users WHERE credits > 0 ORDER BY credits DESC LIMIT 10');
if($query && $query->num_rows > 0): 
	$Count = 0;
	$Pos = 'right';
	$Odd = 'even';
	while($Row = $query->fetch_assoc()):
		$Count++;
		$Pos = ($Pos === 'right') ? 'left' : 'right';
		$Odd = ($Pos === 'left') ? ($Odd === 'odd' ? 'even' : 'odd') : $Odd;
?>
				<li class="<?php echo $Odd . ' ' . $Pos ?>" style="background-image: url(&quot;<?php echo LOOK . $Row['look']; ?>&direction=4&head_direction=4&gesture=sml&action=&size=s&quot;);" "/>
					<a class="item" href="<?php echo PATH; ?>/home/<?php echo $Row['id']; ?>/id"><span class="index"><?php echo $Count; ?>.</span> <?php echo $Row['username']; ?><span class="index">Créditos: <?php echo ProcessCredits($Row['credits']); ?></span></a>
				</li>
<?php endwhile; else:
	echo 'No hay usuarios con referidos de momento.';
endif;
?>
				</ul>
			</div>
		</div>
		<div id="tab-0-1-1-content" >
			<div id="avatar-selector-habblet">
				<ul>
<?php
$query = $GLOBALS['MySQLi']->query('SELECT xdrcms_users_refers.Count, users.id, users.username, users.look FROM xdrcms_users_refers, users WHERE xdrcms_users_refers.UserId = users.id ORDER BY xdrcms_users_refers.Count DESC LIMIT 6');
if($query && $query->num_rows > 0):
	$Odd = ($query->num_rows & 1) ? 'even' : 'odd';
	while($Row = $query->fetch_assoc()):
		$Odd = ($Odd === 'even') ? 'odd' : 'even';
		?>
					<li class="<?php echo $Odd; ?>">
						<img class="avatar-image" src="<?php echo LOOK . $Row['look']; ?>&size=s&direction=4&head_direction=4&gesture=sml" width="33" height="56"/>
						<div class="avatar-info">
							<div class="avatar-info-container">
								<div class="avatar-name"><?php echo $Row['username']; ?></div>
								<div>
									Referidos: <span title="Número de referidos"><?php echo $Row['Count']; ?></span>
								</div>
							</div>
							<div class="avatar-select"><a href="<?php echo PATH; ?>/home/<?php echo $Row['id']; ?>"><b>Ver</b><i></i></a></div>
						</div>
					</li>
		<?php
	endwhile;
else:
	echo '<p>No hay usuarios con referidos de momento.</p>';
endif;
?>
					<li class="even">
						<div class="more">
							<a href="<?php echo PATH; ?>/refers"><b><span style="color: #004e9b"><u>Añadir referidos</u></span></b> a mi cuenta</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">if (!$(document.body).hasClassName('process-template')) { Rounder.init(); }</script>