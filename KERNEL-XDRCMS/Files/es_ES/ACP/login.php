<?php
$c = [1 => 'rgb(0, 84, 166)', 2 => 'rgb(218, 59, 1)', 3 => 'rgb(104, 33, 122)', 4 => 'linear-gradient(135deg,  rgba(255,0,0,1) 0%,rgba(255,238,0,1) 20%,rgba(33,255,0,1) 39%,rgba(8,0,255,1) 60%,rgba(255,0,255,1) 85%,rgba(147,0,145,1) 100%)', 5 => 'rgb(0, 130, 0)', 6 => 'rgb(150, 150, 150)'];
$c = (isset($_COOKIE['acpColor']) && is_numeric($_COOKIE['acpColor']) && isset($c[$_COOKIE['acpColor']])) ? $c[$_COOKIE['acpColor']] : 'rgb(34, 34, 34)';

if(isset($_POST['login'], $_POST['password'])):
	$form_name = $_POST['login'];
	$form_pass = $_POST['password'];
	$SecretKey = ($Restrictions['Security']['SecretKeys']['Enabled'] && isset($_POST['secretKey'])) ? $_POST['secretKey'] : '';

	if(!isset($_POST['g-recaptcha-response'])):
		$ErrorICaptcha = true; goto a;
	elseif(strlen($form_name) < 3 || strlen($form_pass) < 6):
		$ErrorAccount = true; goto a;
	elseif (!METHOD::CheckCaptcha($_POST['g-recaptcha-response'])):
		$ErrorCaptcha = true; goto a;
	endif;

    $form_pass = METHOD::HASH($form_pass);
    
	$chk = $MySQLi->query('SELECT xdrcms_users_data.mail, users.id, users.rank FROM users, xdrcms_users_data WHERE users.username = \'' . $form_name . '\' AND xdrcms_users_data.password = \'' . $form_pass . '\' AND users.rank > 3 AND users.id = xdrcms_users_data.id LIMIT 1');
    
    if($chk === null || $chk->num_rows === 0):
		$ErrorAccount = true; goto a;
	endif;
    
    $row = $chk->fetch_assoc();

	if(!$staffRanks[$row['rank']][2]):
		$ErrorAccount = true;
		goto a;
	endif;

	$_SESSION['Manage']['SecretKey'] = ($Restrictions['Security']['SecretKeys']['Enabled']) ? $SecretKey : rand(10000, 99999);
	$_SESSION['Manage']['Login'] = true;

	if(!USER::$LOGGED)
		USER::LOGIN($row['id'], $row['mail'], $form_pass, 'habboid');
	
	header('Location: ' .HPATH . '/manage?p=dashboard');
	exit;
endif;
a:
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>aXDR CMS ACP</title>

    <style type="text/css">
        :root input.default[type="button"]:hover, :root input.default[type="submit"]:hover, :root button.default:hover {
            background-color: rgb(212, 227, 251);
        }
        :root input.default[type="button"]:active, :root input.default[type="submit"]:active, :root button.default:active {
            background-color: black;
            color: white;
        }
        :root input.default[type="button"], :root input.default[type="submit"], :root button.default {
            background-color: rgb(38, 114, 236);
            color: rgb(255, 255, 255);
        }
        :root input[type="button"]:hover, :root input[type="submit"]:hover, :root button:hover {
            background-color: rgba(205, 205, 205, 0.82);
        }
        :root input[type="button"], :root input[type="submit"], :root button {
            height: 2em;
            min-width: 6em;
            font-family: "Segoe UI Web Semibold","Segoe UI Web Regular","Segoe UI","Helvetica Neue",Arial;
            font-size: 100%;
            background-color: rgba(182, 182, 182, 0.7);
            color: rgb(33, 33, 33);
            padding: 3px 12px 5px;
            border: 0px none;
        }

        :root input[type="text"], :root input[type="password"], :root input[type="email"], :root input[type="tel"] {
            width: 302px;
        }
        :root input[type="text"], :root input[type="password"], :root input[type="email"], :root input[type="number"], :root input[type="tel"] {
            height: 1.466em;
        }

        :root input[type="text"]:hover, :root input[type="password"]:hover, :root input[type="email"]:active, :root input[type="number"]:active, :root input[type="tel"]:active {
            border: 1px solid black;
        }
        :root input[type="text"], :root input[type="password"], :root input[type="email"], :root input[type="number"], :root input[type="tel"], :root textarea {
            width: 18.544em;
            padding: 4px 8px;
            font-family: "Segoe UI","Segoe UI Web Regular","Helvetica Neue","BBAlpha Sans","S60 Sans",Arial,"sans-serif";
            font-size: 100%;
            color: rgb(33, 33, 33);
            border: 1px solid rgba(0, 0, 0, 0.27);
            background-color: rgba(255, 255, 255, 0.8);
        }
        input[type="text"], input[type="password"], input[type="email"], input[type="tel"] {
            padding: 4px 8px;
            height: 1.466em;
            width: 302px;
        }

        div.placeholder {
            color: rgb(153, 153, 153);
            background-color: transparent;
            margin-top: 6px;
            margin-left: 9px;
            white-space: nowrap;
        }

        div.placeholderdis {
            color: transparent;
            background-color: transparent;
            margin-top: 6px;
            margin-left: 9px;
            white-space: nowrap;
        }
    </style>
</head>
<body id="body" style="display: block;position:relative; margin:auto; text-align:left; font-family: 'Segoe UI', 'Segoe Light', 'Segoe UI Light', Segoe, Arial, sans-serif; ">
	<div style="background:<?php echo $c; ?>; height:56px; display: block;color: #FFFFFF;text-align: left;">
		<div style="padding: 11px 20px;font-size: 25px;">aXDR CMS 2.0</div>
	</div>
	<div style="padding-left:25px;">
		<div style="font-size: 2.25em; font-weight: lighter;color:<?php echo $c; ?>">Conéctate</div>
		
		<form action="<?php echo HPATH; ?>/" method="post">
<?php if(isset($ErrorAccount)): ?>
			<div style="color: rgb(200, 83, 5);font-size: 86%;line-height: 178%;margin-top: 18px;margin-bottom: 12px;white-space: normal;">Esta cuenta no existe, <br/> o has escrito mal algo o no tiene rango. Comprueba la información.</div>
<?php endif; ?>
		   <div class="textbox" style="margin-bottom: 8px;">
				<div style="position: relative; width: 100%;">
					<input type="text" name="login" id="i0116" maxlength="113" placeholder="alguien">
					</input>
				</div>
		   </div>
		   <div class="textbox" style="margin-bottom: 8px;">
				<div style="position: relative; width: 100%;">
					<input type="password" name="password" id="i0117" maxlength="113" placeholder="contraseña">
					</input>
				</div>
		   </div>
<?php if($Restrictions['Security']['SecretKeys']['Enabled']): ?>
		   <div class="textbox" style="margin-bottom: 8px;">
				<div style="position: relative; width: 100%;">
					<input type="password" name="secretKey" id="i0118" maxlength="5" placeholder="Secret Key">
					</input>
				</div>
		   </div>
<?php endif; ?>
		   <br />
<?php if(isset($ErrorCaptcha)): ?>
			<div style="color: rgb(200, 83, 5);font-size: 86%;line-height: 178%;margin-top: 18px;margin-bottom: 12px;white-space: normal;">Código de Seguridad inválido.</div>
<?php elseif(isset($ErrorICaptcha)): ?>
			<div style="color: rgb(200, 83, 5);font-size: 86%;line-height: 178%;margin-top: 18px;margin-bottom: 12px;white-space: normal;">Ha habido un error al cargar el captcha. <br /> Vuelva más tarde.</div>
<?php endif; ?>	
<script src='https://www.google.com/recaptcha/api.js'></script>
<div id="recaptcha_response_field" class="g-recaptcha" data-theme="light" name="g-recaptcha-response" data-sitekey="<?php echo $ReCaptcha['siteKey'];?>"></div>
			
			<input type="submit" class="default" value="Iniciar sesión"/>
		</form>
	</div>


     <div style="position: fixed;top: 56px;left: 40%;width:1px; background-color:rgb(221, 221, 221); height: 100%;">
		<div style="padding-left:25px;width:500px;margin-top:25px;">
			<div style="font-weight: bold;color:rgb(69,69,69);">Bienvenido a la Admin Control Panel</div>
            <div style="margin-top:8px;color:rgb(69,69,69); font-size: 14px;">Porfavor, conéctate con tu usuario. Si no sabes que es esto, salte porfavor.</div>
            <div style="margin-top:15px;color:rgb(69,69,69); font-size: 14px;">Puedes editar la configuración aquí.</div>
		</div>
	 </div>

	 <div style="position: fixed;bottom: 0px;width: 100%;">
		 <div style="margin-left: auto;margin-right: auto;height:1px; background-color:rgb(221, 221, 221); width: 100%;"></div>
		 <div style="text-align:right;font-weight: lighter;padding-right: 10px;">Powered by Xdr. All rights reserved</div>
		 <div style="text-align:right;font-weight: lighter;padding-right: 10px;">aXDR CMS 2.0</div>
	 </div>
</body>
</html>