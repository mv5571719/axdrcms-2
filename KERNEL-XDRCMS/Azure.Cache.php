<?php
class CACHE {
	public static function GetAIOConfig($l) {
		if ($GLOBALS['Config']['Cache']['AIO'] === true && file_exists(KERNEL . '/Cache/' . $l . $GLOBALS['Config']['Cache']['internalConfig']['extension'] . '.json')):
			$_jsonData = file_get_contents(KERNEL . 'Cache' . DS . $l . $GLOBALS['Config']['Cache']['internalConfig']['extension'] . '.json');
			$_jsonData = json_decode($_jsonData, true);
			
			if($_jsonData !== NULL)
				return $_jsonData;
		endif;

		$_jsonData = $GLOBALS['MySQLi']->query('SELECT Data FROM xdrcms_aio_config WHERE Name = \'' . $l . '\'')->fetch_assoc()['Data'];
		if($GLOBALS['Config']['Cache']['AIO'] === true)
			self::_WRITE(KERNEL . 'Cache' . DS . $l . $GLOBALS['Config']['Cache']['internalConfig']['extension'] . '.json', $_jsonData);
		
		return json_decode($_jsonData, true);
	}

	public static function SetAIOConfig($l, $_jsonData) {
		$GLOBALS['MySQLi']->query("UPDATE xdrcms_aio_config SET Data = '" . $_jsonData . "' WHERE Name = '" . $l . "'");
		
		if($GLOBALS['Config']['Cache']['AIO'] === true)
			self::_WRITE(KERNEL . 'Cache' . DS . $l . $GLOBALS['Config']['Cache']['internalConfig']['extension'] . '.json', $_jsonData);
	}
	
	public static function LoadPromos($r = true, $u = false) {
		global $Config;

		if($r && $Config['Cache']['PROMOS'] === true && file_exists(KERNEL . 'Cache' . DS . 'Promos' . $Config['Cache']['internalConfig']['extension'] . '.json')):
			$_jsonData = file_get_contents(KERNEL . 'Cache' . DS . 'Promos' . $Config['Cache']['internalConfig']['extension'] . '.json');
			$_jsonData = json_decode($_jsonData, true);
			
			if($_jsonData !== NULL)
				return $_jsonData;
		endif;

		$promoQuery = $GLOBALS['MySQLi']->query('SELECT BackGroundImage, Title, Content, GoToHTML, Button FROM xdrcms_promos WHERE Visible = \'1\' ORDER BY Id DESC');
		$promoArray = [];

		if(!$promoQuery || $promoQuery->num_rows === 0):
		elseif($promoQuery->num_rows === 1):
			array_push($promoArray, $promoQuery->fetch_assoc());
		else:
			while($dataRow = $promoQuery->fetch_assoc())
				array_push($promoArray, $dataRow);
		endif;

		if($u && $Config['Cache']['PROMOS'] === true)
			self::_WRITE(KERNEL . 'Cache' . DS . 'Promos' . $Config['Cache']['internalConfig']['extension'] . '.json', json_encode($promoArray));
		if($r)
			return $promoArray;
	}

	public static function SetOnline(&$o) {
		if($GLOBALS['Config']['Cache']['ONLINES'] === true && file_exists(KERNEL . '/Cache/' . 'uOnlines.uint')):
			if((time() - filemtime(KERNEL . 'Cache' . DS . 'uOnlines.uint')) < 120):
				$o = file_get_contents(KERNEL . 'Cache' . DS . 'uOnlines.uint');
				return;
			endif;
		endif;
		
		$o = METHOD::GetOnlineCount();
		$l = strlen($o);
		if($l > 3)
			$o = substr($o, 0, $l - 3) . ',' . substr($o, $l - 3);
		if($GLOBALS['Config']['Cache']['ONLINES'] === true)
			self::_WRITE(KERNEL . 'Cache' . DS . 'uOnlines.uint', $o);
	}
	
	public static function SetMinRefersForRank(&$c) {		
		if($GLOBALS['Config']['Cache']['ONLINES'] === true && file_exists(KERNEL . '/Cache/' . 'MinRefers.uint')):
			if((time() - filemtime(KERNEL . 'Cache' . DS . 'MinRefers.uint')) < 3600):
				$c = file_get_contents(KERNEL . 'Cache' . DS . 'MinRefers.uint');
				return;
			endif;
		endif;

		$c = (round(($GLOBALS['MySQLi']->query('SELECT null FROM users')->num_rows) * 0.012));
		$c = ($c < 5) ? '5' : (($c > 200) ? 200 : $c);
		
		if($GLOBALS['Config']['Cache']['ONLINES'] === true)
			self::_WRITE(KERNEL . 'Cache' . DS . 'MinRefers.uint', $c);
	}
	
	public static function GetStats(){
		if(file_exists(KERNEL . '/Cache/' . 'ACP.Stats.json') && ((time() - filemtime(KERNEL . '/Cache/' . 'ACP.Stats.json')) < 3600))
			return json_decode(file_get_contents(KERNEL . '/Cache/' . 'ACP.Stats.json'), true);

		global $MySQLi;
		global $Config;

		$users = $MySQLi->query('SELECT COUNT(*) FROM xdrcms_users_data')->fetch_assoc()['COUNT(*)'];
		$usersR = $MySQLi->query('SELECT COUNT(*) FROM users WHERE rank > 4')->fetch_assoc()['COUNT(*)'];
		$bans = $MySQLi->query('SELECT COUNT(*) FROM bans');
		$bans = $bans != null && $bans->num_rows === 1 ? $bans->fetch_assoc()['COUNT(*)'] : 0;
		$logs = $MySQLi->query('SELECT COUNT(*) FROM xdrcms_staff_log')->fetch_assoc()['COUNT(*)'];
		$vouchers = $MySQLi->query('SELECT COUNT(*) FROM ' . $Config['Vouchers']['TableName']);
		$vouchers = $vouchers != null && $vouchers->num_rows === 1 ? $vouchers->fetch_assoc()['COUNT(*)'] : 0;
		$plugins = $MySQLi->query('SELECT COUNT(*) FROM xdrcms_addons')->fetch_assoc()['COUNT(*)'];
		$articles = $MySQLi->query('SELECT COUNT(*) FROM xdrcms_news')->fetch_assoc()['COUNT(*)'];
		$promos = $MySQLi->query('SELECT COUNT(*) FROM xdrcms_promos')->fetch_assoc()['COUNT(*)'];

		$rooms = $MySQLi->query('SELECT COUNT(*) FROM rooms');
		$rooms = $rooms != null && $rooms->num_rows > 0 ? $rooms->fetch_assoc()['COUNT(*)'] : 0;
		$roomsItems = $MySQLi->query('SELECT COUNT(*) FROM items');
		$roomsItems = $roomsItems != null && $roomsItems->num_rows > 0 ? $roomsItems->fetch_assoc()['COUNT(*)'] : 0;
		$catalogItems = $MySQLi->query('SELECT COUNT(*) FROM catalog_items');
		$catalogItems = $catalogItems != null && $catalogItems->num_rows > 0 ? $catalogItems->fetch_assoc()['COUNT(*)'] : 0;

		$s = [$users, $usersR, $bans, $logs, $vouchers, $plugins, $articles, $promos, $rooms, $roomsItems, $catalogItems];
		CACHE::SetAIOConfig('ACP.Stats', json_encode($s));
		
		return $s;
	}

	public static function AppendPluginsPosition($PosId) {
		if(!$GLOBALS['Config']['Cache']['PLUGINS'] || !file_exists(KERNEL . 'Cache' . DS . 'pPlugins.' . $PosId . '.json'))
			return;
		$json = file_get_contents(KERNEL . 'Cache' . DS . 'pPlugins.' . $PosId . '.json');
		$json = json_decode($json, true);
		
		if($json === FALSE)
			return;

		foreach($json as $jsonRow):
			if($jsonRow['Rank'] == 0)
				continue;
			elseif(class_exists('USER') && ((USER::$Data['Rank'] == 0 && $jsonRow['Rank'] != 1) || (USER::$Data['Rank'] > 0 && $jsonRow['Rank'] > USER::$Data['Rank'])))
				continue;

			$jsonRow['canDisable'] = isset($jsonRow['canDisable']) ? $jsonRow['canDisable'] : true; // For updated versions
			if(class_exists('USER') && $jsonRow['canDisable'] == true && !empty(USER::$Row['AddonData'])):
				$uPlugins = json_decode(USER::$Row['AddonData'], true);
				if(isset($uPlugins[$jsonRow['ID']]) && !$uPlugins[$jsonRow['ID']])
					continue;
			endif;

			if(!file_exists(KERNEL . 'Cache' . DS . 'Plugin.' . $jsonRow['ID'] . '.html'))
				continue;

			if($jsonRow['Internal'] === true):
				require Files . $GLOBALS['Config']['Lang'] . DS . 'Addons' . DS . 'PLUGIN.' . $jsonRow['ID'] . '.php';
			elseif($jsonRow['template'] === true):
				require Files . $GLOBALS['Config']['Lang'] . DS . 'Addons' . DS . 'HEAD.html';
				require KERNEL . 'Cache' . DS . 'Plugin.' . $jsonRow['ID'] . '.html';
				require Files . $GLOBALS['Config']['Lang'] . DS . 'Addons' . DS . 'FOOTER.html';
			else:
				require KERNEL . 'Cache' . DS . 'Plugin.' . $jsonRow['ID'] . '.html';
			endif;
		endforeach;
	}

	private static $Colors = ['white','black','blue','pixellightblue','red','green','yellow','orange','brown','settings'];
	private static function _WRITE($p, $s){
		$f = fopen($p, 'w');
		fwrite($f, $s);
		fclose($f);
	}
}
?>