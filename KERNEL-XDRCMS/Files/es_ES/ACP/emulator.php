<?php
$PageName = 'Emulador';
require HTML . 'ACP_header.html';
require KERNEL . 'Other' . DS . 'Azure.Sockets.php';

$q = $MySQLi->query('SELECT status FROM server_status');
if(!$q || $q->num_rows == 0):
	echo 'Ha ocurrido un error con la tabla server_status.';
	exit;
endif;
$q = $q->fetch_assoc()['status'] == '1';
if($do === 'start' && !$q):
	$_c = socket_create(AF_INET, SOCK_STREAM, getprotobyname('tcp'));
	socket_connect($_c, $Config['AXMS'][0], $Config['AXMS'][1]);
	socket_write($_c, $Config['AXMS'][2] . ':' . $Config['AXMS'][3]);
	socket_close($_c);
	
	header('Location: ' . HPATH . '/manage?p=emulator');
elseif($do === 'stop'):
	SOCKET::SEND('shutdown');
	header('Location: ' . HPATH . '/manage?p=emulator');
endif;
?>
Desde aquí puedes encender y apagar el emulador.
Aviso: Esta página debería estar configurada para que solo <b>dueños</b> puedan acceder.
<hr />
<?php if($q): ?>
El emulador está encendido.
<button class="red" onclick="NewDialog('Aviso', '¿Estás seguro de querer cerrar el emulador? ¡No les gustará a tus usuarios!', '', '<button class=\'red\'onclick=\'Shutdown()\'>Continuar</button><button onclick=\'CloseDialog()\'>Cerrar</button>');">Cerrar emulador</button>
<?php else: ?>
El emulador está apagado.
<button class="red"  onclick="window.location.href='<?php echo HPATH; ?>/manage?p=emulator&do=start'">Encender emulador</button>
<?php endif; ?>
<script type="text/javascript">
function Shutdown() {
	get("<?php echo HPATH; ?>/manage?p=emulator&do=stop", "GET", "");
	location.reload(); 
}
</script>