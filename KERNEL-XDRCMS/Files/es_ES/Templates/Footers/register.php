<?php
/*=========================================================+
|| # XdrCMS - Sistema de administración de contenido Habbo.
|+=========================================================+
|| # Copyright ® 2010 Xdr. All rights reserved.
|| # http://www.xdrcms.es/
|+=========================================================+
|| # Lns 2010. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
|| # Todas las imagenes, scripts y temas
|| # Copyright (C) 2010 Sulake Ltd. All rights reserved.
|+=========================================================*/

if (!defined("IN_HOLOCMS")) { header("Location:".PATH); exit; }

?>
</script>
<div class=" fb_reset" id="fb-root"><script src="register_files/all.js" async=""></script><div style="position: absolute; top: -10000px; height: 0pt; width: 0pt;"></div></div>
<script type="text/javascript">
    window.fbAsyncInit = function() {
        FB.init({appId: '<?php echo getRpx("facebook_id"); ?>', status: true, cookie: true, xfbml: true});
        $(document).fire("fbevents:scriptLoaded");

    };
    window.assistedLogin = function(FBobject, optresponse) {

        permissions = 'user_birthday,email';
        defaultAction = function(response) {
            fbConnectUrl = "<?php echo PATH; ?>/facebook?connect=true&partner=FBC";
            if (response.session) {
                window.location.replace(fbConnectUrl);
            }
        };

        if (typeof optresponse == 'undefined')
            FBobject.login(defaultAction, {perms:permissions});
        else
            FBobject.login(optresponse, {perms:permissions});

    };
    (function() {
        var e = document.createElement('script');
        e.async = true;
        e.src = document.location.protocol + '//connect.facebook.net/<?php if($lang == "spanish"){ ?>es_ES<?php } ?><?php if($lang == "english"){ ?>en_US<?php } ?>/all.js';
        document.getElementById('fb-root').appendChild(e);
    }());
</script><script type="text/javascript">
  function logged_in(response) {
    if (response.session) {
        window.location.replace("<?php echo PATH; ?>/facebook?connect=true&partner=FBC");
    }
  }
</script>
      </div>

    </div>
    <div class="register-container-bottom"></div>
<div id="footer">
	<p class="footer-links"><a href="http://help.habbo.com/">Habbo.com Customer Support</a> | <a href="http://www.sulake.com/" target="_new">Sulake</a> | <a href="https://www.habbo.com/papers/termsAndConditions" target="_new">Terms of Use</a> | <a href="https://www.habbo.com/papers/privacy" target="_new">Privacy Policy/Your California Privacy Rights</a> | <a href="http://www.habbo.com/help/82" target="_new">Infringements</a> | <a href="http://www.habbo.com/help/63" target="_new"> Terms and Conditions of Sale - US</a> | <a href="https://www.habbo.com/help/51" target="_new">The Habbo Way</a> | <a href="http://www.habbo.com/groups/officialsafetytips">Safety Tips</a>| <a href="mailto:advertising.com@sulake.com" target="_new">Advertise With Us</a></p>

	<p class="copyright">� 2004 - 2010 Sulake Corporation Oy. HABBO is a 
registered trademark of Sulake Corporation Oy in the European Union, the
 USA, Japan, the People's Republic of China and various other 
jurisdictions. All rights reserved.</p>
</div>  </div>
</div>
<script type="text/javascript">
HabboView.run();
</script>


<script src="register_files/ga.js" type="text/javascript"></script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-448325-2");
pageTracker._trackPageview();
</script>
    
    

    






</body></html>