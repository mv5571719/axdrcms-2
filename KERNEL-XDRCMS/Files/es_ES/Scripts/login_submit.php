<?php
if(USER::$LOGGED === true):
	$Url = '/me';
elseif(isset($_POST['credentials_username'], $_POST['credentials_password'])):
	$UserName = strtolower($_POST['credentials_username']);
	$PassWord = METHOD::HASH($_POST['credentials_password']);
	$Remember = isset($_POST['_login_remember_me']);

	$CaptchaResponse = (isset($_POST['recaptcha_response_field'])) ? $_POST['recaptcha_response_field'] : '';
	$CaptchaChallenge = (isset($_POST['recaptcha_challenge_field'])) ? $_POST['recaptcha_challenge_field'] : '';
	
	$Focus = 'login-username';

	if(isset($_SESSION['Index']['Login']['Tries']) && $_SESSION['Index']['Login']['Tries'] >= 4):
		if (!METHOD::CheckCaptcha($CaptchaResponse, $CaptchaChallenge, $_SERVER['REMOTE_ADDR'])):
			$ErrorName = 'Por favor, introduce el código de seguridad y asegúrate de que tu email y contraseña son correctos';
			$Error = true;
		else:
			unset($_SESSION['Index']['Login']['Tries']);
		endif;
	endif;
	
	$Row = [];

	if($Error == true):
	elseif(empty($UserName) && empty($_POST['credentials_password'])):
		$ErrorName = 'Por favor, introduce tu email y contraseña para conectarte.';
		$Error = true;
	elseif(empty($UserName)):
		$ErrorName = 'Por favor, escribe tu email';
		$Error = true;
	elseif(empty($_POST['credentials_password'])):
		$ErrorName = 'Por favor, escribe tu contraseña';
		$Focus = 'login-password';
		$Error = true;
	elseif(!USER::IsEmail($UserName) && !USER::UsernameExist($UserName, $PassWord, 'habboid', 'users.id, users.username', $Row) || USER::IsEmail($UserName) && !USER::Exist($UserName, $PassWord, 'habboid', 'users.id, users.username', $Row)):
			$ErrorName = 'Tu contraseña y usuario/email no coinciden.';
			$Focus = 'login-password';
			$Error = true;
	endif;

	if($Error):
		$Url = '/?username=' . $UserName . '&rememberme=' . (($Remember) ? 'true' : false) . '&focus=' . $Focus . '';

		if(isset($_SESSION['Index']['Login']['Tries']))
			$_SESSION['Index']['Login']['Tries']++;
		else
			$_SESSION['Index']['Login']['Tries'] = 1;
		$_SESSION['Index']['Login']['Error']['Name'] = $ErrorName;
	else:
		$Url = (isset($_POST['page'])) ? $_POST['page'] : '/me?connect=true&disableFriendLinking=false&next=';

		if(!USER::IsBanned($Row['username'], MY_IP)):
			USER::LOGIN($Row['id'], $UserName, $PassWord, 'habboid', $Remember);
		else:
			$Url = '?username=' . $UserName . '&rememberme=false&focus=login-password';
			$_SESSION['Index']['Login']['Error']['Name'] = '�Has sido baneado! La raz�n del baneo es: "' . USER::$BanData['reason'] . '". El baneo expirar� el ' . date('d/m/Y H:i:s', USER::$BanData['expire']) . '.';
			header('Location: ' . PATH . '/?username=' . $Row['username'] . '&rememberme=false&focus=login-password');
		endif;
	endif;
endif;
?>
<html>
<head>
  <title>Redirecting...</title>
  <meta http-equiv="content-type" content="text/html; charset=<?php echo strtolower(ini_get('default_charset')); ?>">
  <style type="text/css">body { background-color: #e3e3db; text-align: center; font: 11px Verdana, Arial, Helvetica, sans-serif; } a { color: #fc6204; }</style>
</head>
<body>

<script type="text/javascript">window.location.replace('<?php echo str_replace('/', '\/', PATH . $Url); ?>');</script><noscript><meta http-equiv="Refresh" content="0;URL=<?php echo PATH . $Url; ?>"></noscript>

<p class="btn">If you are not automatically redirected, please <a href="<?php echo PATH . $Url; ?>" id="manual_redirect_link">click here</a></p>

</body>
</html>