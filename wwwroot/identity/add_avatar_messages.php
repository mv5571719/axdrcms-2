<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/
require_once('../Kernel/Init.php');

if($_SESSION['name_errors'] !== "" && !empty($_SESSION['name_errors'])){
?>
      <div class="error-messages-holder">
                <h3>Por favor, resuelve los siguientes problemas y completa otra vez el formulario</h3>
                <ul>
                    <li><p class="error-message"><?php echo $_SESSION["name_errors"]; ?></p></li>
                </ul>
            </div>
<?php } ?>