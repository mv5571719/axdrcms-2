<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright � 2011 Xdr.
|+=========================================================+
|| # Xdr 2011. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$credits = $_GET['1'];

if($credits == "1928")
{
	$item_id = 500;
	$item_cost = 500;

	$_SESSION['PAIMENT']['PIXELS'] = "1";
	$payment_id = 1;
}
else if($credits == "1922")
{
	$item_id = 170;
	$item_cost = 200;

	$_SESSION['PAIMENT']['PIXELS'] = "2";
	$payment_id = 2;
}
else if($credits == "1921")
{
	$item_id = 75;
	$item_cost = 100;

	$_SESSION['PAIMENT']['PIXELS'] = "3";
	$payment_id = 3;
}
else if($credits == "1920")
{
	$item_id = 36;
	$item_cost = 50;

	$_SESSION['PAIMENT']['PIXELS'] = "4";
	$payment_id = 4;
}
else
{
	header("Location: ".PATH."/errors/500?tstamp=". $time_error);
	exit;
}

?>