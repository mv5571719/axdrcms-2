<?php
$PageName = 'Promos';

if(($do == 'new' || ($do == 'edit' && is_numeric($key))) && isset($_POST['promo_title'], $_POST['promo_desc'], $_POST['promo_img'], $_POST['promo_button'])):
	$PromoData = [
		'title' => $_POST['promo_title'],
		'desc' => $_POST['promo_desc'],
		'img' => $_POST['promo_img'],
		'button' => $_POST['promo_button']
	];
	
	if(!checkAntiCsrf()):
		$msg_error = 'Invalid Secret Key. Please log in with a valid Secret Key.';
	elseif(strlen($_POST['promo_title']) < 5):
		$msg_error = 'El título es muy corto.';
	else:
		if($do == 'new'):
			if($MySQLi->query("INSERT INTO xdrcms_promos (Title, Content, BackGroundImage, GoToHTML, OwnerID, TimeCreated, Button) VALUES ('" . $PromoData['title']  . "', '" . $PromoData['desc']  . "', '" . $PromoData['img']  . "', '', '" . USER::$Data['ID'] . "', '" . time() . "', '" . $PromoData['button'] . "')")):
				SLOG('Create', 'Creación de la promo "' . $_POST['promo_title'] . '". Id asignada: ' . $MySQLi->insert_id, 'manage.php[promos]', 0);
				$msg_correct = 'Creado con éxito';
			endif;
		elseif($do == 'edit'):
			$MySQLi->query("UPDATE xdrcms_promos SET Title = '" . $PromoData['title'] . "', Content = '" . $PromoData['desc'] . "', BackGroundImage = '" . $PromoData['img'] . "', Button = '" . $PromoData['button'] . "' WHERE Id = " . $key. (!USER::CAN('editdelete') ? ' AND OwnerID = ' . USER::$Data['ID'] : ''));
			SLOG('Change', 'Editado el promo "' . $_POST['promo_title'] . '". Id del promo: ' . $key, 'manage.php[promos]', 0);
			$msg_correct = 'Editado con éxito';
		endif;
		CACHE::LoadPromos(false, true);
		unset($PromoData);
	endif;
	
	if(isset($msg_error) && $do == 'edit'):
		$promoQuery = $MySQLi->query('SELECT * FROM xdrcms_promos WHERE Id = ' . $key);
		$promoRow = $promoQuery->fetch_assoc();

		unset($PromoData);
	endif;
elseif($do === 'remove' && is_numeric($key)):
	$MySQLi->query('DELETE FROM xdrcms_promos WHERE Id = ' . $key. (!USER::CAN('editdelete') ? ' AND OwnerID = ' . USER::$Data['ID'] : ''));
	SLOG('Remove', 'Borrado de un promo. Id del promo: ' . $key, 'manage.php[promos]', 0);
	CACHE::LoadPromos(false, true);
elseif($do === 'edit' && is_numeric($key) && !isset($_POST['promo_title'], $_POST['promo_desc'], $_POST['promo_img'], $_POST['promo_exp'])):
	$promoQuery = $MySQLi->query("SELECT * FROM xdrcms_promos WHERE Id = '" . $key . "'");
	$promoRow = $promoQuery->fetch_assoc();
	if($promoRow['OwnerID'] != USER::$Data['ID'] && !USER::CAN('editdelete')):
		unset($newRow);
	endif;
endif;

require HTML . 'ACP_header.html';
?>
				<table class="striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th class="right">Título</th>
                            <th class="right">Creador</th>
                            <th class="right">Fecha de creación</th>
                            <th class="right">Acciones</th>
                        </tr>
                    </thead>

                    <tbody>
<?php
$promosQuery = $MySQLi->query('SELECT Id, Title, OwnerID, TimeCreated FROM xdrcms_promos ORDER BY Id DESC LIMIT 15');

if($promosQuery->num_rows > 0):
	while ($Row = $promosQuery->fetch_assoc()): ?>
                        <tr>
							<td><?php echo $Row['Id']; ?></td>
							<td class="right"><?php echo $Row['Title']; ?></td>
							<td class="right"><?php echo $Row['OwnerID']; ?></td>
							<td class="right" title="<?php echo (is_numeric($Row['TimeCreated'])) ? date('d-M-o G:i:s', $Row['TimeCreated']) : '--'; ?>"><?php echo METHOD::ParseUNIXTIME($Row['TimeCreated']); ?></td>
							<td class="right"><a href="<?php echo HPATH; ?>/manage?p=promos&do=edit&key=<?php echo $Row['Id']; ?>">Editar</a> || <a href="<?php echo HPATH; ?>/manage?p=promos&do=remove&key=<?php echo $Row['Id']; ?>">Borrar</a></td>
						</tr>
<?php endwhile; endif; ?>
                    </tbody>

                    <tfoot></tfoot>
                </table>
				<ul class="accordion dark" id="accordionid" data-role="accordion">
                    <li class="accordionli <?php echo (isset($PromoData)) ? 'active' : ''; ?>">
                        <a onclick="ChangeAccordion(this)" href="javascript:void(0)"><span></span>Crear Promo</a>
                        <div class="accordionccn">
							<form action='<?php echo HPATH; ?>/manage?p=promos&do=new' method='post' name='theAdminForm' id='theAdminForm' class="block-content form">
							<?php echo getAntiCsrf(); ?>
								<table width='100%' cellpadding='5' align='left' border='0'>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>Título</b>
											<div class='graytext'>
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<input type='text' name='promo.title' id='title' value="<?php echo (isset($PromoData)) ? $PromoData['title'] : ''; ?>" size='30' class='textinput'>
										</td>
									</tr>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>Descripción</b>
											<div class='graytext'>
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<textarea name='promo.desc' cols='150' rows='3' wrap='soft' id='sub_desc' class='multitext'><?php echo (isset($PromoData)) ? $PromoData['desc'] : ''; ?></textarea>
										</td>
									</tr>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>Imágen (=+ 468x301)</b>
											<div class='graytext'>
												<img src="<?php echo (isset($PromoData)) ? $PromoData['img'] : ''; ?>" id="imageid" height="128" width="280"> 
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<input type='text' name='promo.img' id='imageurl' value="<?php echo (isset($PromoData)) ? $PromoData['img'] : ''; ?>" size='100' class='textinput' onchange="ChangeImage('imageid', 'imageurl', '')">
										</td>
									</tr>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>Botón</b>
											<div class='graytext'>
												texto<b>|</b>url<b>|</b>imágen<i>(true|false)</i> <br />
												Ejemplo: ¡Hola!<b>|</b><?php echo PATH; ?>/community<b>|</b>true <br />
												Si la url es <b>client</b>, creará un enlaze al client.
												Si lo dejas en blanco, no habrá botón.
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<textarea name='promo.button' cols='150' rows='3' wrap='soft' id='sub_desc' class='multitext'>¡Hola!|client|true</textarea>
										</td>
									</tr>
									<tr>
										<td align='right' class='tablesubheader' colspan='2' >
											<input class='realbutton' type="submit" value="Crear" accesskey='s'/>
										</td>
									</tr>
								</table>
							</form>
                        </div>
                    </li>

<?php if(isset($promoRow)): ?>
                    <li class="accordionli active">
                        <a onclick="ChangeAccordion(this)" href="javascript:void(0)"><span></span>Editar Promo ID #<?php echo $key; ?></a>
                        <div class="accordionccn">
							<form action='<?php echo HPATH; ?>/manage?p=promos&do=edit&key=<?php echo $key; ?>' method='post' name='theAdminForm' id='theAdminForm' class="block-content form">
							<?php echo getAntiCsrf(); ?>
								<table width='100%' cellpadding='5' align='left' border='0'>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>Título</b>
											<div class='graytext'>
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<input type='text' name='promo.title' id='title' value="<?php echo $promoRow['Title']; ?>" size='30' class='textinput'>
										</td>
									</tr>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>Descripción</b>
											<div class='graytext'>
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<textarea name='promo.desc' cols='150' rows='3' wrap='soft' id='sub_desc' class='multitext'><?php echo $promoRow['Content']; ?></textarea>
										</td>
									</tr>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>Imágen (=+ 468x301)</b>
											<div class='graytext'>
												<img src="<?php echo $promoRow['BackGroundImage']; ?>" id="imageid2" height="128" width="280"> 
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<input type='text' name='promo.img' id='imageurl2' value="<?php echo $promoRow['BackGroundImage']; ?>" size='100' class='textinput' onchange="ChangeImage('imageid2', 'imageurl2', '')">
										</td>
									</tr>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>Button</b>
											<div class='graytext'>
												texto<b>|</b>url<b>|</b>imágen<i>(true|false)</i> <br />
												Ejemplo: ¡Hola!<b>|</b><?php echo PATH; ?>/community<b>|</b>true <br />
												Si la url es <b>client</b>, creará un enlaze al client.
												Si lo dejas en blanco, no habrá botón.
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<textarea name='promo.button' cols='150' rows='3' wrap='soft' id='sub_desc' class='multitext'><?php echo (isset($promoRow['Button'])) ? $promoRow['Button'] : ''; ?></textarea>
										</td>
									</tr>
									<tr>
										<td align='right' class='tablesubheader' colspan='2' >
											<input class='realbutton' type="submit" value="Guardar" accesskey='s'/>
										</td>
									</tr>
								</table>
							</form>
                        </div>
                    </li>
<?php endif; ?>
                </ul>
	</div>