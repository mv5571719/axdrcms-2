<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright � 2012 Xdr.
|+=========================================================+
|| # Xdr 2012. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

checkloggedin(2);

unset($_SESSION['quickregister']['register_errors']);

if($_SESSION['quickregister']['step3'])
{
	unset($_SESSION['quickregister']['step3']);
	header("Location:".PATH."/quickregister/email_password");
}
else if($_SESSION['quickregister']['step2'])
{
	unset($_SESSION['quickregister']['USER']['Step2']);
	unset($_SESSION['quickregister']['step2']);
	unset($_SESSION['quickregister']['error']);
	header("Location:".PATH."/quickregister/age_gate");
}
else 
{
	header("Location:".PATH."/quickregister/age_gate");
}

?>
