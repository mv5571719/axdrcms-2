<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$pagename = 'Mis referidos';
$pageid = 'refers';

$RefersIds = '';

$Refers = USER::GetRefersIds(USER::$Data['ID']);
if($Refers !== null)
	$RefersIds = implode(',', $Refers);

$RefersQueryCount = count($Refers);

if(isset($_POST['getPage']) && is_numeric($_POST['getPage'])):
	$_Page = (($_POST['getPage'] * 10) - 10);
	$RefersQuery = $RefersIds === '' ? null : $MySQLi->query('SELECT users.username, users.look, users.rank, users.motto, users.online, xdrcms_users_data.web_online FROM users, xdrcms_users_data WHERE users.id IN( ' . $RefersIds . ' ) AND xdrcms_users_data.id = users.id LIMIT ' . $_Page. ',10');

	$NextPage = (($_Page + 10) < $RefersQueryCount);
	$BackPage = (($_Page - 10) >= 0);

	require HTML . 'Ajax_RefersUsers.html';
	exit;
endif;

$RefersQuery = $RefersIds === '' ? null : $MySQLi->query('SELECT users.username, users.look, users.rank, users.motto, users.online, xdrcms_users_data.web_online FROM users, xdrcms_users_data WHERE users.id IN( ' . $RefersIds . ' ) AND xdrcms_users_data.id = users.id LIMIT 10');
?>