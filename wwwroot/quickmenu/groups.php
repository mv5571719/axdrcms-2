<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$require_login = true;
require "../../KERNEL-XDRCMS/Init.php";
checkloggedin(1);

$Groups = $MySQLi->query("SELECT groupid, is_current, member_rank FROM groups_memberships WHERE userid = '" . $my_id . "' AND is_pending = '0' ORDER BY member_rank LIMIT 20");

if($Groups->num_rows > 0)
{
	echo '<ul id="quickmenu-groups">';
	
	$Even = "odd";
	
	while($row = $Groups->fetch_assoc())
	{
		if($Even == "even")
			$Even = "odd";
		else
			$Even = "even";

		$group_id = $row['groupid'];

		$check = $MySQLi->query("SELECT name,ownerid FROM groups_details WHERE id = '".$group_id."' LIMIT 1") or die(mysql_error());
		$groupdata = $check->fetch_assoc();

		echo '<li class="' . $Even . '"';
		echo "\">";

		if($row['is_current'] == 1)
			echo "<div class=\"favourite-group\" title=\"Favorito\"></div>\n";
				
		if($row['member_rank'] > 1 && $groupdata['ownerid'] !== $my_id)
			echo "<div class=\"admin-group\" title=\"Admin\"></div>\n";
		
		if($groupdata['ownerid'] == $my_id && $row['member_rank'] > 1)
			echo "<div class=\"owned-group\" title=\"Due�o\"></div>\n";

		echo "\n<a href=\"".PATH."/groups/".$group_id."/id\">".nl2br($groupdata['name'])."</a>\n</li>";
	} 
}
else
{
	echo '<li class="odd">A�n no perteneces a ning�n Grupo</li>';
}
echo '<p class="create-group"><a href="#" onclick="GroupPurchase.open(); return false;">Crear un Grupo</a></p>';
echo '</ul>';

$Hotel->Close();


?>