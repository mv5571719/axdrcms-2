<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$body_id = 'cbs2credits';
$pageid = 'community2';
$pagename = 'Medios sociales';

require '../KERNEL-XDRCMS/Init.php';

require HEADER . 'community.php';
require HTML . 'Community_socialmedia.html';
require FOOTER . 'community.php';
?>