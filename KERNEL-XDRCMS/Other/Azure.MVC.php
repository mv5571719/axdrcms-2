<?php
class mysqlic extends mysqli {

	public $QueryCount = 0;

	public function query($q, $resultmode = null)
	{
		++$this->QueryCount;

        if(!$this->real_query($q)):
            error_log('MySQLi Controler: ' . $this->error . '. q: ' . $q, 0);
			return null;
		endif;

        return new mysqli_result($this);
	}
}

METHOD::MVC();
?>