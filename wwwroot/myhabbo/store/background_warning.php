<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2013 Xdr.
|+=========================================================+
|| # Xdr 2013. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

require "../../../KERNEL-XDRCMS/Init.php";
USER::REDIRECT(1);
?>
<p>
La imagen que has seleccionado permanecer� como Fondo de tu Página hasta que selecciones otra imagen o cierres el Cat�logo. Si quieres conservarla como Fondo, tendr�s que comprarla y seleccionarla en tu Inventario.
</p>

<p>
<a href="#" class="new-button" id="webstore-warning-ok"><b>OK</b><i></i></a>
</p>

<div class="clear"></div>