<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>aXDR Compatibility Test</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="web-gallery/system/css/style.css">
</head>
<body>
    <header>
        <h2>aXDR Compatibility Test</h2>
    </header>
    <div class="wrapper">
        <div class="step light">
        <span class="status"></span>
        Starting...
        </div>
        
        <?php if(version_compare(PHP_VERSION, '7.0.0') <= 0): ?>
            <div class="step light">
            <span class="status fail"></span>
                You must upgrade your PHP version to 7.0.0 or higher. <?php exit; ?>
            </div>
        <?php endif; ?>

        <?php if (!extension_loaded('mysqli')): ?>
            <div class="step light">
            <span class="status fail"></span>
                You must enable the mysqli extension. <?php exit; ?>
            </div>
        <?php endif; ?>

        <?php if (!extension_loaded('curl')): ?>
            <div class="step light">
            <span class="status fail"></span>
                You must enable the curl extension. <?php exit; ?>
            </div>
        <?php endif; ?>

        <?php clearstatcache(); ?>

        <?php if (!is_dir('../KERNEL-XDRCMS/')): ?>
            <div class="step light">
            <span class="status fail"></span>
                Missing or not well located "KERNEL-XDRCMS" folder. It must be in inetpub or xampp folder and not in wwwroot or htdocs folder. <?php exit; ?>
            </div>
        <?php endif; ?>

        <?php if(!is_writable('../KERNEL-XDRCMS/Cache/')): ?>
            <div class="step light">
            <span class="status fail"></span>
                You must give permissions to the "Cache" folder in "KERNEL-XDRCMS" folder. <?php exit; ?>
            </div>
        <?php endif; ?>

        <div class="step light">
        <span class="status"></span>
        Finished Test 1 Successfully. Starting test 2...
        </div>

        <?php require '../KERNEL-XDRCMS/Init.php'; ?>

        <?php 
            $query = $MySQLi->query('SELECT @@sql_mode;');
            if($query && $query->num_rows === 1):
                $result = $query->fetch_assoc();
                $result = $result['@@sql_mode'];
                if(stristr($result, 'STRICT')): 
        ?>
                    <div class="step light">
                    <span class="status fail"></span>
                        For better compatibility with emulators, please, turn off mode STRICT MYSQL.
                    </div>
                <?php endif; ?>
        <?php endif; ?>

        <?php $query = $MySQLi->query('SELECT null FROM users'); if(!$query): ?>
            <div class="step light">
            <span class="status fail"></span>
                "users" table does NOT exist. <?php exit; ?>
            </div>
        <?php endif; ?>

        <?php $query = $MySQLi->query('SELECT null FROM bans'); if(!$query): ?>
            <div class="step light">
            <span class="status fail"></span>
                "bans" table does NOT exist. <?php exit; ?>
            </div>
        <?php endif; ?>

        <?php
        $MySQLi->query('ALTER TABLE xdrcms_promos DROP Owner');
        $MySQLi->query('ALTER TABLE xdrcms_promos ADD OwnerID INT(255) NOT NULL DEFAULT \'0\' AFTER Id');
        $MySQLi->query('ALTER TABLE xdrcms_addons CHANGE title title VARCHAR(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL');
        $MySQLi->query('ALTER TABLE xdrcms_promos DROP Color');
        $MySQLi->query('ALTER TABLE xdrcms_addons ADD creatorId INT(255) NOT NULL DEFAULT \'0\' , ADD created INT(255) NOT NULL DEFAULT \'0\' , ADD canDisable BOOLEAN NOT NULL DEFAULT TRUE');
        ?>

        <div class="step light">
        <span class="status"></span>
        FINISHED!
        </div>

        <br />
        <br />
        <b> FINAL STEP </b>: Go to index.php
        <br />
        <br />
        Remove line: <i>header(\'Location: ../XDRTEST.php\'); exit;</i>
        <br />
        <br />
        Save and delete file <?php echo $_SERVER['DOCUMENT_ROOT'] . '\\XDRTEST.php'; ?>

    </div>
</body>
</html>