<?php
#####################################################################
#||||| XdrCMS - Sistema de administraci�n de contenido Habbo  |||||#
#||||| Created by Xdr - HTML, PHP, CSS & KERNEL/CORE           |||||#
# ----------------------------------------------------------------- #
#||||| � Copyright 2014. Xdr 2014 - Open Source Software       |||||#
#||||| � Copyright 2014. Xdr - Kernel/Core Structure           |||||#
#||||| � Copyright 2014. Sulake Corporation - Habbo Archives   |||||#
# ----------------------------------------------------------------- #
#||||| XdrCMS es un Software de libre edici�n no protegido.   |||||#
#||||| Los archivos de Kernel estan protegidos por Xdr.        |||||#
#####################################################################

$hotelName = 'Habbo'; // HOTEL NAME

$Restrictions = [
	'Maintenance' => [
		'Active' => false, // false -> desacttivado, true -> activado
		'Except' => 8, // rango minimo para saltarse el manteminiento.
		'Twitter' => 'habbo', // nombre de twitter, sin @
		'TwitterCount' => 10 // twitts a mostrar.
	],
	'Country' => [
		'Action' => 0, // 0 -> Desactivado (Todo el mundo puede acceder), 1 -> Bloquear (Los paises que esten en la lista no pueden entrar), 2-> Permitir (Solo los paises que esten en la lista pueden entrar)
		'Strict' => true, // true -> Si ocurre un error (Que no encuentra el pais), no te deja pasar. false -> Desactivado.
		'List' => ['ES', 'US']
	],
	'Security' => [
		'SecretKeys' => [
			'Enabled' => false,
			'Keys' => ['1' => '12345'] // userID => KEY(min-max length 5)
		]
	]
];

$ReCaptcha = [
	'siteKey' => '6LdeqFsUAAAAAHNEkqlaD3ylMuN82VXH3IYM0gzu'
];

$Config['MySQL'] = [
	'host' => '127.0.0.1',
	'user' => 'root',
	'pass' => 'Azure',
	'dbname' => 'axdr'
];

########## UBICACI�N DEL SOFTWARE ##########
$Config['URL'] = [
	'Default' => [
		'Require.www' => false, // www.example.com
		'SSL.enabled' => false, // https://
		'Server' => 'dnsazure.com', // url principal, sin www., no poner localhost, para eso ya est� devPrivateServer.
		'Lang' => 'es_ES' // GLOBAL.
	],
	/* 'example.com' => ['Require.www' => false, 'SSL.enabled' => false, 'Lang' => 'es_ES', 'MySQL' => ['host' => '127.0.0.1','user' => 'root','pass' => 'Azure','dbname' => 'cmsdb']], */
	'devPrivateServer' => 'localhost',	// servidor privado de desarrollo - NO TOCAR
	'dirACP' => '/acp' // ADMIN CONTROL PANEL DIRECTORY
];

$Config['Vouchers'] = [
	'TableName' => 'vouchers',
	'CoinColumn' => 'type',
	'CodeColumn' => 'voucher',
	'ValueColumn' => 'value',
	'Types' => [
		'CREDIT' => 'credits'
	]
];

########## Config. MAIL  ###########
// Si la CMS envia emails(Poner true o false), para esto tienes que tener el SMTP configurado.
$Config['EnabledMails'] = true; 
// En este orden: Puerto, Tipo de Conexion(SSL o TLS), Servidor, Usar Autentificaci�n(true o false), Usuario y contraseña.
$Config['SMTP'] = [587, 'tls', 'smtp.gmail.com', true, 'tutosduque@gmail.com', '98031259622'];

########## Config. Avanzada ##########
// Ajusta los datos correspondientes para la conexi�n a la Base de datos.

$Config['OnlineType'] = 0; // 0 normal (select online from users), 1 (select users_online from server_status), 2 (select count from user_online), 3 (mus)

$Config['Cache'] = [
	'internalConfig' => [
		'extension' => ''// TODAY: '.' . date('YmdH')
	],
	'AIO' => true,
	'PROMOS' => true,
	'PLUGINS' => true,
	'ONLINES' => true
];

$Config['AXMS'] = ['127.0.0.1', 30005, 'localhost', 'password']; // host, port, user, pass

########## OPENID ##########

$_Facebook = [
	'Enabled' => true, // true -> Activado, false -> desactivado
	'App' => [
		'ID' => '133468083361908',
		'PrivateID' => 'b9beef89f474842b725c9b01a731459f',
		'Name' => 'XdrCMS' // App Name
	],
	'Page' => 'http://www.facebook.com/habbo'
];

########## RANKS ##########

// groupId => ['tabName', 'tabColor'],
$rankGroups = [0 => ['Equipo Administrativo', 'red'],
	1 => ['Colaboradores', 'blue']
];
//rankName, tabId, access ACP, can edit or delete created things of other users, can delete logs, edit Server Config, edit Site Config, create News, create Styles, edit Users, ban Users, unBan Users, create vouchers, giveRank, create plugins
$staffRanks = [
// COLABORATORS
	'4' => ['Gu�as', 1, false],
	'5' => ['Publicistas', 1, false],
// ADMINISTRATIVE
	'6' => ['Staffs', 0, true, false, false, false, false, true, false, false, true, false, false, false, false],
	'7' => ['Managers', 0, true, true, false, false, false, true, true, false, false, true, true, false, false, false],
	'8' => ['Administradores', 0, true, true, false, true, true, true, true, true, true, true, true, true, true],
	'9' => ['Fundadores', 0, true, true, true, true, true, true, true, true, true, true, true, true, true]
];

########## ID DEL ADMINISTRADOR ##########
//  Por favor ajusta la ID correspondiente a tu usuario dentro del Hotel.
//  Esto te permitira tener el Acceso total del hotel.

$sysadmin = 1;
?>