<?php
class Timer {
	private $Name = '';
	public $NowCreated = false;

	static $foo = null;

	function __constructor($TimerName)
	{
		$this->Name = $TimerName;

		if(!isset($_SESSION['Timers'][$TimerName])):
			$_SESSION['Timers'][$TimerName] = time();
			$this->NowCreated = true;
		endif;
	}

	public function Reload()
	{
		$_SESSION['Timers'][$this->Name] = time();
	}

	public function IsExpired($Minutes)
	{
		return (time() - $_SESSION['Timers'][$this->Name] > ($Minutes * 60) || $this->NowCreated);
	}
}
?>