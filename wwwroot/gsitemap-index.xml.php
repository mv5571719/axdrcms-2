<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$no_maintenance = true;
$no_rea = true;

require "../KERNEL-XDRCMS/Init.php";

header("Content-type: text/xml"); 
?>
<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.google.com/schemas/sitemap/0.84"><sitemap><loc><?php echo PATH; ?>/gsitemap-navigation.xml</loc></sitemap><sitemap><loc><?php echo PATH; ?>/gsitemap-habbogroup.xml</loc></sitemap><sitemap><loc><?php echo PATH; ?>/gsitemap-habbohome.xml</loc></sitemap></sitemapindex>