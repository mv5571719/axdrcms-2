<?php
$PageName = 'Override variables';
$cSettings = CACHE::GetAIOConfig('Client');

if(!$cSettings['managed.override']):
	require_once HTML . 'ACP_header.html';
	echo 'No está activada esta sección, para activarla ve a la sección Servidor/SWF';
	goto jumpOV;
endif;

if(isset($_POST['oVars'])):
	$f = fopen(KERNEL . '/Cache/Override.Variables.txt', 'w');
	fwrite($f, str_ireplace(['php','<?'], ['&#112;hp','&lt;?'], html_entity_decode(html_entity_decode($_POST['oVars'], ENT_QUOTES, 'ISO-8859-1'), ENT_QUOTES, 'ISO-8859-1')));
	fclose($f);
	
	$cSettings['managed.override.token'] = METHOD::RANDOM(40, true, false, 'qwertyuiopasdfghjklzxcvbnm');
endif;

if(isset($_POST['oVars'])):
	$f = fopen(KERNEL . '/Cache/Override.Texts.txt', 'w');
	fwrite($f, str_ireplace(['php','<?'], ['&#112;hp','&lt;?'], html_entity_decode(html_entity_decode($_POST['oTxts'], ENT_QUOTES | ENT_HTML401, 'ISO-8859-1'), ENT_QUOTES, 'ISO-8859-1')));
	fclose($f);
endif;

if(isset($_POST['oVars']) || isset($_POST['oTxts'])):
	$cSettings['managed.override.token'] = METHOD::RANDOM(40, true, false, 'qwertyuiopasdfghjklzxcvbnm');
	CACHE::SetAIOConfig('Client', json_encode($cSettings));
	SLOG('Change', 'Editado las variables de override', 'manage.php[override]', 0);
endif;

$oVars = file_exists(KERNEL . '/Cache/Override.Variables.txt') ? file_get_contents(KERNEL . '/Cache/Override.Variables.txt') : '';
$oTxts = file_exists(KERNEL . '/Cache/Override.Texts.txt') ? file_get_contents(KERNEL . '/Cache/Override.Texts.txt') : '';
require HTML . 'ACP_header.html';
?>
<form action='<?php echo HPATH; ?>/manage?p=override_variables' method='post' name='theAdminForm' id='form1' class="block-content form">
	<textarea name="oVars" cols="150" rows="18" class="multitext"><?php echo $oVars; ?></textarea>
	<h1 style="color:black;font-size: 2em;">Override flash texts</h1>
	<textarea name="oTxts" cols="150" rows="18" class="multitext"><?php echo $oTxts; ?></textarea>
</form>
Current Token: <?php echo $cSettings['managed.override.token']; ?>
<script type="text/javascript">
	ChangeMenuButtons("Guardar", function() { element('#form1').submit() }, "", "");
</script>
<?php jumpOV: ?>