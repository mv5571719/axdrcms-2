<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

require "../../KERNEL-XDRCMS/Init.php";
checkloggedin(1);

if(isset($_GET["hid"])):
	$hid = $_GET['hid'];
endif;

if($hid == "h22") {
?>
<div class="habblet box-content">

    <?php require_once('tags.php'); ?>
	
    <div class="tag-search-form">
<form name="tag_search_form" action="<?php echo PATH; ?>/tag/search" class="search-box">
    <input type="text" name="tag" id="search_query" value="" class="search-box-query" style="float: left"/>
	<a onclick="$(this).up('form').submit(); return false;" href="#" class="new-button search-icon" style="float: left"><b><span></span></b><i></i></a>	
</form>    </div>
</div>
<?php } else if($hid == "h19") { 

echo "<div class=\"habblet box-content\">";

$result = $MySQLi->query("SELECT tag, COUNT(id) AS quantity FROM user_tags GROUP BY tag ORDER BY rand() LIMIT 20");
$number = $result->num_rows;

if($number > 0){
echo "<ul class=\"tag-list\">";

	while($row = $result->fetch_assoc()){
		$tags[$row['tag']] = $row['quantity'];
	}

	$max_qty = max(array_values($tags));
	$min_qty = min(array_values($tags));
	$spread = $max_qty - $min_qty;

	if($spread == 0){ $spread = 1; }

	$step = (200 - 100)/($spread);

	foreach($tags as $key => $value){
	    $size = 100 + (($value - $min_qty) * $step);
	    $size = ceil($size);
	    echo "<li><a href=\"".PATH."/tag/".strtolower($key)."\" class=\"tag\" style=\"font-size:".$size."%\">".trim(strtolower($key))."</a> </li>\n";
	}

echo "</ul>";
} else {
echo "<div>No hay ning�n 'YoSoy'</div>";
}
 ?>
    <div class="tag-search-form">
<form name="tag_search_form" action="<?php echo PATH; ?>/tag/search" class="search-box">
    <input name="tag" id="search_query" value="" class="search-box-query" style="float: left;" type="text">
	<a onclick="$(this).up('form').submit(); return false;" href="#" class="new-button search-icon" style="float: left;"><b><span></span></b><i></i></a>	
</form>    </div>

<?php 
}
else if($hid == "h119") {
function GenerateRoomOccupancy($usersNow, $usersMax)
{
	$num = 1;
	$percentage = intval(($usersNow / $usersMax) * 100);

	if ($percentage <= 0)
	{
		$num = 1;
	}
	else if ($percentage < 35)
	{
		$num = 2;
	}
	else if ($percentage < 75)
	{
		$num = 3;
	}
	else if ($percentage < 100)
	{
		$num = 4;
	}
	else if ($percentage >= 100)
	{
		$num = 5;
	}
	
	return 'room-occupancy-' . $num;
}

?>
<head>
<link rel="stylesheet" href="<?php echo webgallery; ?>/web-gallery/v2/styles/rooms.css" type="text/css" />
<script src="<?php echo webgallery; ?>/web-gallery/static/js/rooms.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/web-gallery/static/js/rooms.js" type="text/javascript"></script>
</head>

<div id="rooms-habblet-list-container-h120" class="recommendedrooms-lite-habblet-list-container">
        <ul class="habblet-list">
<?php

$get = $MySQLi->query("SELECT * FROM rooms WHERE roomtype = 'private' ORDER BY score DESC LIMIT 5");

while ($room = $get->fetch_assoc())
{	
	if ($eo == 'even')
	{
		$eo = 'odd';
	}
	else
	{
		$eo = 'even';
	}

	echo '<li class="' . $eo . '"> 
	<span class="clearfix enter-room-link ' . GenerateRoomOccupancy($room['users_now'], $room['users_max']) . '" title="Ir a la sala" roomid="' . $room['id'] . '"> 
	<span class="room-enter">Entrada</span> 
	<span class="room-name">' . $room['caption'] . '</span> 
	<span class="room-description">' . $room['description'] . '</span>              
	<span class="room-owner">Due�o: <a href="./home/' . $room['owner'] . '">' . $room['owner'] . '</a></span> 
	</span> 
	</li>';
}

?>
        </ul>
            <div id="room-more-data-h120" style="display: none">
                <ul class="habblet-list room-more-data">
<?php
$i = 0;
$getem = $MySQLi->query("SELECT *,COUNT(vote) AS votes FROM rooms, room_votes WHERE room_votes.roomid = rooms.id GROUP BY id ORDER BY votes DESC LIMIT 15 OFFSET 5");

while ($row = $getem->fetch_assoc()) {
    if($row['owner'] !== ""){
        $i++;

        if($row['visitors_now'] == 0){ $row['visitors_now'] = 1; }
        $data[$i] = ($row['visitors_now'] / $row['visitors_max']) * 100;

        if($data[$i] == 99 || $data[$i] > 99){
            $room_fill = "5";
        } elseif($data[$i] > 65){
            $room_fill = "4";
        } elseif($data[$i] > 32){
            $room_fill = "3";
        } elseif($data[$i] > 0){
            $room_fill = "2";
        } elseif($data[$i] < 1){
            $room_fill = "1";
        }
		
		if($row['showname'] == "1"){ $roomname = $row['name']; }else{ $roomname = ""; } ?>
         <li class="<?php echo IsEven($i); ?>">
			<span class="clearfix enter-room-link room-occupancy-<?php echo $room_fill; ?>" title="Ir a la sala" roomid="<?php echo $row['id']; ?>">
				<span class="room-enter">Entrada</span>
				<span class="room-name"><?php echo HoloText($roomname); ?></span>
				<span class="room-description"><?php echo FilterText($row['description']); ?></span>
					<span class="room-owner">Due�@: <a href="<?php echo PATH; ?>/home/<?php echo $row['owner']; ?>"><?php echo HoloText($row['owner']); ?></a></span>
			</span>
		</li>	
<?php } } ?>
                </ul>
            </div>
            <div class="clearfix">
                <a href="#" class="room-toggle-more-data" id="room-toggle-more-data-h120">Mostrar más Salas</a>
            </div>
</div>
<script type="text/javascript">
L10N.put("show.more", "Mostrar más Salas");
L10N.put("show.less", "Mostrar menos Salas");
var roomListHabblet_h120 = new RoomListHabblet("rooms-habblet-list-container-h120", "room-toggle-more-data-h120", "room-more-data-h120");
</script>

<?php
}
else if($hid == "h121")
{
?>
<head>
<script src="<?php echo webgallery; ?>/static/js/moredata.js" type="text/javascript"></script>
</head>
<div id="hotgroups-habblet-list-container" class="habblet-list-container groups-list">
    <ul class="habblet-list two-cols clearfix">
<?php
$i = 0;
$j = 1;
$getem = $MySQLi->query("SELECT * FROM groups_details ORDER BY views DESC LIMIT 10");

while ($row = $getem->fetch_assoc()) {
        $i++;

        if(IsEven($i) == "even"){
            $left = "right";
        } else {
            $left = "left";
			$j++;
        }
		
        if(IsEven($j) == "even"){
            $even = "even";
        } else {
            $even = "odd";
        } ?>
		<li style="background-image: url(<?php echo PATH; ?>/habbo-imaging/badge/<?php echo $row['badge']; ?>.gif);" class="<?php echo $even; ?> <?php echo $left; ?>">
                <a href="<?php echo PATH; ?>/groups/<?php echo $row['id']; ?>/id" class="item"><span class="index"><?php echo $i; ?>.</span> <?php echo HoloText($row['name']); ?></a>
           </li>
		<?php } ?>
</ul>
    <div id="hotgroups-list-hidden-h122" style="display: none">
    <ul class="habblet-list two-cols clearfix">
<?php
$i = 10;
$j = 1;
$getem = $MySQLi->query("SELECT * FROM groups_details ORDER BY views DESC LIMIT 40 OFFSET 10");

while ($row = $getem->fetch_assoc()) {
        $i++;

        if(IsEven($i) == "even"){
            $left = "right";
        } else {
            $left = "left";
			$j++;
        }
		
        if(IsEven($j) == "even"){
            $even = "even";
        } else {
            $even = "odd";
        } ?>
			<li style="background-image: url(<?php echo PATH; ?>/habbo-imaging/badge/<?php echo $row['badge']; ?>.gif);" class="<?php echo $even; ?> <?php echo $left; ?>">
                <a href="<?php echo PATH; ?>/groups/<?php echo $row['id']; ?>/id" class="item"><span class="index"><?php echo $i; ?>.</span> <?php echo HoloText($row['name']); ?></a>
           </li>
<?php } ?>
</ul>
</div>
    <div class="clearfix">
        <a href="#" class="hotgroups-toggle-more-data secondary" id="hotgroups-toggle-more-data-h122">Mostrar más Grupos</a>
    </div>
<script type="text/javascript">
L10N.put("show.more.groups", "Mostrar más Grupos");
L10N.put("show.less.groups", "Mostrar menos Grupos");
var hotGroupsMoreDataHelper = new MoreDataHelper("hotgroups-toggle-more-data-h122", "hotgroups-list-hidden-h122","groups");
</script>
</div>

<?php
}
else if($hid == "h112")
{
?>
<head>
<script src="<?php echo webgallery; ?>/static/js/moredata.js" type="text/javascript"></script>
</head>
<ul class="active-discussions-toplist">
<?php
$i = 0;
$getem = $MySQLi->query("SELECT * FROM xdrcms_forum_threads ORDER BY unix DESC LIMIT 10");

while ($row = $getem->fetch_assoc()) {
        $i++;		
		$posts = mysql_evaluate("SELECT COUNT(*) FROM xdrcms_forum_posts WHERE threadid = '".$row['id']."'");
		$pages = ceil($posts / 10);
		$pagelink = "<a href=\"".PATH."/groups/".FilterText($row['forumid'])."/id/discussions/".$row['id']."/id\" class=\"topiclist-page-link secondary\">1</a>";
		if($pages > 4){
			$pageat = $pages - 2;
			$pagelink .= "\n...";
			while($pageat <= $pages){
				$pagelink .= " <a href=\"".PATH."/groups/".FilterText($row['forumid'])."/id/discussions/".$row['id']."/id/page/".$pageat."\" class=\"topiclist-page-link secondary\">".$pageat."</a>";
				$pageat++;
			}
		}elseif($pages != 1){
			$pageat = 2;
			while($pageat <= $pages){
				$pagelink .= " <a href=\"".PATH."/groups/".FilterText($row['forumid'])."/id/discussions/".$row['id']."/id/page/".$pageat."\" class=\"topiclist-page-link secondary\">".$pageat."</a>";
				$pageat++;
			}
		} ?>
		<li class="<?php echo IsEven($i); ?>" >
		<a href="<?php echo PATH; ?>/groups/<?php echo FilterText($row['forumid']); ?>/id/discussions/<?php echo $row['id']; ?>/id" class="topic">
		<span><?php echo $row['title']; ?></span>
		</a>
		<div class="topic-info post-icon">
		<span class="grey">(</span>
		<?php echo $pagelink; ?>
		<span class="grey">)</span>
		</div>
		</li>
		<?php } ?>
</ul>
<div id="active-discussions-toplist-hidden-h121" style="display: none">
    <ul class="active-discussions-toplist">
<?php
$i = 0;
$getem = $MySQLi->query("SELECT * FROM xdrcms_forum_threads ORDER BY unix DESC LIMIT 40 OFFSET 10") or die(mysql_error());

while ($row = $getem->fetch_assoc())
{
	$i++;
	$posts =  $MySQLi->query("SELECT COUNT(*) FROM xdrcms_forum_posts WHERE threadid = '".$row['id']."'")->num_rows();
	$pages = ceil($posts / 10);

	$pagelink = "<a href=\"".PATH."/groups/".FilterText($row['forumid'])."/id/discussions/".$row['id']."/id\" class=\"topiclist-page-link secondary\">1</a>";

	if($pages > 4)
	{
		$pageat = $pages - 2;
		$pagelink .= "\n...";

		while($pageat <= $pages)
		{
			$pagelink .= " <a href=\"".PATH."/groups/".FilterText($row['forumid'])."/id/discussions/".$row['id']."/id/page/".$pageat."\" class=\"topiclist-page-link secondary\">".$pageat."</a>";
			$pageat++;
		}
	}
	else if($pages != 1)
	{
		$pageat = 2;

		while($pageat <= $pages)
		{
			$pagelink .= " <a href=\"".PATH."/groups/".FilterText($row['forumid'])."/id/discussions/".$row['id']."/id/page/".$pageat."\" class=\"topiclist-page-link secondary\">".$pageat."</a>";
			$pageat++;
		}
	} ?>
			<li class="<?php echo IsEven($i); ?>" >
		<a href="<?php echo PATH; ?>/groups/<?php echo FilterText($row['forumid']); ?>/id/discussions/<?php echo $row['id']; ?>/id" class="topic">
		<span><?php echo $row['title']; ?></span>
		</a>
		<div class="topic-info post-icon">
		<span class="grey">(</span>
		<?php echo $pagelink; ?>
		<span class="grey">)</span>
		</div>
		</li>
		<?php } ?>
</ul>
</div>
<div class="clearfix">
    <a href="#" class="discussions-toggle-more-data secondary" id="discussions-toggle-more-data-h112">Mostrar más temas</a>
</div>
<script type="text/javascript">
L10N.put("show.more.discussions", "Mostrar más temas");
L10N.put("show.less.discussions", "Mostrar menos temas");
var discussionMoreDataHelper = new MoreDataHelper("discussions-toggle-more-data-h112", "active-discussions-toplist-hidden-h112","discussions");
</script>

<?php } else if($hid == "a811") {
?>
<div id="hotgroups-habblet-list-container" class="habblet-list-container groups-list">
    <ul class="habblet-list two-cols clearfix">
	<?php
$UsersRow = $MySQLi->query("SELECT * FROM users WHERE rank < 5 ORDER BY activity_points DESC LIMIT 5");
$I = 0;
$J = 0;
while($UserData = $UsersRow->fetch_assoc())
{        
$I++;
$J++;

       	if(IsEven($I) == "even")
	{
        	$left = "right";
        }
	else
	{
        	$left = "left";
        }

	if($J == 1)
		$even = "even";
	else if($J == 2)
		$even = "even";
	else if($J == 3)
		$even = "odd";
	else if($J == 4)
		$even = "odd";
	else if($J == 5)
		$even = "even";
	?>
            <li class="<?php echo $even; ?> <?php echo $left; ?>" style="background-image: url(&quot;http://www.habbo.com/habbo-imaging/avatarimage?figure=<?php echo $UserData["look"]; ?>&direction=4&head_direction=4&gesture=sml&action=&size=s&quot;);" "/>
                <a class="item" href="<?php echo PATH; ?>/home/<?php echo $UserData["id"]; ?>/id"><span class="index"><?php echo $I; ?>.</span> <?php echo $UserData["username"]; ?><span class="index">P�xeles: <?php echo $UserData["activity_points"]; ?></span></a>
            </li>

	<?php
}
?>
		</ul>
</div>
<?php } else if($hid == "a812") {
?>
<div id="hotgroups-habblet-list-container" class="habblet-list-container groups-list">
    <ul class="habblet-list two-cols clearfix">
	<?php
$UsersRow = $MySQLi->query("SELECT * FROM users WHERE rank < 5 ORDER BY credits DESC LIMIT 5");
$I = 0;
$J = 0;
while($UserData = $UsersRow->fetch_assoc())
{        
$I++;
$J++;

       	if(IsEven($I) == "even")
	{
        	$left = "right";
        }
	else
	{
        	$left = "left";
        }

	if($J == 1)
		$even = "even";
	else if($J == 2)
		$even = "even";
	else if($J == 3)
		$even = "odd";
	else if($J == 4)
		$even = "odd";
	else if($J == 5)
		$even = "even";
	?>
            <li class="<?php echo $even; ?> <?php echo $left; ?>" style="background-image: url(&quot;http://www.habbo.com/habbo-imaging/avatarimage?figure=<?php echo $UserData["look"]; ?>&direction=4&head_direction=4&gesture=sml&action=&size=s&quot;);" "/>
                <a class="item" href="<?php echo PATH; ?>/home/<?php echo $UserData["id"]; ?>/id"><span class="index"><?php echo $I; ?>.</span> <?php echo $UserData["username"]; ?><span class="index">Cr�ditos: <?php echo $UserData["credits"]; ?></span></a>
            </li>

	<?php
}
?>
		</ul>
</div>
<?php } ?>