<?php
if(!isset($_POST['next'], $_POST['registrationBean_month'], $_POST['registrationBean_day'], $_POST['registrationBean_year'], $_POST['registrationBean_email'], $_POST['registrationBean_password'], $_POST['g-recaptcha-response'], $_POST['registrationBean_parentEmail'])):
	echo utf8_encode('{"registrationErrors":{"registration_password":"Escribe una contraseña","registration_birthday_format":"Por favor, debes proporcionar una fecha v?lida","registration_termsofservice":"Por favor, debes aceptar los t?rminos.","registration_email":"Por favor, introduce una direcci?n de email v?lida","registration_captcha":"El c?digo de seguridad no era v?lido. Por favor, int?ntalo de nuevo."}}');
	exit;
endif;

$responseJson = ['registrationErrors' => ['empty_field_error_message' => 'Hey, you forgot to fill me!']];

if(empty($_POST['registrationBean_password'])):
	$responseJson['registrationErrors']['registration_password'] = utf8_encode('Escribe una contrase?a');
elseif(strlen($_POST['registrationBean_password']) < 6):
	$responseJson['registrationErrors']['registration_password'] = utf8_encode('Tu contrase?a debe tener al menos 6 caracteres');
elseif(strlen($_POST['registrationBean_password']) > 32):
	$responseJson['registrationErrors']['registration_password'] = utf8_encode('Tu contrase?a es muy larga');
elseif(!preg_match('`[0-9]`', $_POST['registrationBean_password'])):
	$responseJson['registrationErrors']['registration_password'] = utf8_encode('Tu contrase?a debe incluir alg?n n?mero');
endif;

if(!USER::CheckAge($_POST['registrationBean_day'], $_POST['registrationBean_month'], $_POST['registrationBean_year'])):
	$responseJson['registrationErrors']['registration_birthday_format'] = utf8_encode('Por favor, debes proporcionar una fecha v?lida');
endif;

if(!isset($_POST['registrationBean_termsOfServiceSelection']) || $_POST['registrationBean_termsOfServiceSelection'] !== 'true')
	$responseJson['registrationErrors']['registration_termsofservice'] = utf8_encode('Por favor, debes aceptar los t?rminos.');
if(!isset($_POST['registrationBean_cookiePolicySelection']) || $_POST['registrationBean_cookiePolicySelection'] !== 'true')
	$responseJson['registrationErrors']['registration_cookiepolicy'] = utf8_encode('Por favor, acepta la pol?tica de cookies');
	
$_POST['registrationBean_email'] = str_replace('%40', '@', strtolower($_POST['registrationBean_email']));

if(empty($_POST['registrationBean_email']) || strlen($_POST['registrationBean_email']) > 50 || !filter_var($_POST['registrationBean_email'], FILTER_VALIDATE_EMAIL))
	$responseJson['registrationErrors']['registration_email'] = utf8_encode('Por favor, introduce una direcci?n de email v?lida');

if($responseJson['registrationErrors'] != ['empty_field_error_message' => 'Hey, you forgot to fill me!']):
	echo json_encode($responseJson);
	exit;
elseif(!METHOD::CheckCaptcha($_POST['g-recaptcha-response'])):
	$responseJson['registrationErrors']['registration_captcha'] = utf8_encode('El c?digo de seguridad no era v?lido. Por favor, int?ntalo de nuevo.');
	echo json_encode($responseJson);
	exit;
endif;


$r = USER::SomeRegistered($_POST['registrationBean_email']);
if($r === null || $r === true):
	$responseJson['registrationErrors']['registration_email'] = utf8_encode('El email proporcionado ya est? siendo usado. Si intentas crear un nuevo personaje, por favor con?ctate con tu ' . $hotelName . ' cuenta y haz clic en A?adir personaje.');
	echo json_encode($responseJson);
	exit;
endif;

$RegisterSettings = CACHE::GetAIOConfig('Register');
if(!$RegisterSettings['register_enabled']):
	$responseJson["registrationErrors"]['registration_password'] = 'El registro est? desactivado, int?ntalo m?s tarde.';
	echo json_encode($responseJson);
	exit;
endif;

$userName = USER::GenerateName($_POST['registrationBean_email']);
if($userName === null):
	$responseJson["registrationErrors"]['registration_password'] = 'Error 505 L59. Try later.';
	echo json_encode($responseJson);
	exit;
endif;

$birth = $_POST['registrationBean_day'] . '-' . $_POST['registrationBean_month'] . '-' . $_POST['registrationBean_year'];

$IpC = $MySQLi->query('SELECT COUNT(*) FROM users WHERE ip_last = \'' . MY_IP . '\'')->fetch_assoc()['COUNT(*)'];
if($RegisterSettings['limitips'] != 0 && $IpC >= $RegisterSettings['limitips']):
	$responseJson['registrationErrors']['registration_password'] = utf8_encode('Has sobrepasado el l?mite de usuarios.');
	echo json_encode($responseJson);
	exit;
endif;

$q = $MySQLi->query('INSERT INTO users (username, rank, credits, account_created, block_newfriends, ip_last, look, gender, motto, online) 
					 VALUES (\'' . $userName . '\', 1, ' . $SiteSettings['initial.credits.int'] . ', ' . time() . ', \'0\', \'' . MY_IP . '\', \'\', \'M\', \'\', \'0\')');

if($q == null || $MySQLi->affected_rows !== 1):
	$responseJson['registrationErrors']['registration_password'] = 'Error MySQLi';
	echo json_encode($responseJson);
	exit;
endif;

$userid = $MySQLi->insert_id;
$MySQLi->query('REPLACE INTO xdrcms_users_data (id, mail, password, birth, rpx_id, rpx_type, web_online, AddonData, RememberMeToken, AccountID, AccountIdentifier, AccountPhoto, securityTokens) VALUES (' . $userid . ', \'' . $_POST['registrationBean_email'] . '\', \'' . METHOD::HASH($_POST['registrationBean_password']) . '\', \'' . $birth . '\', ' . METHOD::RANDOM(12, true, false) . ', \'habboid\', \'--\', \'\', \'\', \'\', \'\', \'\', \'\')');

if(isset($_SESSION['Register']['RefId']) && is_numeric($_SESSION['Register']['RefId']) && $IpC == 0)
	USER::NewRefer($userid, $_SESSION['Register']['RefId']);

USER::LOGIN($userid, $_POST['registrationBean_email'], METHOD::HASH($_POST['registrationBean_password']), 'habboid', false);
echo '{"registrationCompletionRedirectUrl":"' . str_replace('/', '\\/', PATH . '/' . $RegisterSettings['redirection']) . '"}';

ITEM::PLACE($userid, 0, '180', '366', '11', '', 's_paper_clip_1', '', 'sticker');
ITEM::PLACE($userid, 0, '130', '22', '10', '', 's_needle_3', '', 'sticker');
ITEM::PLACE($userid, 0, '280', '343', '3', '', 's_sticker_spaceduck', '', 'sticker');
ITEM::PLACE($userid, 0, '593', '11', '9', '', 's_sticker_arrow_down', '', 'sticker');

ITEM::PLACE($userid, 0, '107', '402', '8', '', 'n_skin_notepadskin', 'note.myfriends', 'stickie');
ITEM::PLACE($userid, 0, '57', '229', '6', '', 'n_skin_speechbubbleskin', 'note.welcome', 'stickie');
ITEM::PLACE($userid, 0, '148', '41', '7', '', 'n_skin_noteitskin', 'note.remember.security', 'stickie');

ITEM::PLACE($userid, 0, '457', '26', '4', 'ProfileWidget', 'w_skin_defaultskin', '', 'widget');
ITEM::PLACE($userid, 0, '450', '319', '1', 'RoomsWidget', 'w_skin_notepadskin', '', 'widget');
	
//if($SiteSettings['initial.credits.int'] > 0):
//	newTransaction($userid, $date_full, $SiteSettings['initial.credits.int'], $System->CorrectStr('?Bienvenido a ' . $hotelName . '!'));
//endif;

unset($_SESSION['Registration']);
$_SESSION['client_login'] = true;
$_SESSION['next_login'] = true;

exit;
?>