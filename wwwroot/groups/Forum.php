<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$no_rea = true;

$pagename = "Foro de Discusi�n";
$pageid = "groupforum";
$body_id = "viewmode";
$the_forum = true;

require "../Kernel/Init.php";

if(isset($_GET['id']) && is_numeric($_GET['id'])):
	$check = $MySQLi->query("SELECT * FROM groups_details WHERE id = '" . $_GET['id'] . "' LIMIT 1");

	if($check->num_rows > 0):
		$groupid = $_GET['id'];

		$error = false;
		$groupdata = $check->fetch_assoc();

		$pagename = "Grupo: ".$groupdata['name'];
		$ownerid = $groupdata['ownerid'];

		$members = $MySQLi->query("SELECT COUNT(*) FROM groups_memberships WHERE groupid = '".$groupid."' AND is_pending = '0'")->num_rows;

		$check = $MySQLi->query("SELECT * FROM groups_memberships WHERE userid = '".$my_id."' AND groupid = '".$groupid."' AND is_pending = '0' LIMIT 1");
		$is_member = $check->num_rows;

		if($is_member > 0 && LOGGED_IN == TRUE):
			$is_member = true;
			$my_membership = $check->fetch_assoc();
			$member_rank = $my_membership['member_rank'];

		else:
			$is_member = false;
		endif;
	else:
		$error = true;
	endif;
else:
	$error = true;
endif;

if($error != true)
{
		require(Files . $Config["Lang"] . '/Templates/Headers/Community.php');

$viewtools = "<div class=\"myhabbo-view-tools\">\n";

$page = $_GET['page'];

$threads = $MySQLi->query("SELECT COUNT(*) FROM xdrcms_forum_threads WHERE forumid = '".$groupid."'")->num_rows;
$pages = ceil(($threads + 0) / 10);

if($page > $pages || $page < 1)
	$page = 1;

$key = 0;
?>

<div id="container">
	<div id="content" style="position: relative" class="clearfix">
    <div id="mypage-wrapper" class="cbb blue">
<div class="box-tabs-container box-tabs-left clearfix">
	
    <h2 class="page-owner">
<?php echo $groupdata['name']; ?>&nbsp;
<?php if($groupdata['type'] == "2"){ ?><img src='<?php echo webgallery; ?>/images/status_closed_big.gif' alt='Grupo cerrado' title='Grupo cerrado'><?php } ?>
<?php if($groupdata['type'] == "1"){ ?><img src='<?php echo webgallery; ?>/images/status_exclusive_big.gif' alt='Grupo moderado' title='Grupo moderado'><?php } ?></h2>
</h2>
    <ul class="box-tabs">
        <li><a href="<?php echo PATH; ?>/groups/<?php echo $groupid; ?>/id">Página Principal</a><span class="tab-spacer"></span></li>
        <li class="selected"><a href="<?php echo PATH; ?>/groups/<?php echo $groupid; ?>/id/discussions">Foro de Discusi�n <?php if($groupdata['pane'] > 0) { ?><img src="<?php echo webgallery; ?>/images/groups/status_exclusive.gif"><?php } ?></a><span class="tab-spacer"></span></li>
    </ul>
</div>	

	<div id="mypage-content">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="content-1col">
            <tr>
                <td valign="top" style="width: 750px;" class="habboPage-col rightmost">
                    <div id="discussionbox">
<?php
if($groupdata['pane'] > 0)
{
	$sql = $MySQLi->query("SELECT * FROM groups_memberships WHERE userid = '".$my_id."' AND is_pending <> '1' AND groupid = '".$groupid."'");
	$member = $sql->fetch_assoc();

	if($sql->num_rows > 0) { ?>
<div id="group-topiclist-container">
<div class="topiclist-header clearfix">
<input type="hidden" id="email-verfication-ok" value="1"/>
		
		<?php
		$sql = $MySQLi->query("SELECT * FROM groups_details WHERE id = '".$groupid."' LIMIT 1");
		$row = $sql->fetch_assoc();
		if($row['topics'] == 0) {
		if(LOGGED != "null"){ ?>
		<a href="#" id="newtopic-upper" class="new-button verify-email newtopic-icon" style="float:left"><b><span></span>Nuevo Asunto</b><i></i></a><?php } else { echo "Con�ctate para publicar nuevos asuntos"; } }elseif($row['topics'] == 1) {
		$check = $MySQLi->query("SELECT * FROM groups_memberships WHERE userid = '".$my_id."' AND groupid='".$groupid."' AND is_pending <> '1' LIMIT 1");
		if($check->num_rows > 0) { ?>
        <?php if(LOGGED != "null"){ ?><a href="#" id="newtopic-upper" class="new-button verify-email newtopic-icon" style="float:left"><b><span></span>Nuevo Asunto</b><i></i></a><?php } else { echo "Con�ctate para publicar nuevos asuntos"; } } }elseif($row['topics'] == 2) {
		$check = $MySQLi->query("SELECT * FROM groups_memberships WHERE userid = '".$my_id."' AND groupid = '".$_GET['id']."' AND member_rank = '2' AND is_pending <> '1' LIMIT 1");
		if($check->num_rows > 0) { ?>
        <?php if($logged_in){ ?><a href="#" id="newtopic-upper" class="new-button verify-email newtopic-icon" style="float:left"><b><span></span>Nuevo Asunto</b><i></i></a><?php } else { echo "Con�ctate para publicar nuevos asuntos"; } } } ?>
		
    <div class="page-num-list">
    Ver página:
<?php
	for ($i = 1; $i <= $pages; $i++)
	{
		if($page == $i)
			echo $i."\n";
		else
			echo "<a href=\"".PATH."/groups/".$groupid."/id/dicussions?page=".$i."\" class=\"topiclist-page-link\">".$i."</a>\n";
	} 
?>
    </div>
</div>
<table class="group-topiclist" border="0" cellpadding="0" cellspacing="0" id="group-topiclist-list">
	<tr class="topiclist-columncaption">
		<td class="topiclist-columncaption-topic">Asunto</td>
		<td class="topiclist-columncaption-lastpost">�ltima respuesta</td>
		<td class="topiclist-columncaption-replies">Respuestas</td>
		<td class="topiclist-columncaption-views">Visitas</td>
	</tr>	
<?php if($threads == 0){ ?>
<tr class="topiclist-row-1">
		<td class="topiclist-rowtopic" valign="top">
			No hay asuntos por mostrar. �Crea uno ahora!
		</td>
		</tr>
<?php }

$sql = $MySQLi->query("SELECT * FROM xdrcms_forum_threads WHERE type > 2 AND forumid= '".$groupid."' ORDER BY unix DESC");
$stickies = $sql->num_rows;

$query_min = ($page * 10) - 10;
$query_max = 10;

$query_max = $query_max - $stickies;
$query_min = $query_min - $stickies;

if($query_min < 0){ // Page 1
$query_min = 0;
}

while($row = $sql->fetch_assoc())
{
	$key++;

	if(IsEven($key))
		$x = "odd";
	else 
		$x = "even";
?>

<tr class="topiclist-row-<?php echo $x; ?>">
		<td class="topiclist-rowtopic" valign="top">
			<div class="topiclist-row-content">
			<a class="topiclist-link icon icon-sticky" href="<?php echo webgallery; ?>/groups/<?php echo $groupid; ?>/id/discussions/<?php echo $row['id']; ?>/id"><?php echo $row['title']; ?></a>

			<?php if($row['type'] == 4){ ?>
			&nbsp;<span class="topiclist-row-topicsticky"><img src="<?php echo webgallery; ?>/images/groups/status_closed.gif" title="Asunto cerrado" alt="Asunto cerrado"></span>
			<?php } ?>
			&nbsp;(página
			<?php
			$thread_pages = ceil(($row['posts'] + 1) / 10);

			for ($i = 1; $i <= $thread_pages; $i++){
				echo "<a href=\"".PATH."/groups/".$groupid."/id/discussions/".$row['id']."/id/page/".$i."\" class=\"topiclist-page-link\">".$i."</a>\n";
			} ?>
            )
			<br />
			<span><a class="topiclist-row-openername" href="<?php echo webgallery; ?>/home/<?php echo $row['author']; ?>"><?php echo $row['author']; ?></a></span>
			<?php
			$date_bits = explode(" ", $row['date']);
			$date = $date_bits[0];
			$time = $date_bits[1]; ?>
			
			&nbsp;<span class="latestpost"><?php echo $date; ?></span>
			<span class="latestpost">(<?php echo $time; ?>)</span>
			</div>
		</td>
		<td class="topiclist-lastpost" valign="top">
		    <a class="lastpost-page-link" href="<?php echo webgallery; ?>/groups/<?php echo $groupid; ?>/id/discussions/<?php echo $row['id']; ?>/id/&sp=JumpToLast">
			
			<?php 
			$date_bits = explode(" ", $row['lastpost_date']);
			$date = $date_bits[0];
			$time = $date_bits[1];
			?>

			<span class="lastpost"><?php echo $date; ?></span>
            <span class="lastpost">(<?php echo $time; ?>)</span></a><br />
			<span class="topiclist-row-writtenby">por:</span> <a class="topiclist-row-openername" href="<?php echo webgallery; ?>/home/<?php echo $row['lastpost_author']; ?>"><?php echo $row['lastpost_author']; ?></a>&nbsp;
		</td>
 		<td class="topiclist-replies" valign="top"><?php echo $row['posts']; ?></td>
 		<td class="topiclist-views" valign="top"><?php echo $row['views']; ?></td>
	</tr>
<?php }

$sql = $MySQLi->query("SELECT * FROM xdrcms_forum_threads WHERE type < 3 AND forumid= '".$groupid."' ORDER BY unix DESC LIMIT ".$query_min.", ".$query_max."");

while($row = $sql->fetch_assoc()){

	$key++;

	if(IsEven($key)){
		$x = "odd";
	} else {
		$x = "even";
	}  
?>

	<tr class="topiclist-row-<?php echo $x; ?>">
		<td class="topiclist-rowtopic" valign="top">
			<div class="topiclist-row-content">
			<a class="topiclist-link " href="<?php echo webgallery; ?>/groups/<?php echo $groupid; ?>/id/discussions/<?php echo $row['id']; ?>/id"><?php echo $row['title']; ?></a>

			<?php if($row['type'] == 2){ ?>
			&nbsp;<span class="topiclist-row-topicsticky"><img src="<?php echo webgallery; ?>/images/groups/status_closed.gif" title="Asunto cerrado" alt="Asunto cerrado"></span>
			<?php } ?>
			&nbsp;(página 
			<?php
			$thread_pages = ceil(($row['posts'] + 1) / 10);

			for ($i = 1; $i <= $thread_pages; $i++){
				echo "<a href=\"".PATH."/groups/".$groupid."/id/discussions/".$row['id']."/id/page/" . $i . "\" class=\"topiclist-page-link\">" . $i . "</a>\n";
			} 

            ?>
			)
			<br />
			<span><a class="topiclist-row-openername" href="<?php echo webgallery; ?>/home/<?php echo $row['author']; ?>"><?php echo $row['author']; ?></a></span>
			<?php
			$date_bits = explode(" ", $row['date']);
			$date = $date_bits[0];
			$time = $date_bits[1];
			?>
			
			&nbsp;<span class="latestpost"><?php echo $date; ?></span>
			<span class="latestpost">(<?php echo $time; ?>)</span>
			</div>
		</td>
		<td class="topiclist-lastpost" valign="top">
		    <a class="lastpost-page-link" href="<?php echo webgallery; ?>/groups/<?php echo $groupid; ?>/id/discussions/<?php echo $row['id']; ?>/id/&sp=JumpToLast">
		<?php
			$date_bits = explode(" ", $row['lastpost_date']);
			$date = $date_bits[0];
			$time = $date_bits[1];
		?>
			<span class="lastpost"><?php echo $date; ?></span>
            <span class="lastpost">(<?php echo $time; ?>)</span></a><br />
			<span class="topiclist-row-writtenby">por:</span> <a class="topiclist-row-openername" href="<?php echo webgallery; ?>/home/<?php echo $row['lastpost_author']; ?>"><?php echo $row['lastpost_author']; ?></a>&nbsp;
		</td>
 		<td class="topiclist-replies" valign="top"><?php echo $row['posts']; ?></td>
 		<td class="topiclist-views" valign="top"><?php echo $row['views']; ?></td>
	</tr>
	<?php } ?>	

	</table>
<div class="topiclist-footer clearfix">
<input type="hidden" id="email-verfication-ok" value="1"/>
		<?php
		$sql = $MySQLi->query("SELECT * FROM groups_details WHERE id = '".$groupid."' LIMIT 1");
		$row = $sql->fetch_assoc();

		if($row['topics'] == 0) { if(LOGGED != "null"){ ?>
		<a href="#" id="newtopic-upper" class="new-button verify-email newtopic-icon" style="float:left"><b><span></span>Nuevo Asunto</b><i></i></a><?php } else { echo "Con�ctate para publicar nuevos asuntos"; }
		}elseif($row['topics'] == 1) {
		$check = $MySQLi->query("SELECT * FROM groups_memberships WHERE userid = '".$my_id."' AND groupid = '".$groupid."' LIMIT 1");
		if($check->num_rows > 0) { ?>
        <?php if(LOGGED != "null"){ ?>
		<a href="#" id="newtopic-upper" class="new-button verify-email newtopic-icon" style="float:left"><b><span></span>Nuevo Asunto</b><i></i></a><?php } else { echo "Con�ctate para publicar nuevos asuntos"; }
		} }elseif($row['topics'] == 2) {
		$check = $MySQLi->query("SELECT * FROM groups_memberships WHERE userid='".$my_id."' AND groupid = '".$groupid."' AND member_rank='2' LIMIT 1");
		if($check->num_rows > 0) { ?>
        <?php if(LOGGED != "null"){ ?><a href="#" id="newtopic-upper" class="new-button verify-email newtopic-icon" style="float:left"><b><span></span>Nuevo Asunto</b><i></i></a><?php } else { echo "Con�ctate para publicar nuevos asuntos"; }
	} } ?>
    <div class="page-num-list">
    Ver página:
<?php
	for ($i = 1; $i <= $pages; $i++){
		if($page == $i){
			echo $i . "\n";
		} else {
			echo "<a href=\"".PATH."/groups/".$groupid."/id/dicussions?page=".$i."\" class=\"topiclist-page-link\">".$i."</a>\n";
		}
	}
?>
    </div>
<?php } else { ?>
<h1>Oops...</h1>

<p>
Lo sentimos, pero no puedes acceder a este Foro de Discusi�n. Necesitas ser un miembro del grupo antes. <br />

</p>
<?php } } else { ?>
<div id="group-topiclist-container">
<div class="topiclist-header clearfix">
        <input type="hidden" id="email-verfication-ok" value="1"/>
		<?php
		$sql = $MySQLi->query("SELECT * FROM groups_details WHERE id = '".$groupid."' LIMIT 1");
		$row = $sql->fetch_assoc();

		if($row['topics'] == 0) { ?>		
        <?php if(LOGGED != "null"){ ?><a href="#" id="newtopic-upper" class="new-button verify-email newtopic-icon" style="float:left"><b><span></span>Nuevo Asunto</b><i></i></a><?php } else { echo "Con�ctate para publicar nuevos asuntos"; }
		}elseif($row['topics'] == 1) {
		$check = $MySQLi->query("SELECT * FROM groups_memberships WHERE userid='".$my_id."' AND groupid='".$_GET['id']."' LIMIT 1");
		if($check->num_rows > 0) { ?>
        <?php if(LOGGED != "null"){ ?><a href="#" id="newtopic-upper" class="new-button verify-email newtopic-icon" style="float:left"><b><span></span>Nuevo Asunto</b><i></i></a><?php } else { echo "Con�ctate para publicar nuevos asuntos"; } }
	} elseif($row['topics'] == 2) {
	$check = $MySQLi->query("SELECT * FROM groups_memberships WHERE userid='".$my_id."' AND groupid='".$_GET['id']."' AND member_rank='2' LIMIT 1");
		if($check->num_rows > 0) { ?>
        <?php if($logged_in){ ?><a href="#" id="newtopic-upper" class="new-button verify-email newtopic-icon" style="float:left"><b><span></span>Nuevo Asunto</b><i></i></a><?php } else { echo "Con�ctate para publicar nuevos asuntos"; } } }
?>
    <div class="page-num-list">
    Ver página:
<?php
	for ($i = 1; $i <= $pages; $i++){
		if($page == $i){
			echo $i . "\n";
		} else {
			echo "<a href=\"".PATH."/groups/".$groupid."/id/dicussions?page=".$i."\" class=\"topiclist-page-link\">".$i."</a>\n";
		}
	} 
?>
    </div>
</div>
<table class="group-topiclist" border="0" cellpadding="0" cellspacing="0" id="group-topiclist-list">
	<tr class="topiclist-columncaption">

		<td class="topiclist-columncaption-topic">Asunto</td>
		<td class="topiclist-columncaption-lastpost">�ltimo mensaje</td>
		<td class="topiclist-columncaption-replies">Respuestas</td>
		<td class="topiclist-columncaption-views">Opiniones</td>
	</tr>
<?php

if($threads == 0){
echo "	<tr class=\"topiclist-row-1\">
		<td class=\"topiclist-rowtopic\" valign=\"top\">
			No hay asuntos por mostrar. �Crea uno ahora!
		</td>
		</tr>";
}

$sql = $MySQLi->query("SELECT * FROM xdrcms_forum_threads WHERE type > 2 AND forumid='".$groupid."' ORDER BY unix DESC");
$stickies = $sql->num_rows;

$query_min = ($page * 10) - 10;
$query_max = 10;

$query_max = $query_max - $stickies;
$query_min = $query_min - $stickies;

if($query_min < 0){
$query_min = 0;
}

while($row = $sql->fetch_assoc()){

	$key++;

	if(IsEven($key)){
		$x = "odd";
	} else {
		$x = "even";
	}

	echo "<tr class=\"topiclist-row-" . $x . "\">
		<td class=\"topiclist-rowtopic\" valign=\"top\">
			<div class=\"topiclist-row-content\">
			<a class=\"topiclist-link icon icon-sticky\" href=\"".PATH."/groups/".$groupid."/id/discussions/".$row['id']."/id\">" . $row['title'] . "</a>";

			if($row['type'] == 4){
			echo "&nbsp;<span class=\"topiclist-row-topicsticky\"><img src=\"".PATH."/web-gallery/images/groups/status_closed.gif\" title=\"Asunto cerrado\" alt=\"Asunto cerrado\"></span>";
			}

			echo "&nbsp;(página ";

			$thread_pages = ceil(($row['posts'] + 1) / 10);

			for ($i = 1; $i <= $thread_pages; $i++){
				echo "<a href=\"".PATH."/groups/".$groupid."/id/discussions/".$row['id']."/id/page/" . $i . "\" class=\"topiclist-page-link\">" . $i . "</a>\n";
			} 

            echo ")
			<br />
			<span><a class=\"topiclist-row-openername\" href=\"".PATH."/home/" . $row['author'] . "\">" . $row['author'] . "</a></span>";

			$date_bits = explode(" ", $row['date']);
			$date = $date_bits[0];
			$time = $date_bits[1];
			
				echo "&nbsp;<span class=\"latestpost\">" . $date . "</span>
			<span class=\"latestpost\">(" . $time . ")</span>
			</div>
		</td>
		<td class=\"topiclist-lastpost\" valign=\"top\">
		    <a class=\"lastpost-page-link\" href=\"".PATH."/groups/".$groupid."/id/discussions/".$row['id']."/id/&sp=JumpToLast\">";

			$date_bits = explode(" ", $row['lastpost_date']);
			$date = $date_bits[0];
			$time = $date_bits[1];

				echo "<span class=\"lastpost\">" . $date . "</span>
            <span class=\"lastpost\">(" . $time . ")</span></a><br />
			<span class=\"topiclist-row-writtenby\">por:</span> <a class=\"topiclist-row-openername\" href=\"".PATH."/home/" . $row['lastpost_author'] . "\">" . $row['lastpost_author'] . "</a>&nbsp;
		</td>
 		<td class=\"topiclist-replies\" valign=\"top\">" . $row['posts'] . "</td>
 		<td class=\"topiclist-views\" valign=\"top\">" . $row['views'] . "</td>
	</tr>";

}

$sql = $MySQLi->query("SELECT * FROM xdrcms_forum_threads WHERE type < 3 AND forumid='".$groupid."' ORDER BY unix DESC LIMIT ".$query_min.", ".$query_max."");

while($row = $sql->fetch_assoc()){

	$key++;

	if(IsEven($key)){
		$x = "odd";
	} else {
		$x = "even";
	}

	echo "<tr class=\"topiclist-row-" . $x . "\">
		<td class=\"topiclist-rowtopic\" valign=\"top\">
			<div class=\"topiclist-row-content\">
			<a class=\"topiclist-link \" href=\"".PATH."/groups/".$groupid."/id/discussions/".$row['id']."/id\">" . $row['title'] . "</a>";

			if($row['type'] == 2){
			echo "&nbsp;<span class=\"topiclist-row-topicsticky\"><img src=\"".PATH."/web-gallery/images/groups/status_closed.gif\" title=\"Asunto cerrado\" alt=\"Asunto cerrado\"></span>";
			}

			echo "&nbsp;(página ";

			$thread_pages = ceil(($row['posts'] + 1) / 10);

			for ($i = 1; $i <= $thread_pages; $i++){
				echo "<a href=\"".PATH."/groups/".$groupid."/id/discussions/".$row['id']."/id/page/" . $i . "\" class=\"topiclist-page-link\">" . $i . "</a>\n";
			} 

            echo ")
			<br />
			<span><a class=\"topiclist-row-openername\" href=\"".PATH."/home/" . $row['author'] . "\">" . $row['author'] . "</a></span>";

			$date_bits = explode(" ", $row['date']);
			$date = $date_bits[0];
			$time = $date_bits[1];
			
				echo "&nbsp;<span class=\"latestpost\">" . $date . "</span>
			<span class=\"latestpost\">(" . $time . ")</span>
			</div>
		</td>
		<td class=\"topiclist-lastpost\" valign=\"top\">
		    <a class=\"lastpost-page-link\" href=\"".PATH."/groups/".$groupid."/id/discussions/".$row['id']."/id/&sp=JumpToLast\">";

			$date_bits = explode(" ", $row['lastpost_date']);
			$date = $date_bits[0];
			$time = $date_bits[1];

				echo "<span class=\"lastpost\">" . $date . "</span>
            <span class=\"lastpost\">(" . $time . ")</span></a><br />
			<span class=\"topiclist-row-writtenby\">por:</span> <a class=\"topiclist-row-openername\" href=\"".PATH."/home/" . $row['lastpost_author'] . "\">" . $row['lastpost_author'] . "</a>&nbsp;
		</td>
 		<td class=\"topiclist-replies\" valign=\"top\">" . $row['posts'] . "</td>
 		<td class=\"topiclist-views\" valign=\"top\">" . $row['views'] . "</td>
	</tr>";

}

?>	

	</table>
<div class="topiclist-footer clearfix">
<input type="hidden" id="email-verfication-ok" value="1"/>
<?php
	$sql = $MySQLi->query("SELECT * FROM groups_details WHERE id='".$groupid."' LIMIT 1");
	$row = $sql->fetch_assoc();

	if($row['topics'] == 0)
	{ 
		if(LOGGED_IN)
		{ ?>
			<a href="#" id="newtopic-upper" class="new-button verify-email newtopic-icon" style="float:left"><b><span></span>Nuevo Asunto</b><i></i></a><?php } else { echo "Con�ctate para publicar nuevos asuntos"; }
		}
		elseif($row['topics'] == 1)
		{
			$check = $MySQLi->query("SELECT * FROM groups_memberships WHERE userid='".$my_id."' AND groupid='".$groupid."' AND is_pending <> '1' LIMIT 1");
			if($check->num_rows > 0)
			{ ?>
        			<?php if(LOGGED_IN)
				{ ?>
					<a href="#" id="newtopic-upper" class="new-button verify-email newtopic-icon" style="float:left"><b><span></span>Nuevo Asunto</b><i></i></a>
				<?php
				}
				else
					echo "Con�ctate para publicar nuevos asuntos";
			}
		}
		elseif($row['topics'] == 2)
		{
			$check = $MySQLi->query("SELECT * FROM groups_memberships WHERE userid='".$my_id."' AND groupid='".$groupid."' AND member_rank='2' AND is_pending <> '1' LIMIT 1");

			if($check->num_rows > 0)
			{ ?>
       				 <?php if(LOGGED_IN){ ?>
					<a href="#" id="newtopic-upper" class="new-button verify-email newtopic-icon" style="float:left"><b><span></span>Nuevo Asunto</b><i></i></a>
			<?php
			}
			else
				echo "Con�ctate para publicar nuevos asuntos";
		}
	}
?>
    <div class="page-num-list">
    Ver página:
<?php
	for ($i = 1; $i <= $pages; $i++)
	{
		if($page == $i)
			echo $i . "\n";
		else
			echo "<a href=\"".PATH."/groups/".$groupid."/discussions?page=" . $i . "\" class=\"topiclist-page-link\">" . $i . "</a>\n";
	}
?>
    </div>
<?php } ?>
</div>
</div>

<script type="text/javascript" language="JavaScript">
L10N.put("myhabbo.discussion.error.topic_name_empty", "El Asunto no debe estar vacio");
Discussions.initialize("<?php echo $groupid; ?>", "<?php echo webgallery; ?>/groups/<?php echo $groupid; ?>/discussions", null);
</script>
                    </div>
					
                </td>
                <td style="width: 4px;"></td>
                <td valign="top" style="width: 164px;">
    <div class="habblet ">
    
    </div>
                </td>
            </tr>
        </table>
    </div>
</div>

<script type="text/javascript">
	Event.observe(window, "load", observeAnim);
	document.observe("dom:loaded", initDraggableDialogs);
</script>
    </div>
<?php
		require(Files . $Config["Lang"] . '/Templates/Footers/Login.php');
?>
</div>

</div>
<?php
}
else
{
	$cored = true;
	require "../error.php";
}
?>