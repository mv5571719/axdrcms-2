<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

/*
$_c = socket_create(AF_INET, SOCK_STREAM, getprotobyname('tcp'));
socket_connect($_c, '127.0.0.1', 30005);
socket_write($_c, 'localhost:password');
socket_close($_c);
*/

define('Start', microtime(true)); 
require '../KERNEL-XDRCMS/Init.php';

$pagename = 'Home';
$pageid = 'home';

USER::REDIRECT(1);

require HEADER . 'community.php';
require HTML . 'Community_me.html';
require FOOTER . 'community.php';


echo '<!--Loaded in '.(microtime(true) - Start).' seconds-->'; 
?>