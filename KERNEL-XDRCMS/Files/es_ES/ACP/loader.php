<?php
$PageName = 'SWF';
$cSettings = CACHE::GetAIOConfig('Client');

if($do == 'save' && isset($_POST['external_variables_txt'], $_POST['external_texts_txt'], $_POST['safechat_list_txt'], $_POST['productdata_load_url'], $_POST['furnidata_load_url'], $_POST['flash_client_url'], $_POST['params__base'], $_POST['clientUrl'], $_POST['client_starting'], $_POST['hotelview_banner_url'], $_POST['managed_override'])):
	if(empty($_POST['external_variables_txt']) || empty($_POST['flash_client_url']) || empty($_POST['params__base']) || empty($_POST['clientUrl']) || !is_numeric($_POST['managed_override'])):
		$msg_error = 'No dejes campos obligatorios en blanco.';
	elseif(!checkAntiCsrf()):
		$msg_error = 'Secret key inválida. Porfavor, logueate con la secret key.';
	else:
		$cSettings['managed.override'] = $_POST['managed_override'] == '1';
		$cSettings['Txts'] = ['external.variables.txt' => $_POST['external_variables_txt'],
				'external.texts.txt' => $_POST['external_texts_txt'],
				'safechat.list.txt' => $_POST['safechat_list_txt'],
				'external.override.texts.txt' => isset($_POST['external_override_texts_txt']) ? $_POST['external_override_texts_txt'] : $cSettings['Txts']['external.override.texts.txt'],
				'external.override.variables.txt' => isset($_POST['external_override_variables_txt']) ? $_POST['external_override_variables_txt'] : $cSettings['Txts']['external.override.variables.txt'],
				'productdata.load.url' => $_POST['productdata_load_url'],
				'furnidata.load.url' => $_POST['furnidata_load_url'],
				'hotelview.banner.url' => $_POST['hotelview_banner_url']];

		$cSettings['Folders'] = ['flash.client.url' => $_POST['flash_client_url'],
				'params..base' => $_POST['params__base']];
		$cSettings['clientUrl'] = $_POST['clientUrl'];
		$cSettings['client.starting'] = $_POST['client_starting'];
		$cSettings['client.new.user.reception'] = ($_POST['client_new_user_reception'] == '0') ? '0' : '1';
		
		CACHE::SetAIOConfig('Client', json_encode($cSettings));
		CACHE::SetAIOConfig('Site', json_encode($SiteSettings));
		SLOG('Change', 'Cambiado la configuración del client', 'manage.php[loader]', 0);

		$msg_correct = 'Los cambios han sido guardados con éxito.';		
	endif;
endif;

require HTML . 'ACP_header.html';
?>
<a onclick="Export()" href="javascript:void(0)">Exportar</a> <a onclick="Import()" href="javascript:void(0)">Importar</a>
		Nota: Para cargar las cosas opcionales, dejar en blanco.
<form action="<?php echo HPATH; ?>/manage?p=loader&do=save" method="post" name="theAdminForm" id="form1" class="block-content form">
<?php echo getAntiCsrf(); ?>
	<!--<div id="bottomBar"><p></p><p>Changes may take longer to update. <input class="blue" type="submit" value="Save Changes"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>-->
	<table width="100%" cellpadding="5" align="left" border="0">
		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>Reception (new Crypto)</b>
				<div class="graytext">
					new.user.reception. Necesitas unas SWF compatibles.
				</div>
			</td>
			<td class="tablerow2" width="40%" valign="middle">
				<select name="client.new.user.reception" class="styled">
					<option value="0" <?php echo $cSettings['client.new.user.reception'] == '0' ? 'selected=true' : ''; ?>>Desactivado</option>						 	
					<option value="1" <?php echo $cSettings['client.new.user.reception'] == '1' ? 'selected=true' : ''; ?>>Activado</option>
				</select>
			</td>
		</tr>
		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>Managed override variables and texts</b>
				<div class="graytext">
					Cargan las override variables & texts manejadas por aXDR, esto permite añadir texts & variables desde la ACP
				</div>
			</td>
			<td class="tablerow2" width="40%" valign="middle">
				<select name="managed.override" class="styled">
					<option value="0" <?php echo $cSettings['managed.override'] == false ? 'selected=true' : ''; ?>>Desactivado</option>						 	
					<option value="1" <?php echo $cSettings['managed.override'] == true ? 'selected=true' : ''; ?>>Activado</option>
				</select>
			</td>
		</tr>
		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>External Variables</b>
				<div class="graytext">
					external.variables.txt
				</div>
			</td>
			<td class="tablerow2" width="40%" valign="middle">
				<textarea name="external.variables.txt" cols="150" rows="3" wrap="soft" id="external.variables.txt" class="multitext"><?php echo $cSettings['Txts']['external.variables.txt']; ?></textarea>
			</td>
		</tr>

		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>Supersecret token</b>
				<div class="graytext">
					hotelview.banner.url
				</div>
			</td>
			<td class="tablerow2" width="40%" valign="middle">
				<textarea name="hotelview.banner.url" cols="150" rows="3" wrap="soft" id="hotelview.banner.url" class="multitext"><?php echo $cSettings['Txts']['hotelview.banner.url']; ?></textarea>
			</td>
		</tr>
		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>Flash Client URL</b>
				<div class="graytext">
					flash.client.url
				</div>
			</td>
			<td class="tablerow2" width="40%" valign="middle">
				<textarea name="flash.client.url" cols="150" rows="3" wrap="soft" id="flash.client.url" class="multitext"><?php echo $cSettings['Folders']['flash.client.url']; ?></textarea>
			</td>
		</tr>
		
		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>Flash Base</b>
				<div class="graytext">
					params::base
				</div>
			</td>
			<td class="tablerow2" width="40%" valign="middle">
				<textarea name="params..base" cols="150" rows="3" wrap="soft" id="params..base" class="multitext"><?php echo $cSettings['Folders']['params..base']; ?></textarea>
			</td>
		</tr>
		
		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>Principal SWF File</b>
				<div class="graytext">
					clientUrl
				</div>
			</td>
			<td class="tablerow2" width="40%" valign="middle">
				<textarea name="clientUrl" cols="150" rows="3" wrap="soft" id="clientUrl" class="multitext"><?php echo $cSettings['clientUrl']; ?></textarea>
			</td>
		</tr>	
		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>Loader Text (Optional)</b>
				<div class="graytext">
					client.starting
				</div>
			</td>
			<td class="tablerow2" width="40%" valign="middle">
				<div class="input-control">
					<input type="text" name="client.starting" id="client.starting" value="<?php echo $cSettings['client.starting']; ?>" size="30" class="textinput">
				</div>
			</td>
		</tr>
		
		<tr><td><br /><br /><br /><b>Opcionales:</b><br />Dejar en blanco para cargar desde las variables.</tr><td></td></td>
		<tr>
			<td class="tablerow1" width="40%" valign="middle"><b>External Texts</b>
				<div class="graytext">
					external.texts.txt
				</div>
			</td>
			<td class="tablerow2" width="60%" valign="middle">
				<textarea name="external.texts.txt" cols="150" rows="3" wrap="soft" id="external.texts.txt" class="multitext"><?php echo $cSettings['Txts']['external.texts.txt']; ?></textarea>
			</td>
		</tr>
		
		<tr>
			<td class="tablerow1" width="40%" valign="middle"><b>Safe Chat List</b>
				<div class="graytext">
					safechat.list.txt
				</div>
			</td>
			<td class="tablerow2" width="60%" valign="middle">
				<textarea name="safechat.list.txt" cols="150" rows="3" wrap="soft" id="safechat.list.txt" class="multitext"><?php echo $cSettings['Txts']['safechat.list.txt']; ?></textarea>
			</td>
		</tr>
		
		<tr>
			<td class="tablerow1" width="40%" valign="middle"><b>External Override Texts</b>
				<div class="graytext">
					external.override.texts.txt
				</div>
			</td>
			<td class="tablerow2" width="60%" valign="middle">
<?php if($cSettings['managed.override'] == true): ?>
				MANAGED: <?php echo PATH; ?>/managed-gamedata/override/external_flash_override_texts/<?php echo $cSettings['managed.override.token']; ?>
<?php else: ?>
				<textarea name="external.override.texts.txt" cols="150" rows="3" wrap="soft" id="external.override.texts.txt" class="multitext"><?php echo $cSettings['Txts']['external.override.texts.txt']; ?></textarea>
<?php endif; ?>
			</td>
		</tr>

		<tr>
			<td class="tablerow1" width="40%" valign="middle"><b>External Override Variables</b>
				<div class="graytext">
					external.override.variables.txt
				</div>
			</td>
			<td class="tablerow2" width="60%" valign="middle">
<?php if($cSettings['managed.override'] == true): ?>
				MANAGED: <?php echo PATH; ?>/managed-gamedata/override/external_override_variables//<?php echo $cSettings['managed.override.token']; ?>
<?php else: ?>
				<textarea name="external.override.variables.txt" cols="150" rows="3" wrap="soft" id="external.override.variables.txt" class="multitext"><?php echo $cSettings['Txts']['external.override.variables.txt']; ?></textarea>
<?php endif; ?>
			</td>
		</tr>					

		<tr>
			<td class="tablerow1" width="40%" valign="middle"><b>Productdata</b>
				<div class="graytext">
					productdata.load.url
				</div>
			</td>
			<td class="tablerow2" width="60%" valign="middle">
				<textarea name="productdata.load.url" cols="150" rows="3" wrap="soft" id="productdata.load.url" class="multitext"><?php echo $cSettings['Txts']['productdata.load.url']; ?></textarea>
			</td>
		</tr>

		<tr>
			<td class="tablerow1" width="20%" valign="middle"><b>Furnidata</b>
				<div class="graytext">
					furnidata.load.url
				</div>
			</td>
			<td class="tablerow2" width="40%" valign="middle">
				<textarea name="furnidata.load.url" cols="150" rows="3" wrap="soft" id="furnidata.load.url" class="multitext"><?php echo $cSettings['Txts']['furnidata.load.url']; ?></textarea>
			</td>
		</tr>

	</table>
</form>
<script type="text/javascript">
ChangeMenuButtons("Guardar", function() { element('#form1').submit() }, "", "");

function Export(){
	var Txts = {"external.variables.txt": element("#external.variables.txt").value,
		"external.texts.txt": element('#external.texts.txt').value,
		"safechat.list.txt": element('#safechat.list.txt').value,
		<?php if(!$cSettings['managed.override']): ?>
		"external.override.texts.txt": element('#external.override.texts.txt').value,
		"external.override.variables.txt": element('#external.override.variables.txt').value,
		<?php endif; ?>
		"productdata.load.url": element('#productdata.load.url').value,
		"furnidata.load.url": element('#furnidata.load.url').value,
		"flash.client.url": element('#flash.client.url').value,
		"params..base": element('#params..base').value,
		"clientUrl": element('#clientUrl').value,
	};
	
	NewDialog("Exportar código", "Guarda o comparte el código:", "<textarea style='width: 620px; height: 90px;'>" + JSON.stringify(Txts) + "</textarea>", "<button onclick='CloseDialog()'>Listo</button>");
}

function Insert(){
	var code = JSON.parse(document.getElementById("codeJSON").value);
	element("#external.variables.txt").value = code['external.variables.txt'];
	element('#external.texts.txt').value = code['external.texts.txt'];
	element('#safechat.list.txt').value = code['safechat.list.txt'];
	<?php if(!$cSettings['managed.override']): ?>
	element('#external.override.texts.txt').value = code['external.override.texts.txt'];
	element('#external.override.variables.txt').value = code['external.override.variables.txt'];
	<?php endif; ?>
	element('#productdata.load.url').value = code['productdata.load.url'];
	element('#furnidata.load.url').value = code['furnidata.load.url'];
	element('#flash.client.url').value = code['flash.client.url'];
	element('#params..base').value = code['params..base'];
	element('#clientUrl').value = code['clientUrl'];
	CloseDialog();
	NewDialog("Advice", "Se ha importado el código correctamente.", "", "<button onclick='CloseDialog()'>Listo</button>");
}
function Import(){
	NewDialog("Code Import", "Inserta el código que recibiste:", "<textarea id='codeJSON' style='width: 620px; height: 90px;'></textarea>", "<button class='marked' onclick='Insert()'>Listo</button><button onclick='CloseDialog()'>Cerrar</button>");
}
</script>