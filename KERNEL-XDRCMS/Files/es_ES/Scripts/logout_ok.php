<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$pagename = 'Reiniciar';

$token = isset($_GET['token']) ? $_GET['token'] : '';
$reason = isset($_GET['reason']) ? $_GET['reason'] : '';
$msg = 'Te has desconectado correctamente.';

if(USER::$LOGGED === true && $token == USER::$Row['token']):
		$MySQLi->query('UPDATE xdrcms_users_data SET RememberMeToken = \'\', web_online = ' . time() . ' WHERE id = \'' . USER::$Data['ID'] . '\' LIMIT 1');
		session_destroy();

		if($reason === 'concurrentlogin') $msg = 'Has sido desconectado porque te has registrado con otro navegador u ordenador. ';
		elseif($reason === 'expired') $msg = 'Sessi�n expirada. Has estado inactivado durante mucho tiempo.';
endif;
?>