<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2013 Xdr.
|+=========================================================+
|| # Xdr 2013. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

if (!defined("IN_AZURE")):
	header("Location:".PATH);
	exit;
endif;

if($siteBlocked):
	header('Location:' . PATH . '/error/blocked');
	exit;
endif;
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo $siteName; ?> -  </title>

    <script>
        var andSoItBegins = (new Date()).getTime();
        var habboPageInitQueue = [];
        var habboSecureServerRoot = "<?php echo SPATH; ?>";
        var habboStaticFilePath = "<?php echo webgallery; ?>";
    </script>
    <link rel="shortcut icon" href="<?php echo webgallery; ?>/v2/favicon.ico" type="image/vnd.microsoft.icon" />

    <link href='//fonts.googleapis.com/css?family=Ubuntu:400,700,400italic,700italic|Ubuntu+Condensed' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/v3_default.css" type="text/css" />
<script src="<?php echo webgallery; ?>/static/js/v3_default_top.js" type="text/javascript"></script>

<meta name="description" content="<?php echo $siteName; ?>: haz amig@s, únete a la diversión y date a conocer." />
<meta name="keywords" content="<?php echo strtolower($siteName); ?>, mundo, virtual, red social, gratis, comunidad, personaje, chat, online, adolescente, roleplaying, unirse, social, grupos, forums, seguro, jugar, juegos, amigos, adolescentes, raros, furni raros, coleccionable, crear, coleccionar, conectar, furni, muebles, mascotas, diseño de salas, compartir, expresión, placas, pasar el rato, música, celebridad, visitas de famosos, celebridades, juegos en línea, juegos multijugador, multijugador masivo" />
    <meta name="build" content="<?php echo $_XDRBuild; ?>" />

<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/v3_creditflow.css" type="text/css" />

    <!--[if lt IE 8]>
    <style type="text/css">
        .blocker {
            background-color: transparent !important;
            opacity: 1 !important;
        }
    </style>
    <![endif]-->
</head>
<body>

<div id="overlay"></div>
<header>
    <div id="border-left"></div>
    <div id="border-right"></div>
    <div id="header-content">
    <a href="<?php echo PATH; ?>" id="habbo-logo"<?php if(isset($SiteSettings['logo'])): ?> style="background-image: url(<?php echo $SiteSettings['logo']; ?>);background-repeat:no-repeat;width:300px;height:55px;"<?php endif; ?>></a>

<?php if(!isset($noNAV)): ?>
            <div class="navigation">
                <ul>

                        <?php if(USER::$LOGGED): ?><li><a href="<?php echo PATH; ?>/me"> <span style="width: 30px; display: inline-block;"><img src="<?php echo LOOK . USER::$Data['Look']; ?>&head_direction=2&headonly=true" width="27" height="30" style="position: absolute; top: 2px;"/></span> <span>Home</span></a></li><?php endif; ?>
                        <li>
                            <a href="<?php echo PATH; ?>/community"><span class="community"></span> Comunidad</a>
                            <span></span>
                        </li>
                        <li>
                            <a href="<?php echo PATH; ?>/safety"><span class="safety"></span> Seguridad</a>
                            <span></span>
                        </li>
                        <li class="selected">
                            <span class="credits"></span> <strong>Créditos</strong><span></span>
                        </li>
                        <li>
                            <a href="<?php echo PATH; ?>/articles"><span class="articles"></span> Noticias</a>
                            <span></span>
                        </li>
                </ul>
            </div>
<?php endif; ?>
    </div>
    <div id="top-bar-triangle"></div>
    <div id="top-bar-triangle-border"></div>
</header>