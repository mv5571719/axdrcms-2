<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
|| # Housekeeping by Xdr
|| # Copyright (C) 2014. Todos los derechos reservados.
|+=========================================================+
*/

$no_rea = true;
$_uMaintenance = true;
$hkzone = true;

require '../../KERNEL-XDRCMS/Init.php';
if(isset($_SERVER['HTTP_REFERER']) && parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) != www)
	exit();

$p = (isset($_GET['p'])) ? $_GET['p'] : '';
$do = (isset($_GET['do'])) ? $_GET['do'] : '';
$key = (isset($_GET['key'])) ? $_GET['key'] : '';

$LoadFooter = true;

if(!isset($_SESSION['Manage']['Login'])):
	header('Location:' . HPATH);
	exit;
endif;

function SLOG($a, $m, $p, $t){
	$GLOBALS['MySQLi']->query('INSERT INTO xdrcms_staff_log (action, message, note, userid, targetid, timestamp) VALUES (\'' . $a . '\', \'' . $m . '\', \'' . $p . '\', ' . USER::$Data['ID'] . ', ' . $t . ', ' . time() . ')');
}

function checkSecurityKey($userId, $key, $_Crypt = false) {
	if($GLOBALS['Restrictions']['Security']['SecretKeys']['Enabled']):
		if(isset($GLOBALS['Restrictions']['Security']['SecretKeys']['Keys'][$userId])):
			$_Key = ($_Crypt === true) ? md5($GLOBALS['Restrictions']['Security']['SecretKeys']['Keys'][$userId]) : $GLOBALS['Restrictions']['Security']['SecretKeys']['Keys'][$userId];
			if($_Key === $key)	return true;
		endif;
		return false;
	else:
		return isset($_SESSION['Manage']['SecretKey']) && md5($_SESSION['Manage']['SecretKey']) == $key;
	endif;
}

function getSecurityKey() {
	return isset($_SESSION['Manage']['SecretKey']) ? md5($_SESSION['Manage']['SecretKey']) : '';
}

function getAntiCsrf(){
	return '<input type="hidden" name="GUID" value="' . getSecurityKey() . '"/><input type="hidden" name="uid" value="' . USER::$Data['ID'] . '"/>';
}

function checkAntiCsrf(){
	if(isset($_POST['GUID'], $_POST['uid']) && is_numeric($_POST['uid'])):
		if($_POST['uid'] != USER::$Data['ID']):
			return false;
		elseif(checkSecurityKey($_POST['uid'], $_POST['GUID'], true)):
			return true;
		endif;
	endif;
	return false;
}

if(!USER::$LOGGED || !isset($_SESSION['Manage']['Login']) || !USER::CAN('ACP')):
	unset($_SESSION['Manage']['Login']);
	header('Location: ' . HPATH);
	exit;
endif;

if(isset($_GET['c']) && $_GET['c'] === 'login'):
	if(empty($_POST['secretKey'])):
		echo 'E1';
	elseif(strlen($_POST['secretKey']) != 5):
		echo 'E2';
	elseif(!$GLOBALS['Restrictions']['Security']['SecretKeys']['Enabled']):
		echo 'E';
	elseif(checkSecurityKey(USER::$Data['ID'], $_POST['secretKey'])):
		$_SESSION['Manage']['SecretKey'] = $_POST['secretKey'];
		echo 'S';
	else:
		echo 'E3';
	endif;
	exit;
endif;

$Pages = ['dashboard' => 1, 'stats' => 1, 'help' => 1, 
'server' => 2, 'exchange' => 2, 'loader' => 2, 'override_varstexts' => 2,
'site' => 3, 'promos' => 3, 'articles' => 3, 'styles' => 3,
'users' => 4, 'vouchers' => 4, 'ranktest' => 4, 'minimail' => 4,
'plugins' => 5, 
'logs' => 6, 'debug' => 6, 'emulator' => 6, 
'' => 1];
$menuId = 1;

if(!isset($Pages[$p])):
	require ACP . 'error404.php';
elseif($p !== '' && !USER::CAN($p)):
	require ACP . 'error403.php';
else:
	$menuId = $Pages[$p];
	require ACP . (($p === '') ? 'dashboard' : $p) . '.php';
endif;

if($LoadFooter == true):
	require HTML . 'ACP_footer.html';
endif;
?>