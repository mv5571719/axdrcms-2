<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

require "../../../KERNEL-XDRCMS/Init.php";

if(isset($_POST["groupId"]) && is_numeric($_POST["groupId"]))
	$groupid = $_POST['groupId'];
else
	exit;
	
checkloggedin(1);

$check = $MySQLi->query("SELECT member_rank FROM groups_memberships WHERE userid = '".$my_id."' AND groupid = '".$groupid."' AND member_rank > 1 AND is_pending = '0' LIMIT 1");

if($check->num_rows > 0){
	$my_membership = $check->fetch_assoc();
	$member_rank = $my_membership['member_rank'];
} else {
	echo "Lo sentimos, pero no puedes editar este Grupo.\n\n<p>\n<a href=\"".PATH."/groups/".$groupid."/id\" class=\"new-button\"><b>OK</b><i></i></a>\n</p>\n\n<div class=\"clear\"></div>";
	exit;
}

$check = $MySQLi->query("SELECT ownerid, type FROM groups_details WHERE id = '".$groupid."' LIMIT 1");

if($check->num_rows > 0){
	$groupdata = $check->fetch_assoc();
	$ownerid = $groupdata['ownerid'];
} else {
	echo "Lo sentimos, pero no puedes editar este Grupo.\n\n<p>\n<a href=\"".PATH."/groups/".$groupid."/id\" class=\"new-button\"><b>OK</b><i></i></a>\n</p>\n\n<div class=\"clear\"></div>";
	exit;
}

if($ownerid !== $my_id){ exit; }

$name = $System->CorrectStr(trim(SwitchWordFilter(textInJS($_POST['name']))));
$description = $System->CorrectStr(trim(SwitchWordFilter(textInJS($_POST['description']))));
$type = $_POST['type'];
$forum_type = $_POST['forumType'];
$topics_type = $_POST['newTopicPermission'];
$roomId = $_POST['roomId'];

if($groupdata['type'] == "3" && $_POST['type'] !== "3"){ exit; }
if($type < 0 || $type > 3){ echo "El tipo de Grupo no es v�lido."; exit; }
if($forum_type < 0 || $forum_type > 1){ echo "El tipo del Foro no es v�lido."; exit; }
if($topics_type < 0 || $topics_type > 2){ echo "El tipo del Foro no es v�lido."; exit; }

if(strlen($name) > 25){
	echo "�El nombre del Grupo es muy largo!\n\n<p>\n<a href=\"".PATH."/groups/".$groupid."/id\" class=\"new-button\"><b>OK</b><i></i></a>\n</p>\n\n<div class=\"clear\"></div>";
} elseif(strlen($description) > 200){
	echo "�La descripci�n del Grupo es muy larga!\n\n<p>\n<a href=\"".PATH."/groups/".$groupid."/id\" class=\"new-button\"><b>OK</b><i></i></a>\n</p>\n\n<div class=\"clear\"></div>";
} elseif(strlen($name) < 1){
	echo "Por favor, escribe un nombre para el Grupo.\n\n<p>\n<a href=\"".PATH."/groups/".$groupid."/id\" class=\"new-button\"><b>OK</b><i></i></a>\n</p>\n\n<div class=\"clear\"></div>";	
} else {
	$MySQLi->query("UPDATE groups_details SET name = '".$name."', description = '".$description."', roomid = '".$roomId."', type = '".$type."', pane = '".$forum_type."', topics = '".$topics_type."' WHERE id = '".$groupid."' AND ownerid = '".$my_id."' LIMIT 1");
	
	if($type == 2) {
		$MySQLi->query("DELETE FROM groups_memberships WHERE is_pending = '1' AND groupid = '".$groupid."' LIMIT 1");
	}

	echo "�Se ha editado el Grupo con �xito!\n\n<p>\n<a href=\"".PATH."/groups/".$groupid."/id\" class=\"new-button\"><b>OK</b><i></i></a>\n</p>\n\n<div class=\"clear\"></div>";
}
 
?>