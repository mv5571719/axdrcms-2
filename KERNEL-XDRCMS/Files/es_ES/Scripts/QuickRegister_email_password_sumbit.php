<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright � 2012 Xdr.
|+=========================================================+
|| # Xdr 2012. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

if(!isset($_SESSION['quickregister']['USER']['Step1']))
{
	header("Location:".PATH."/quickregister/start");
	exit();
}

unset($_SESSION['quickregister']['register_errors']);
unset($_SESSION['quickregister']['error']['username']);
unset($_SESSION['quickregister']['error']['mail']);
unset($_SESSION['quickregister']['error']['password']);
unset($_SESSION['quickregister']['error']['tos']);

if (isset($_SESSION["Index"]["Login"]["Tries"]) && $_SESSION["Index"]["Login"]["Tries"] == 4 && !$Users->CheckCaptcha($_POST['captchaResponse'], $_POST["recaptcha_challenge_field"], $_SERVER["REMOTE_ADDR"])):
	$Error = [true, "x12"];
	$_SESSION['quickregister']['register_errors'] = "Codigo de seguridad incorrecto. <br />";
	$_SESSION['quickregister']['error']["captcha2"] = true;
elseif(isset($_POST['bean_email'], $_POST['bean_retypedEmail'], $_POST['bean_password'], $_POST['bean_termsOfServiceSelection'])):
	$userName = "";

	if(!$cConfig["client.new.user.reception"] && isset($_POST['bean_username'])):
		$userName = $_POST['bean_username'];
	endif;

	$Info = Array($_POST['bean_email'], $_POST['bean_retypedEmail'], $_POST['bean_password'], $_POST['bean_termsOfServiceSelection'], $userName);
	$_SESSION['quickregister']['USER']['Step2'] = Array($Info[0], $Info[1], $Info[2], $Info[3], $Info[4]);
	$Error = $Users->Step2($Info[4], $Info[0], $Info[1], $Info[2], $Info[3], $cConfig["client.new.user.reception"]);
else:
	header("Location:" . PATH . "/quickregister/email_password");
	exit;
endif;

if($Error[0] == true)
	header("Location:" . PATH . "/quickregister/email_password/e/" . $Error[1]);
else
{
	$_SESSION['quickregister']['step3'] = "step3";
	unset($_SESSION['quickregister']['quickregister']['step2']);

	header("Location:" . PATH . "/quickregister/captcha");
}

?>