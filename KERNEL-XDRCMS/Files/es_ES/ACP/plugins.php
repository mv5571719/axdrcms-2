<?php
function reloadCategory($c){
	global $MySQLi;
	$pluginData = [];
	$Query = $MySQLi->query('SELECT * FROM xdrcms_addons WHERE category = ' . $c);
	while($Row = $Query->fetch_assoc()){
		array_push($pluginData, ['ID' => $Row['id'],
		'Rank' => $Row['rank'],
		'Color' => $Row['color'],
		'Title' => $Row['title'],
		'Internal' => $Row['internal'] === '1',
		'template' => $Row['template'] === '1',
		'canDisable' => $Row['canDisable'] === '1']);
	}

	$_file = fopen(KERNEL . '/Cache/pPlugins.' . $c . '.json', 'w');
	fwrite($_file, json_encode($pluginData));
	fclose($_file);
}

$PageName = 'Plugins';

if(($do === 'new' || ($do === 'edit' && is_numeric($key))) && isset($_POST['plugin_title'], $_POST['plugin_site'], $_POST['plugin_color'], $_POST['plugin_rank'], $_POST['plugin_template'], $_POST['plugin_cDisable'])):
	$PluginData = ['title' => $_POST['plugin_title'],
		'site' => $_POST['plugin_site'],
		'color' => $_POST['plugin_color'],
		'rank' => $_POST['plugin_rank'],
		'template' => $_POST['plugin_template'] == '1' ? '1' : '0',
		'canDisable' => $_POST['plugin_cDisable'] == '1'? '1' : '0',
		'body' => ((isset($_POST['plugin_html'])) ? str_ireplace(['php','<?'], ['&#112;hp','&lt;?'], html_entity_decode(html_entity_decode($_POST['plugin_html'], ENT_QUOTES | ENT_HTML401, 'ISO-8859-1'), ENT_QUOTES, 'ISO-8859-1')) : '')
	];
	if(!checkAntiCsrf()):
		$msg_error = 'An error occurred. Security token failed.';
	elseif(strlen($PluginData['title']) < 4):
		$msg_error = 'An error occurred. Title is too short.';
	elseif(!is_numeric($PluginData['site']) || !is_numeric($PluginData['color']) || !is_numeric($PluginData['rank']) || !is_numeric($_POST['plugin_template']) || $PluginData['site'] < 0 || $PluginData['site'] > 9 || $PluginData['color'] < 0 || $PluginData['color'] > 9 || $PluginData['rank'] < 0 || $PluginData['rank'] > $MaxRank):
		$msg_error = 'An error occurred.';
	else:
		if($do === 'new'):
			if($MySQLi->query('INSERT INTO xdrcms_addons (title, category, color, rank, template) VALUES (\'' . $PluginData['title'] . '\', ' . $PluginData['site'] . ', ' . $PluginData['color'] . ', ' . $PluginData['rank'] . ', \'' . $PluginData['template'] . '\')')):
				$PluginId = $MySQLi->insert_id;
				$_file = fopen(KERNEL . '/Cache/Plugin.' . $PluginId . '.html', 'w');
				fwrite($_file, $PluginData['body']);
				fclose($_file);
				
				reloadCategory($PluginData['site']);
				SLOG('Create', 'Creación del plugin "' . $PluginData['title'] . '". Id asignada: ' . $PluginId, 'manage.php[plugins]', 0);
				$msg_correct = 'Creado con éxito';	
				unset($PluginData);
			endif;
		elseif(isset($_SESSION['pluginLastEdit'][0]) && $_SESSION['pluginLastEdit'][0] === $key):
			if($MySQLi->query('UPDATE xdrcms_addons SET title = \'' . $PluginData['title'] . '\', category = ' . $PluginData['site'] . ', color = ' . $PluginData['color'] . ', rank = ' . $PluginData['rank'] . ', template = \'' . $PluginData['template'] . '\', canDisable = ' . $PluginData['canDisable'] . ' WHERE id = ' . $key)):
				$_file = fopen(KERNEL . '/Cache/Plugin.' . $key . '.html', 'w');
				fwrite($_file, $PluginData['body']);
				fclose($_file);
				
				reloadCategory($PluginData['site']);
				if($PluginData['site'] !== $_SESSION['pluginLastEdit'][1]):
					reloadCategory($_SESSION['pluginLastEdit'][1]);
				endif;
				
				SLOG('Change', 'Editado el plugin "' . $PluginData['title'] . '". Id del plugin: ' . $key, 'manage.php[plugins]', 0);
				$msg_correct = 'Edited plugin properly';
				unset($PluginData);
			endif;
		endif;
	endif;
elseif($do === 'edit' && is_numeric($key) && !isset($_POST['plugin_title'], $_POST['plugin_site'], $_POST['plugin_color'], $_POST['plugin_rank'], $_POST['plugin_template'])):
	$pluginQuery = $MySQLi->query("SELECT * FROM xdrcms_addons WHERE id = '" . $key . "'");
	if($pluginQuery && $pluginQuery->num_rows === 1):
		$pluginRow = $pluginQuery->fetch_assoc();
		$pluginRow['HTML'] = file_exists(KERNEL . '/Cache/Plugin.' . $key . '.html') ? file_get_contents(KERNEL . '/Cache/Plugin.' . $key . '.html') : '';
		$_SESSION['pluginLastEdit'] = [$key, $pluginRow['category']];
	endif;
elseif($do == 'remove' && is_numeric($key)):
	$c = $MySQLi->query('SELECT category FROM xdrcms_addons WHERE id = ' . $key);
	if($c && $c->num_rows == 1):
		$c = $c->fetch_assoc()['category'];
		$MySQLi->query('DELETE FROM xdrcms_addons WHERE internal = \'0\' AND id = ' . $key);
		SLOG('Remove', 'Borrado de un plugin. Id del plugin: ' . $key, 'manage.php[plugins]', 0);
		reloadCategory($c);
		$msg_correct = 'Removed plugin properly';
	endif;
endif;

require HTML . 'ACP_header.html';
if(!$Config['Cache']['PLUGINS']):
	echo 'You need to activate the Plugin\'s Cache.';
	goto a;
endif;

?>
		<table class="striped">
			<thead>
				<tr>
					<th>Id</th>
					<th class="right">Título</th>
					<th class="right">Creador</th>
					<th class="right">Publicado:</th>
					<th class="right">Acciones:</th>
				</tr>
			</thead>

			<tbody>
<?php
$pluginsQuery = $MySQLi->query('SELECT id, title, internal, creatorId, created FROM xdrcms_addons ORDER BY Id DESC');
while ($Row = $pluginsQuery->fetch_assoc()){
?>
			<tr>
				<td><?php echo $Row['id']; ?></td>
				<td class="right"><?php echo $Row['title']; ?></td>
				<td class="right"><?php echo ($Row['creatorId'] === '0') ? '<i>aXDR CMS 2.0</i>' : (($Row['creatorId'] == USER::$Data['ID']) ? USER::$Data['Name'] : USER::GetData($Row['creatorId'])['username']); ?></td>
				<td class="right"><?php echo METHOD::ParseUNIXTIME($Row['created']); ?></td>
				<td class="right"><a href='<?php echo HPATH; ?>/manage?p=plugins&do=edit&key=<?php echo $Row['id']; ?>'>Editar</a> <?php if($Row['internal'] == '0'): ?><a href='<?php echo HPATH; ?>/manage?p=plugins&do=remove&key=<?php echo $Row['id']; ?>'>Borrar</a><?php endif; ?></td>
			</tr>
<?php } ?>
			</tbody>
			<tfoot></tfoot>
		</table>

	<ul class="accordion dark" id="accordionid" data-role="accordion">
		<li class="accordionli <?php echo (isset($PluginData)) ? 'active' : ''; ?>">
			<a onclick="ChangeAccordion(this)" href="javascript:void(0)"><span></span>Crear</a>
			<div class="accordionccn">
				<form action='<?php echo HPATH; ?>/manage?p=plugins&do=new' method='post' name='theAdminForm' id='theAdminForm' class="block-content form">
				<?php echo getAntiCsrf(); ?>
					<table width='100%' cellpadding='5' align='left' border='0'>
						<tr>
							<td class='tablerow1' width='20%'  valign='middle'><b>Título</b>
								<div class='graytext'>
								</div>
							</td>
							<td class='tablerow2' width='40%'  valign='middle'>
								<input type='text' name='plugin.title' id='title' value="<?php echo (isset($PluginData)) ? $PluginData['title'] : ''; ?>" size='30' class='textinput'>
							</td>
						</tr>
						<tr>
							<td class='tablerow1'  width='20%' valign='middle'><b>Lugar</b>
								<div class='graytext'>
									Posición del Plugin
								</div>
							</td>
							<td class='tablerow2' width='40%' valign='middle'>
								<select id='plugin.nsite' name='plugin.site'>
									<option value='0'>Index</option>
									<option value='1'>Home - Arriba del Perfil</option>
									<option value='2'>Home - Abajo del Perfil</option>
									<option value='3'>Home - Abajo de las Promos | Izquierda</option>
									<option value='4'>Home - Abajo de las Promos | Derecha</option>
									<option value='5'>Todo - Column 3</option>
									<option value='6'>Todo - Footer</option>
									<option value='7'>Community - Izquierda</option>
									<option value='8'>Community - Derecha</option>
									<option value='9'>Staffs - Derecha</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class='tablerow1'  width='20%' valign='middle'><b>Color (Cuadro requerido)</b>
								<div class='graytext'>
									Elige el color del plugin:
								</div>
							</td>
							<td class='tablerow2' width='40%' valign='middle'>
								<select id="plugin.ncolor" name='plugin.color'>
									<option value='0'>Blanco</option>
									<option value='1'>Negro</option>
									<option value='2'>Azul</option>
									<option value='3'>Azul claro</option>
									<option value='4'>Rojo</option>
									<option value='5'>Verde</option>
									<option value='6'>Amárillo</option>
									<option value='7'>Naranja</option>
									<option value='8'>Marrón</option>
									<option value='9'>Gris</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class='tablerow1'  width='20%' valign='middle'><b>Rango</b>
								<div class='graytext'>
									Mínimo
								</div>
							</td>
							<td class='tablerow2' width='40%' valign='middle'>
								<select id="plugin.nrank" name='plugin.rank'>
									<option value='0'>Plugin Desactivado</option>
<?php $n = 1; while($n <= USER::$Data['Rank']): ?>
						<option id="ur.<?php echo $n; ?>" value='<?php echo $n; ?>'><?php echo ($n === 1)? $hotelName : (isset($staffRanks[$n]) ? $staffRanks[$n][0] : $n); ?></option>
<?php $n++; endwhile; ?>
								</select>
							</td>
						</tr>
						<tr>
							<td class='tablerow1'  width='20%' valign='middle'><b>Cuadro</b>
								<div class='graytext'>
									Usar cuadro de Habbo.
								</div>
							</td>
							<td class='tablerow2' width='40%' valign='middle'>
								<select id="plugin.ntemplate" name='plugin.template'>
									<option value='0'>No</option>
									<option value='1'>Sí</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class='tablerow1'  width='20%' valign='middle'><b>Desactivable</b>
								<div class='graytext'>
									Sí los usuarios pueden desactivar el plugin desde Ajustes.
								</div>
							</td>
							<td class='tablerow2' width='40%' valign='middle'>
								<select id="plugin.ncDisable" name='plugin.cDisable'>
									<option value='0'>No</option>
									<option value='1'>Sí</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class='tablerow1' width='20%' valign='middle'><b>HTML/JS/CSS</b>
								<div class='graytext'>
								</div>
							</td>
							<td class='tablerow2' width='40%' valign='middle'>
								<textarea name='plugin.html' cols='150' rows='5' wrap='soft' id='sub_desc' class='multitext'><?php echo (isset($PluginData)) ? $PluginData['body'] : ''; ?></textarea>
							</td>
						</tr>
						<tr>
							<td align='right' class='tablesubheader' colspan='2' >
								<input class='realbutton' type="submit" value="Crear" accesskey='s'/>
							</td>
						</tr>
					</table>
<?php if(isset($PluginData)): ?>
					<script type="text/javascript">
						element('#plugin.nsite').selectedIndex = <?php echo $PluginData['site']; ?>;
						element('#plugin.ncolor').selectedIndex = <?php echo $PluginData['color']; ?>;
						element('#plugin.nrank').selectedIndex = <?php echo $PluginData['rank']; ?>;
						element('#plugin.ntemplate').selectedIndex = <?php echo $PluginData['template']; ?>;
						element('#plugin.ncDisable').selectedIndex = <?php echo $PluginData['canDisable']; ?>;
					</script>
<?php endif; ?>
				</form>
			</div>
		</li>
<?php if(isset($pluginRow)): ?>
		<li class="accordionli active">
			<a onclick="ChangeAccordion(this)" href="javascript:void(0)"><span></span>Editar Plugin <?php echo $pluginRow['title']; ?></a>
			<div class="accordionccn">
				<form action='<?php echo HPATH; ?>/manage?p=plugins&do=edit&key=<?php echo $pluginRow['id']; ?>' method='post' name='theAdminForm' id='theAdminForm' class="block-content form">
				<?php echo getAntiCsrf(); ?>
					<table width='100%' cellpadding='5' align='left' border='0'>
						<tr>
							<td class='tablerow1' width='20%' valign='middle'><b>Título</b>
								<div class='graytext'>
								</div>
							</td>
							<td class='tablerow2' width='40%'  valign='middle'>
								<input type='text' name='plugin.title' id='title' value="<?php echo $pluginRow['title']; ?>" size='30' class='textinput'>
							</td>
						</tr>
						<tr>
							<td class='tablerow1'  width='20%' valign='middle'><b>Posición</b>
								<div class='graytext'>
									Posición del plugin.
								</div>
							</td>
							<td class='tablerow2' width='40%' valign='middle'>
								<select id='plugin.site' name='plugin.site'>
									<option value='0'>Index</option>
									<option value='1'>Home - Arriba del Perfil</option>
									<option value='2'>Home - Abajo del Perfil</option>
									<option value='3'>Home - Abajo de las Promos | Izquierda</option>
									<option value='4'>Home - Abajo de las Promos | Derecha</option>
									<option value='5'>Todo - Column 3</option>
									<option value='6'>Todo - Footer</option>
									<option value='7'>Community - Izquierda</option>
									<option value='8'>Community - Derecha</option>
									<option value='9'>Staffs - Derecha</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class='tablerow1'  width='20%' valign='middle'><b>Color (Cuadro requerido)</b>
								<div class='graytext'>
									Elige el color
								</div>
							</td>
							<td class='tablerow2' width='40%' valign='middle'>
								<select id="plugin.color" name='plugin.color'>
									<option value='0'>Blanco</option>
									<option value='1'>Negro</option>
									<option value='2'>Azul</option>
									<option value='3'>Azul claro</option>
									<option value='4'>Rojo</option>
									<option value='5'>Verde</option>
									<option value='6'>Amárillo</option>
									<option value='7'>Naranja</option>
									<option value='8'>Marrón</option>
									<option value='9'>Gris</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class='tablerow1'  width='20%' valign='middle'><b>Rango</b>
								<div class='graytext'>
									Mínimo
								</div>
							</td>
							<td class='tablerow2' width='40%' valign='middle'>
								<select id="plugin.rank" name='plugin.rank'>
									<option value='0'>Plugin Desactivado</option>
<?php $n = 1; while($n <= USER::$Data['Rank']): ?>
						<option id="ur.<?php echo $n; ?>" value='<?php echo $n; ?>'><?php echo ($n === 1)? $hotelName : (isset($staffRanks[$n]) ? $staffRanks[$n][0] : $n); ?></option>
<?php $n++; endwhile; ?>
								</select>
							</td>
						</tr>
						<tr>
							<td class='tablerow1'  width='20%' valign='middle'><b>Cuadro</b>
								<div class='graytext'>
									Usar cuadro de Habbo.
								</div>
							</td>
							<td class='tablerow2' width='40%' valign='middle'>
								<select id="plugin.template" name='plugin.template'>
									<option value='0'>No</option>
									<option value='1'>Sí</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class='tablerow1'  width='20%' valign='middle'><b>Desactivable</b>
								<div class='graytext'>
									Sí los usuarios pueden desactivar el plugin desde Ajustes.
								</div>
							</td>
							<td class='tablerow2' width='40%' valign='middle'>
								<select id="plugin.cDisable" name='plugin.cDisable'>
									<option value='0'>No</option>
									<option value='1'>Sí</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class='tablerow1' width='20%'  valign='middle'><b>HTML/JS/CSS</b>
								<div class='graytext'>
								</div>
							</td>
							<td class='tablerow2' width='40%'  valign='middle'>
								<textarea name='plugin.html' cols='150' rows='5' wrap='soft' id='plugin.html' class='multitext'><?php echo $pluginRow['HTML']; ?></textarea>
							</td>
						</tr>
						<tr>
							<td align='right' class='tablesubheader' colspan='2' >
								<input class='realbutton' type="submit" value="Guardar" accesskey='s'/>
							</td>
						</tr>
					</table>
				</form>
			</div>
			<script type="text/javascript">
				element('#plugin.site').selectedIndex = <?php echo $pluginRow['category']; ?>;
				element('#plugin.color').selectedIndex = <?php echo $pluginRow['color']; ?>;
				element('#plugin.rank').selectedIndex = <?php echo $pluginRow['rank']; ?>;
				element('#plugin.template').selectedIndex = <?php echo $pluginRow['template']; ?>;
				element('#plugin.cDisable').selectedIndex = <?php echo $pluginRow['canDisable']; ?>;
			</script>
		</li>
<?php endif; ?>
	</ul>
</div>
<?php
a:
?>