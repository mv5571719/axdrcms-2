<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright � 2011 Xdr.
|+=========================================================+
|| # Xdr 2011. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

if (!defined("IN_AZURE")):
	header("Location: " . PATH);
	exit;
endif;

if(!isset($body_id))
	$body_id = "landing";

if(!isset($body_class))
	$body_class = "process-template";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<title><?php echo $hotelName; ?>: <?php echo $pagename; ?> </title>

<script type="text/javascript">
var andSoItBegins = (new Date()).getTime();
</script>
	<link rel="shortcut icon" href="<?php echo webgallery; ?>/v2/favicon.ico" type="image/vnd.microsoft.icon" />
	<link rel="alternate" type="application/rss+xml" title="<?php echo $hotelName; ?>: RSS" href="<?php echo PATH; ?>/articles/rss.xml" />
	
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/process.css" type="text/css" />
<script src="<?php echo webgallery; ?>/static/js/libs2.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/visual.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/libs.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/common.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/fullcontent.js" type="text/javascript"></script>
<link rel="stylesheet" href="/styles/local/es.css" type="text/css" />

<script type="text/javascript"> 
document.habboLoggedIn = false;
var habboName = null;
var habboId = null;
var habboReqPath = "";
var habboStaticFilePath = "<?php echo webgallery; ?>";
var habboImagerUrl = "<?php echo PATH; ?>/habbo-imaging/";
var habboPartner = "";
var habboDefaultClientPopupUrl = "<?php echo PATH; ?>/client";
window.name = "habboMain";
if (typeof HabboClient != "undefined") {
    HabboClient.windowName = "client";
    HabboClient.maximizeWindow = true;
}
</script>

<meta property="fb:app_id" content="" /> 



<style type="text/css"> 
		div.left-column { float: left; width: 48% }
		div.right-column { float: right; width: 47% }
		label { display: block }
		input { width: 98% }
		input.process-button { width: auto; float: right }
        div.box-content { padding: 15px 8px; }
        div.right-column p { color: gray; }
        div.right-column .habbo-id-logo { background: transparent url(<?php echo PATH; ?>/web-gallery/v2/images/registration/Habbo_ID_logo_white.png) no-repeat; padding-top: 2px; height: 35px; width: 124px; float:right; }
        div.divider {background: transparent url(<?php echo PATH; ?>/web-gallery/v2/images/shared_icons/line_gray.png) repeat-y; width: 1px; height: 130px; float:left; margin: 1px 15px 20px;}
	</style> 


<meta name="description" content="<?php echo $hotelName; ?> Hotel: haz amig@s, �nete a la diversi�n y date a conocer." />
<meta name="keywords" content="<?php echo strtolower($hotelName); ?> hotel, mundo, virtual, red social, gratis, comunidad, personaje, chat, online, adolescente, roleplaying, unirse, social, grupos, forums, seguro, jugar, juegos, amigos, adolescentes, raros, furni raros, coleccionable, crear, coleccionar, conectar, furni, muebles, mascotas, dise�o de salas, compartir, expresi�n, placas, pasar el rato, m�sica, celebridad, visitas de famosos, celebridades, juegos en l�nea, juegos multijugador, multijugador masivo" />


<!--[if IE 8]>
<link rel="stylesheet" href="<?php echo PATH; ?>/web-gallery/v2/styles/ie8.css" type="text/css" />
<![endif]-->
<!--[if lt IE 8]>
<link rel="stylesheet" href="<?php echo PATH; ?>/web-gallery/v2/styles/ie.css" type="text/css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" href="<?php echo PATH; ?>/web-gallery/v2/styles/ie6.css" type="text/css" />
<script src="<?php echo PATH; ?>/web-gallery/static/js/pngfix.js" type="text/javascript"></script>
<script type="text/javascript">
try { document.execCommand('BackgroundImageCache', false, true); } catch(e) {}
</script>

<style type="text/css">
body { behavior: url(<?php echo PATH; ?>/web-gallery/js/csshover.htc); }
</style>
<![endif]-->

<meta name="build" content="<?php echo $_XDRBuild; ?>" />
</head>
<body id="" class="process-template secure-page">

<div id="overlay"></div>

<div id="container">
	<div class="cbb process-template-box clearfix">

		<div id="content">
			<div id="header" class="clearfix">
				<h1><a href="<?php echo PATH; ?>"></a></h1>
				<ul class="stats">
								   
				</ul>

			</div>
<div id="process-content">