<?php
// Coded by Xdr
header('Cache-Control: no-cache, no-store, must-revalidate');
header('Expires: Thu, 01 Jan 1970 00:00:00 GMT');
header('Content-Type: application/json;charset=UTF-8');
header('Pragma: no-cache');
header('P3P: CP="NON DSP COR CURa ADMa OUR STP STA"');
header('Connection: keep-alive');

if(!isset($_POST['figure'], $_POST['gender']) || strlen($_POST['figure']) > 100)
	exit;

$validLooks = [ // array (Male|Female) => array(type => array(array(ids), array(colors)))
	'M' => [
		'hr' => [[100, 110, 165, 891], [31, 32, 34, 35, 36, 37, 38, 39, 40, 42, 43, 45, 46, 47, 48, 44]],
		'sh' => [[290, 305, 906, 3115], [64, 66, 68, 71, 72, 73, 74, 75, 76, 80, 81, 82, 83, 84, 85, 88, 90, 91, 1320, 1408]],
		'ch' => [[215, 255, 262, 267], [64, 66, 68, 71, 72, 73, 74, 75, 76, 80, 81, 82, 83, 84, 85, 88, 90, 91, 1320, 1408]],
		'hd' => [[180, 200, 206, 209], [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 30, 1369, 1370, 1371]],
		'lg' => [[275, 281, 285, 3116], [64, 66, 68, 71, 72, 73, 74, 75, 76, 80, 81, 82, 83, 84, 85, 88, 90, 91, 1320, 1408]],
		'ha' => [[1003, 1015], [64, 66, 68, 71, 72, 73, 74, 75, 76, 80, 81, 82, 83, 84, 85, 88, 90, 91, 1320, 1408]],
		'ea' => [[1406], [64, 66, 68, 71, 72, 73, 74, 75, 76, 80, 81, 82, 83, 84, 85, 88, 90, 91, 1320, 1408]],
		'fa' => [[1212], [64, 66, 68, 71, 72, 73, 74, 75, 76, 80, 81, 82, 83, 84, 85, 88, 90, 91, 1320, 1408]],
		
	],
	'F' => [
		'hr' => [[515, 540, 596, 891], [31, 32, 34, 35, 36, 37, 38, 39, 40, 42, 43, 45, 46, 47, 48, 44]],
		'sh' => [[725, 906, 907, 3115], [64, 66, 68, 71, 72, 73, 74, 75, 76, 80, 81, 82, 83, 84, 85, 88, 90, 91, 1320, 1408]],
		'ch' => [[640, 660, 685, 690], [64, 66, 68, 71, 72, 73, 74, 75, 76, 80, 81, 82, 83, 84, 85, 88, 90, 91, 1320, 1408]],
		'hd' => [[600, 615, 626, 629], [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 30, 1369, 1370, 1371]],
		'lg' => [[705, 716, 720, 3116], [64, 66, 68, 71, 72, 73, 74, 75, 76, 80, 81, 82, 83, 84, 85, 88, 90, 91, 1320, 1408]],
		'ha' => [[1003, 1015], [64, 66, 68, 71, 72, 73, 74, 75, 76, 80, 81, 82, 83, 84, 85, 88, 90, 91, 1320, 1408]],
		'ea' => [[1406], [64, 66, 68, 71, 72, 73, 74, 75, 76, 80, 81, 82, 83, 84, 85, 88, 90, 91, 1320, 1408]],
		'fa' => [[1212], [64, 66, 68, 71, 72, 73, 74, 75, 76, 80, 81, 82, 83, 84, 85, 88, 90, 91, 1320, 1408]],
	]
];

$gender = (strtoupper($_POST['gender']) === 'M') ? 'M' : 'F';
$figure = $_POST['figure'];

if(strpos($figure, '.') === false):
	echo '{"code":"INVALID_FIGURE"}';
	exit;
endif;

// ANTI MUTANT 1
$_figure = explode('.', $figure);
$figure = '';

$c = [];
foreach($_figure as $str):
	if(substr_count($str, '-') < 2)
		continue;
	$_str = explode('-', $str);

	if(!isset($validLooks[$gender][$_str[0]][0]) || !in_array($_str[1], $validLooks[$gender][$_str[0]][0]))
		continue;

	if(isset($_str[2]) && !in_array($_str[2], $validLooks[$gender][$_str[0]][1]))
		continue;
	if(isset($_str[3]) && !empty($_str[3]) && !in_array($_str[3], $validLooks[$gender][$_str[0]][1]))
		continue;
	if(in_array($_str[0], $c))
		continue;

	$c[] = $_str[0];
	$figure .= $str . '.';
endforeach;
$figure = substr($figure, 0, -1);

// ANTI MUTANT 2

if($figure === '')
	$figure = ($gender === 'M') ? 'hr-165-33.ch-255-64.sh-906-77.hd-180-7.lg-285-82' : 'hr-515-47.ch-690-66.sh-725-68.hd-600-7.lg-720-62';
if(substr_count($figure, 'hd-') === 0)
	$figure .= ($gender === 'M') ? '.hd-180-7' : '.hd-600-7';
if(substr_count($figure, 'lg-') === 0)
	$figure .= ($gender === 'M') ? '.lg-285-82' : '.lg-720-62';
if($gender === 'F' && substr_count($figure, 'lg-') === 0)
	$figure .= '.ch-690-66';

// END
ini_set('session.name', 'aXDR-RTM:1');
session_start();
$_SESSION['newReceptionGender'] = $gender;
$_SESSION['newReceptionLook'] = $figure;

echo '{"code":"OK"}';
?>